#pragma once
#include "ColorType.h"
#include "LabelConnectedPixels.h"
#include "RGNData.h"
#include "CTCParams.h"
#include "SingleChannelTIFFData.h"
#include "Log.h"
#include <vector>
#include "BlobData.h"
#include "ColorStain.h"
#include "MSERDetector.h"

using namespace std;

class CHitFindingOperation
{
protected:
	template<typename T> void QuickSort(vector<T> *list, int lo, int hi);
	template<typename T> int Partition(vector<T> *list, int lo, int hi);
	template<typename T> int ChoosePivot(vector<T> *list, int lo, int hi);
	template<typename T> void Swap(vector<T> *list, int index1, int index2);
	CLabelConnectedPixels m_Grouper;
	void GetBlobs(CCTCParams *params, CRGNData *hitData, vector<CColorStain *> *stainList);
	int m_MaxBlobPixelCount;
	int m_MinBlobPixelCount;
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void SortData(vector<CRGNData *> *list);
	void GetIntersectPixels(CBlobData *blob1, CBlobData *blob2, int *pixelCount);
	void GetBlobDataFromMap(CBlobData *blob, unsigned char *map, int label, unsigned short *image, int threshold);
	void GetBlobBoundary(CBlobData *blob);
	void SaveMap(CString filename, unsigned char *map, int width, int height);
	void SaveImage(CString filename, unsigned short *map, int width, int height);
	bool IsInGivenFrame(CRGNData *newCell, int x0, int y0, int width, int height);
	bool NeighborhoodON(CBlobData *redBlob, int i, int j);
	int GetAspectRatio(CBlobData *blob, CRGNData *ptr);
	void GetColorStains(CCTCParams *params, CRGNData *hitData, vector<CColorStain *> *redBlueStainList);
	bool AlreadyFound(CRGNData *cell, vector<CRGNData *> *cellList);
	CRGNData *GetTheSameCell(int maxInten, int maxIntenX, int maxIntenY, vector<CRGNData *> *cellList);
	bool IsAQualifiedCell(CRGNData *cell);
	CMSERDetector m_MSERDetector;
	void GetBlueBlobs(CCTCParams *params, CRGNData *hitData, CBlobData *redBlob);
	void GetFrameMap(unsigned short *map, int width, int height, int numLabels);
	void GetPeaks(unsigned short *image, unsigned short *map, int width, int height, int numLabels, int minCount, int threshold, vector<PEAKINFO *> *peaks);
	void GetBlobDataFromPeak(unsigned short *image, unsigned short *map, int width, int height, PEAKINFO *peak, CBlobData *blob, int threshold);
	void GetMaxBlobFromMap(CBlobData *blob);
	void MoveToCenter(vector<CRGNData *> *inputList, vector<CRGNData *> *outputList);
	bool HasCircularBoundary(CBlobData *blob);
	int GetYLength(vector<unsigned int> **xpos, int startY, int endY, int x);

public:
	CHitFindingOperation();
	virtual ~CHitFindingOperation();
	CLog *m_Log;
	void ProcessOneROI(CCTCParams *params, CRGNData *hitData, vector<CRGNData *> *hitsFound);
	void FillCellScoreList(CRGNData *hitData, CListCtrl *cellScoreList, int hitIndex);
	void DumpRegionDataToCSVFile(CString filename, vector<CRGNData *> *list, bool isCellMapPlus);
	void ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height);
	void DilationOperation(unsigned char *inImage, unsigned char *outImage, int width, int height);
	void FillCellRegionList(vector<CRGNData *> *rgnList, CListCtrl *cellRegionList, int sortColumn, int *FoundHitCount);
	void SetPixelCountLimits(int minPixels, int maxPixels);
	void GetCenterCell(CRGNData *ptr, vector<CRGNData *> *cellList, int x, int y, CListCtrl *cellScoreList, int hitIndex);	
	void ScanImageForConfirmedCTCs(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
		CSingleChannelTIFFData *blueImage, CRGNData *data, vector<CRGNData *> *list);
	void ScanImage(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
		CSingleChannelTIFFData *blueImage, vector<CRGNData *> *list, int *confidence);
	void FreeBlobList(vector<CBlobData *> *blobList);
	unsigned int GetPos(unsigned short x, unsigned short y);
	int GetX(unsigned int pos);
	int GetY(unsigned int pos);
};
