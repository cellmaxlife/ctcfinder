//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 產生的 Include 檔案。
// 由 CellMapPlusMFC.rc 使用
//
#define IDC_SAVERGN                     3
#define IDC_LOADPARAM                   3
#define IDCANCEL2                       4
#define IDC_FINDCTC                     4
#define IDC_SAVEPARAM                   4
#define IDD_CELLMAPPLUSMFC_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_RGBIMAGE                    1000
#define IDC_BLUESTAIN                   1001
#define IDC_REDSTAIN                    1002
#define IDC_GREENSTAIN                  1003
#define IDC_LIST1                       1004
#define IDC_CELLREGIONLIST              1004
#define IDC_OVERLAYFILE                 1005
#define IDC_OPENOVERLAY                 1006
#define IDC_STATUS                      1007
#define IDC_CELLIDX                     1008
#define IDC_CHECK1                      1009
#define IDC_USEKEY                      1009
#define IDC_GBIMAGE                     1009
#define IDC_GREENFILE                   1010
#define IDC_RBIMAGE                     1010
#define IDC_OPENGREEN                   1011
#define IDC_MAXSIZE                     1011
#define IDC_CELLIDX3                    1011
#define IDC_PARAMTEXT                   1011
#define IDC_OVERLAYFILE2                1012
#define IDC_BLUEFILE                    1012
#define IDC_CELLIDX2                    1012
#define IDC_MINSIZE                     1012
#define IDC_OPENBLUE                    1013
#define IDC_NUMBITS                     1014
#define IDC_PARAMLIST                   1016
#define IDC_CTCSCORELIST                1017
#define IDC_REDBONLY                    1018
#define IDC_RADIO2                      1019
#define IDC_REDBLUE                     1019
#define IDC_CHECK2                      1020
#define IDC_USEKEY2                     1020
#define IDC_BUTTON1                     1021
#define IDC_BATCHRUN                    1021

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
