
// CellMapPlusMFCDlg.cpp : 實作檔
//

#include "stdafx.h"
#include "CellMapPlusMFC.h"
#include "CellMapPlusMFCDlg.h"
#include "afxdialogex.h"
#include "SingleRegionImageData.h"
#include "VersionNumber.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define RGNINDEX_FACTOR	1000

// CCellMapPlusMFCDlg 對話方塊

CCellMapPlusMFCDlg::CCellMapPlusMFCDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCellMapPlusMFCDlg::IDD, pParent)
	, m_RedFileName(_T(""))
	, m_Status(_T(""))
	, m_CellRegionIdx(0)
	, m_RGNDataList(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_RedIntensity = 200;
	m_GreenIntensity = 200;
	m_BlueIntensity = 200;
}

void CCellMapPlusMFCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BLUESTAIN, m_BlueStain);
	DDX_Control(pDX, IDC_GREENSTAIN, m_GreenStain);
	DDX_Control(pDX, IDC_CELLREGIONLIST, m_CellRegionList);
	DDX_Text(pDX, IDC_OVERLAYFILE, m_RedFileName);
	DDX_Control(pDX, IDC_REDSTAIN, m_RedStain);
	DDX_Control(pDX, IDC_RGBIMAGE, m_RGBImage);
	DDX_Text(pDX, IDC_STATUS, m_Status);
	DDX_Text(pDX, IDC_CELLIDX, m_CellRegionIdx);
	DDX_Control(pDX, IDC_GBIMAGE, m_GBImage);
	DDX_Control(pDX, IDC_RBIMAGE, m_RBImage);
	DDX_Control(pDX, IDC_CTCSCORELIST, m_CellScoreList);
	DDX_Control(pDX, IDC_USEKEY2, m_UseKey);
}

BEGIN_MESSAGE_MAP(CCellMapPlusMFCDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CCellMapPlusMFCDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_OPENOVERLAY, &CCellMapPlusMFCDlg::OnBnClickedOpenred)
	ON_NOTIFY(NM_CLICK, IDC_CELLREGIONLIST, &CCellMapPlusMFCDlg::OnNMClickCellregionlist)
	ON_BN_CLICKED(IDC_SAVEPARAM, &CCellMapPlusMFCDlg::OnBnClickedSaveparam)
	ON_BN_CLICKED(IDC_LOADPARAM, &CCellMapPlusMFCDlg::OnBnClickedLoadparam)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_CELLREGIONLIST, &CCellMapPlusMFCDlg::OnColumnclickCellregionlist)
	ON_BN_CLICKED(IDC_BATCHRUN, &CCellMapPlusMFCDlg::OnBnClickedBatchrun)
END_MESSAGE_MAP()


// CCellMapPlusMFCDlg 訊息處理常式

BOOL CCellMapPlusMFCDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 設定此對話方塊的圖示。當應用程式的主視窗不是對話方塊時，
	// 框架會自動從事此作業
	SetIcon(m_hIcon, TRUE);			// 設定大圖示
	SetIcon(m_hIcon, FALSE);		// 設定小圖示

	// TODO:  在此加入額外的初始設定
	CString version;
	version.Format(_T("HitViewer(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	SetWindowText(version);
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version);
	m_Status = "Please load Hits Archive File";
	UpdateData(FALSE);
	m_Image.Create(100, -100, 24);
	m_RedImage.Create(100, -100, 24);
	m_GreenImage.Create(100, -100, 24);
	m_BlueImage.Create(100, -100, 24);
	m_ImageGB.Create(100, -100, 24);
	m_ImageRB.Create(100, -100, 24);

	int nSize[] = { 60, 55, 70, 70 };
	LV_COLUMN nListColumn;
	for (int i = 0; i < 4; i++)
	{
		nListColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		nListColumn.fmt = LVCFMT_LEFT;
		nListColumn.cx = nSize[i];
		nListColumn.iSubItem = 0;
		if (i == 0)
			nListColumn.pszText = _T("RgnIdx");
		else if (i == 1)
			nListColumn.pszText = _T("Score");
		else if (i == 2)
			nListColumn.pszText = _T("X0");
		else if (i == 3)
			nListColumn.pszText = _T("Y0");
		
		m_CellRegionList.InsertColumn(i, &nListColumn);
	}
	m_CellRegionList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	
	CString message;
	for (int i = 0; i <= (int)PARAM_LASTONE; i++)
	{
		message.Format(_T("%s=%d"), m_CTCParams.m_CTCParamNames[i], m_CTCParams.m_CTCParams[i]);
		m_Log.Message(message);
	}
	int nSize1[] = { 100, 60, 60 };
	LV_COLUMN nListColumn1;
	for (int i = 0; i < 3; i++)
	{
		nListColumn1.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		nListColumn1.fmt = LVCFMT_LEFT;
		nListColumn1.cx = nSize1[i];
		nListColumn1.iSubItem = 0;
		if (i == 0)
			nListColumn1.pszText = _T("Name");
		else if (i == 1)
			nListColumn1.pszText = _T("Value");
		else if (i == 2)
			nListColumn1.pszText = _T("Score");

		m_CellScoreList.InsertColumn(i, &nListColumn1);
	}
	m_CellScoreList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_HitFinder.m_Log = &m_Log;
	return TRUE;  // 傳回 TRUE，除非您對控制項設定焦點
}

// 如果將最小化按鈕加入您的對話方塊，您需要下列的程式碼，
// 以便繪製圖示。對於使用文件/檢視模式的 MFC 應用程式，
// 框架會自動完成此作業。

void CCellMapPlusMFCDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 繪製的裝置內容

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 將圖示置中於用戶端矩形
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 描繪圖示
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

		if (m_Image != NULL)
		{
			CPaintDC dc(&m_RGBImage);
			CRect rect;
			m_RGBImage.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_Image.StretchBlt(dc.m_hDC, rect);
		}
		if (m_BlueImage != NULL)
		{
			CPaintDC dc(&m_BlueStain);
			CRect rect;
			m_BlueStain.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_BlueImage.StretchBlt(dc.m_hDC, rect);
		}
		if (m_RedImage != NULL)
		{
			CPaintDC dc(&m_RedStain);
			CRect rect;
			m_RedStain.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_RedImage.StretchBlt(dc.m_hDC, rect);
		}
		if (m_GreenImage != NULL)
		{
			CPaintDC dc(&m_GreenStain);
			CRect rect;
			m_GreenStain.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_GreenImage.StretchBlt(dc.m_hDC, rect);
		}
		if (m_ImageGB != NULL)
		{
			CPaintDC dc(&m_GBImage);
			CRect rect;
			m_GBImage.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_ImageGB.StretchBlt(dc.m_hDC, rect);
		}
		if (m_ImageRB != NULL)
		{
			CPaintDC dc(&m_RBImage);
			CRect rect;
			m_RBImage.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_ImageRB.StretchBlt(dc.m_hDC, rect);
		}
	}
}

// 當使用者拖曳最小化視窗時，
// 系統呼叫這個功能取得游標顯示。
HCURSOR CCellMapPlusMFCDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCellMapPlusMFCDlg::UpdateImageDisplay(void)
{
	RECT rect;
	m_RGBImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_BlueStain.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_GreenStain.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_RedStain.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_GBImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_RBImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}

void CCellMapPlusMFCDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_RGNDataList);
	m_CellRegionList.DeleteAllItems();
	m_CellScoreList.DeleteAllItems();
	CDialogEx::OnCancel();
}

void CCellMapPlusMFCDlg::OnBnClickedOpenred()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_RGNDataList);
	m_CellScoreList.DeleteAllItems();
	m_CellRegionList.DeleteAllItems();
	m_RedFileName = _T("");
	m_Status = _T("Start to load Hits Archive File. Please wait...");
	UpdateData(FALSE);

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Hits files (*.hit)|*.hit||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString TIFFpathname = dlg.GetPathName();
		m_RedFileName = dlg.GetFileName();
		UpdateData(FALSE);
		CFile theFile;
		if (theFile.Open(TIFFpathname, CFile::modeRead))
		{
			CArchive archive(&theFile, CArchive::load);
			int version = 0;
			archive >> version;
			if (version != 1)
			{
				m_Status.Format(_T("Hits Image Data Archive Version Number (=%d) != 1"), version);
				UpdateData(FALSE);
			}
			else
			{
				BOOL success = FALSE;
				int totalCount = 0;
				archive >> totalCount;
				if (totalCount > 0)
				{
					m_HitFinder.SetPixelCountLimits(m_CTCParams.m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT], 
						m_CTCParams.m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT]);
					unsigned short CPI_Value;
					archive >> CPI_Value;
					m_RedIntensity = CPI_Value;
					archive >> CPI_Value;
					m_GreenIntensity = CPI_Value;
					archive >> CPI_Value;
					m_BlueIntensity = CPI_Value;
					CString message;
					message.Format(_T("Hit Filename=%s, #ConfirmedCTCs=%d"), m_RedFileName, totalCount);
					m_Log.Message(message);
					for (int i = 0; i < totalCount; i++)
					{
						CSingleRegionImageData *data = new CSingleRegionImageData();
						data->Serialize(archive);
						CRGNData *ptr = new CRGNData(data->m_RegionX0Pos, data->m_RegionY0Pos, data->m_RegionWidth, data->m_RegionHeight);
						ptr->SetImages(data->m_RedRegionImage, data->m_GreenRegionImage, data->m_BlueRegionImage);
						data->m_RedRegionImage = NULL;
						data->m_GreenRegionImage = NULL;
						data->m_BlueRegionImage = NULL;
						ptr->m_HitIndex = data->m_HitIndex + 1;
						delete data;
						ptr->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
						ptr->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
						ptr->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
						ptr->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
						ptr->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
						ptr->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
						ptr->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
						ptr->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
						ptr->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
						ptr->SetCPI(RED_COLOR, m_RedIntensity);
						ptr->SetCPI(GREEN_COLOR, m_GreenIntensity);
						ptr->SetCPI(BLUE_COLOR, m_BlueIntensity);
						vector<CRGNData *> cellList;
						m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
#if 0  // == 1 display blob finding results only
						int minIntensity = MAXINT;
						for (int i = 0; i < (int)ptr->GetBlobData(RED_COLOR)->size(); i++)
						{
							CBlobData *blob = (*ptr->GetBlobData(RED_COLOR))[i];
							if (blob->m_MaxIntensity < minIntensity)
								minIntensity = blob->m_MaxIntensity;
						}
						if (minIntensity < MAXINT)
							ptr->m_RedFrameMax = minIntensity;
						else
							ptr->m_RedFrameMax = ptr->GetCPI(RED_COLOR) + ptr->GetContrast(RED_COLOR);
						minIntensity = MAXINT;
						for (int i = 0; i < (int)ptr->GetBlobData(BLUE_COLOR)->size(); i++)
						{
							CBlobData *blob = (*ptr->GetBlobData(BLUE_COLOR))[i];
							if (blob->m_MaxIntensity < minIntensity)
								minIntensity = blob->m_MaxIntensity;
						}
						if (minIntensity < MAXINT)
							ptr->m_BlueFrameMax = minIntensity;
						else
							ptr->m_BlueFrameMax = ptr->GetCPI(BLUE_COLOR) + ptr->GetContrast(BLUE_COLOR);
						minIntensity = MAXINT;
						for (int i = 0; i < (int)ptr->GetBlobData(GREEN_COLOR)->size(); i++)
						{
							CBlobData *blob = (*ptr->GetBlobData(GREEN_COLOR))[i];
							if (blob->m_MaxIntensity < minIntensity)
								minIntensity = blob->m_MaxIntensity;
						}
						if (minIntensity < MAXINT)
							ptr->m_GreenFrameMax = minIntensity;
						else
							ptr->m_GreenFrameMax = ptr->GetCPI(GREEN_COLOR) + ptr->GetContrast(GREEN_COLOR);
						CopyToRGBImage(ptr);
						UpdateImageDisplay();
						m_Status.Format(_T("Displayed %s Image"), m_RedFileName);
						UpdateData(FALSE);
						FreeRGNList(&cellList);
						delete ptr;
#else
						if (cellList.size() > 0)
						{
							int x0, y0;
							ptr->GetPosition(&x0, &y0);
							int width = ptr->GetWidth();
							int height = ptr->GetHeight();
							for (int j=0; j<(int)cellList.size(); j++)
							{
								CBlobData *blob = (*cellList[j]->GetBlobData(RED_COLOR))[0];
								CRGNData *region = new CRGNData(x0 + blob->m_MaxIntenX, y0 + blob->m_MaxIntenY, width, height);
								unsigned short *redImg = new unsigned short[width * height];
								memcpy(redImg, ptr->GetImage(RED_COLOR), sizeof(unsigned short) * width * height);
								unsigned short *greenImg = new unsigned short[width * height];
								memcpy(greenImg, ptr->GetImage(GREEN_COLOR), sizeof(unsigned short) * width * height);
								unsigned short *blueImg = new unsigned short[width * height];
								memcpy(blueImg, ptr->GetImage(BLUE_COLOR), sizeof(unsigned short) * width * height);
								region->SetImages(redImg, greenImg, blueImg);	
								m_HitFinder.GetCenterCell(region, &cellList, blob->m_MaxIntenX, blob->m_MaxIntenY, NULL, (ptr->m_HitIndex * RGNINDEX_FACTOR + j + 1));
								m_RGNDataList.push_back(region);
							}
							FreeRGNList(&cellList);
							delete ptr;
						}
						else
						{
							ptr->SetScore(0);
							m_RGNDataList.push_back(ptr);
						}
#endif
					}
#if 1
					message.Format(_T("#ConfirmedCTCs=%d Done"), totalCount);
					m_Log.Message(message);
					m_Status.Format(_T("#ConfirmedCTCs=%d Done"), totalCount);
					UpdateData(FALSE);
					if (m_RGNDataList.size() > 0)
					{
						m_Status.Format(_T("Loaded %d Hits Data"), m_RGNDataList.size());
						UpdateData(FALSE);
						m_CellRegionIdx = 0;
						FillCellRegionList();
					}
#endif
				}
				else
				{
					m_Status.Format(_T("NumHits (=%d) = 0"), totalCount);
					UpdateData(FALSE);
				}
			}
			archive.Close();
			theFile.Close();
		}
	}
}

void CCellMapPlusMFCDlg::DisplayCellRegion(int index)
{
	CopyToRGBImage(m_RGNDataList[index]);
	UpdateImageDisplay();
}

void CCellMapPlusMFCDlg::CopyToRGBImage(CRGNData *rgnPtr)
{
	BYTE *pCursor = (BYTE*)m_Image.GetBits();
	BYTE *pCursorBlue = (BYTE*)m_BlueImage.GetBits();
	BYTE *pCursorRed = (BYTE*)m_RedImage.GetBits();
	BYTE *pCursorGreen = (BYTE*)m_GreenImage.GetBits();
	BYTE *pCursorGB = (BYTE*)m_ImageGB.GetBits();
	BYTE *pCursorRB = (BYTE*)m_ImageRB.GetBits();

	int nHeight = rgnPtr->GetHeight();
	int nWidth = rgnPtr->GetWidth();
	int nStride = m_Image.GetPitch() - (nWidth * 3);

	unsigned short *pBlueBuffer = rgnPtr->GetImage(BLUE_COLOR);
	unsigned short *pGreenBuffer = rgnPtr->GetImage(GREEN_COLOR);
	unsigned short *pRedBuffer = rgnPtr->GetImage(RED_COLOR);
	unsigned short *ptrRed1 = pRedBuffer;
	unsigned short *ptrGreen1 = pGreenBuffer;
	unsigned short *ptrBlue1 = pBlueBuffer;
	unsigned short redMax = rgnPtr->m_RedFrameMax;
	unsigned short greenMax = rgnPtr->m_GreenFrameMax;
	unsigned short blueMax = rgnPtr->m_BlueFrameMax;

	BYTE bluePixelValue = 0;
	BYTE greenPixelValue = 0;
	BYTE redPixelValue = 0;

	for (int y = 0; y<nHeight; y++)
	{
		for (int x = 0; x < nWidth; x++)
		{
			if (pBlueBuffer != NULL)
				bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, rgnPtr->GetCutoff(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR));
			else
				bluePixelValue = 0;
			if (pGreenBuffer != NULL)
				greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, greenMax, rgnPtr->GetCutoff(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR));
			else
				greenPixelValue = 0;
			if (pRedBuffer != NULL)
				redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, rgnPtr->GetCutoff(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR));
			else
				redPixelValue = 0;
		
			*pCursor++ = bluePixelValue;
			*pCursor++ = greenPixelValue;
			*pCursor++ = redPixelValue;

			*pCursorBlue++ = bluePixelValue;
			*pCursorBlue++ = (BYTE)0;
			*pCursorBlue++ = (BYTE)0;

			*pCursorGreen++ = (BYTE)0;
			*pCursorGreen++ = greenPixelValue;
			*pCursorGreen++ = (BYTE)0;

			*pCursorRed++ = (BYTE)0;
			*pCursorRed++ = (BYTE)0;
			*pCursorRed++ = redPixelValue;

			*pCursorRB++ = bluePixelValue;
			*pCursorRB++ = (BYTE)0;
			*pCursorRB++ = redPixelValue;

			*pCursorGB++ = bluePixelValue;
			*pCursorGB++ = greenPixelValue;
			*pCursorGB++ = (BYTE)0;
		}
		if (nStride > 0)
		{
			pCursor += nStride;
			pCursorBlue += nStride;
			pCursorRed += nStride;
			pCursorGreen += nStride;
			pCursorGB += nStride;
			pCursorRB += nStride;
		}
	}

	vector<CBlobData *> *redBlobs = rgnPtr->GetBlobData(RED_COLOR);
	vector<CBlobData *> *greenBlobs = rgnPtr->GetBlobData(GREEN_COLOR);
	vector<CBlobData *> *blueBlobs = rgnPtr->GetBlobData(BLUE_COLOR);
	vector<CBlobData *> *greenRingBlobs = rgnPtr->GetBlobData(GB_COLORS);

	if (redBlobs->size() > 0)
	{
		pCursor = (BYTE*)m_Image.GetBits();
		pCursorRed = (BYTE*)m_RedImage.GetBits();
		pCursorRB = (BYTE*)m_ImageRB.GetBits();
		int pitch = m_Image.GetPitch();
		int pitchRed = m_RedImage.GetPitch();
		int pitchRB = m_ImageRB.GetPitch();
		for (int i = 0; i < (int)redBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*redBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*redBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;

				pCursorRed[pitchRed * y0 + 3 * x0] = (BYTE)255;
				pCursorRed[pitchRed * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorRed[pitchRed * y0 + 3 * x0 + 2] = (BYTE)255;

				pCursorRB[pitchRB * y0 + 3 * x0] = (BYTE)255;
				pCursorRB[pitchRB * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorRB[pitchRB * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}

	if (greenBlobs->size() > 0)
	{
		pCursorGreen = (BYTE*)m_GreenImage.GetBits();
		int pitch = m_GreenImage.GetPitch();
		for (int i = 0; i < (int)greenBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*greenBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*greenBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursorGreen[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}

	if (greenRingBlobs->size() > 0)
	{
		pCursorGreen = (BYTE*)m_GreenImage.GetBits();
		int pitch = m_GreenImage.GetPitch();
		for (int i = 0; i < (int)greenRingBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*greenRingBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*greenRingBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursorGreen[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}

	if (blueBlobs->size() > 0)
	{
		pCursorBlue = (BYTE*)m_BlueImage.GetBits();
		int pitch = m_BlueImage.GetPitch();
		for (int i = 0; i < (int)blueBlobs->size(); i++)
		{
			for (int j = 0; j < (int) (*blueBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*blueBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursorBlue[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursorBlue[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorBlue[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}
}

BYTE CCellMapPlusMFCDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int) contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

void CCellMapPlusMFCDlg::FillCellRegionList(void)
{
	int FoundHitCount = 0;
	m_CellRegionList.DeleteAllItems();
	m_HitFinder.FillCellRegionList(&m_RGNDataList, &m_CellRegionList, 0, &FoundHitCount);
	m_Status.Format(_T("Found HITs=%d(%0.2lf%%)"), FoundHitCount, (100.0*FoundHitCount/m_RGNDataList.size()));
	UpdateData(FALSE);
	if (m_RGNDataList.size() > 0)
	{
		m_CellRegionIdx = m_RGNDataList[0]->m_HitIndex;
		UpdateData(FALSE);
		CRGNData *ptr = m_RGNDataList[0];
		vector<CRGNData *> cellList;
		m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
		if (cellList.size() > 0)
		{
			int cellIndex = (m_CellRegionIdx % RGNINDEX_FACTOR) - 1;
			CBlobData *blob = (*cellList[cellIndex]->GetBlobData(RED_COLOR))[cellIndex];
			m_HitFinder.GetCenterCell(ptr, &cellList, blob->m_MaxIntenX, blob->m_MaxIntenY, &m_CellScoreList, m_CellRegionIdx);
			FreeRGNList(&cellList);
		}
		else
		{
			ptr->SetScore(0);
		}
		DisplayCellRegion(0);
	}
}

void CCellMapPlusMFCDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		delete (*rgnList)[i];
		(*rgnList)[i] = NULL;
	}
	(*rgnList).clear();
}

void CCellMapPlusMFCDlg::OnNMClickCellregionlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO:  在此加入控制項告知處理常式程式碼
	*pResult = 0;
	UpdateData(TRUE);
	int nSel = m_CellRegionList.GetNextItem(-1, LVNI_ALL | LVNI_SELECTED);
	CString text = m_CellRegionList.GetItemText(nSel, 0);
	int itemData = _wtoi(text);
	int RegionIndex = GetRegionIndex(itemData);
	m_CellScoreList.DeleteAllItems();
	if ((RegionIndex > 0) && (RegionIndex <= (int)m_RGNDataList.size()))
	{
		m_CellRegionIdx = itemData;
		UpdateData(FALSE);
		CRGNData *ptr = m_RGNDataList[RegionIndex - 1];
		vector<CRGNData *> cellList;
		m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
		if (cellList.size() > 0)
		{
			int cellIndex = (m_CellRegionIdx % RGNINDEX_FACTOR) - 1;
			CBlobData *blob = (*cellList[cellIndex]->GetBlobData(RED_COLOR))[0];
			m_HitFinder.GetCenterCell(ptr, &cellList, blob->m_MaxIntenX, blob->m_MaxIntenY, &m_CellScoreList, m_CellRegionIdx);
			FreeRGNList(&cellList);
		}
		else
		{
			ptr->SetScore(0);
		}
		DisplayCellRegion(RegionIndex-1);
	}
}


void CCellMapPlusMFCDlg::OnBnClickedSaveparam()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CFileDialog dlg(FALSE,    // save
		CString(".txt"),    // with default extension
		NULL,    // no initial file name
		OFN_OVERWRITEPROMPT,
		_T("CTCParam files (*.txt)|*.txt||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_CTCParams.SaveCTCParams(dlg.GetPathName());
	}
}


void CCellMapPlusMFCDlg::OnBnClickedLoadparam()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CFileDialog dlg(TRUE,    // open
		CString(".txt"),    // with default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("CTCParam files (*.txt)|*.txt||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_CTCParams.LoadCTCParams(dlg.GetPathName());
	}
}

BOOL CCellMapPlusMFCDlg::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN))
	{
		if (m_UseKey.GetCheck() == BST_CHECKED)
			m_UseKey.SetCheck(BST_UNCHECKED);
		return TRUE;
	}
	else if ((m_UseKey.GetCheck() == BST_CHECKED)
		&& (pMsg->message == WM_KEYDOWN))
	{
		int RegionIndex = GetRegionIndex(m_CellRegionIdx);
		if ((pMsg->wParam == VK_DOWN) && (RegionIndex < (int)m_RGNDataList.size()))
		{
			RegionIndex++;
		}
		else if ((pMsg->wParam == VK_UP) && (RegionIndex > 1))
		{
			RegionIndex--;
		}
		m_CellRegionIdx = m_RGNDataList[RegionIndex - 1]->m_HitIndex;
		UpdateData(FALSE);
		CRGNData *ptr = m_RGNDataList[RegionIndex - 1];
		vector<CRGNData *> cellList;
		m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
		if (cellList.size() > 0)
		{
			int cellIndex = (m_CellRegionIdx % RGNINDEX_FACTOR) - 1;
			CBlobData *blob = (*cellList[cellIndex]->GetBlobData(RED_COLOR))[0];
			m_HitFinder.GetCenterCell(ptr, &cellList, blob->m_MaxIntenX, blob->m_MaxIntenY, &m_CellScoreList, m_CellRegionIdx);
			FreeRGNList(&cellList);
		}
		else
		{
			ptr->SetScore(0);
		}
		DisplayCellRegion(RegionIndex - 1);
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

int CCellMapPlusMFCDlg::GetRegionIndex(int HitIndex)
{
	int index = 1;

	for (int i = 0; i < (int)m_RGNDataList.size(); i++)
	{
		if (m_RGNDataList[i]->m_HitIndex == HitIndex)
		{
			index = i + 1;
			break;
		}
	}
	return index;
}

void CCellMapPlusMFCDlg::OnColumnclickCellregionlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO:  在此加入控制項告知處理常式程式碼
	int FoundHitCount = 0;
	m_CellRegionList.DeleteAllItems();
	int sortColumn = pNMLV->iSubItem;
	m_HitFinder.FillCellRegionList(&m_RGNDataList, &m_CellRegionList, sortColumn, &FoundHitCount);
	m_Status.Format(_T("Found HITs=%d(%0.2lf%%)"), FoundHitCount, (100.0*FoundHitCount / m_RGNDataList.size()));
	UpdateData(FALSE);
	*pResult = 0;
}


void CCellMapPlusMFCDlg::OnBnClickedBatchrun()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_BATCHRUN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_OPENOVERLAY);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_LOADPARAM);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_SAVEPARAM);
	btn->EnableWindow(FALSE);
	
	CStdioFile BatchScriptFile;
	CString message;

	if (BatchScriptFile.Open(_T("CTCFinderBatch.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		CStdioFile batchResFile;
		if (batchResFile.Open(_T("CTCFinderBatchRunResult.csv"), CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("Sample,HitIndex,Score,Threshold,AverageInten,MaxInten,PixelCount,IntenRange,AxisLength,AspectRatio,GreenIntensity,BluePixels,GreenRingValue\n"));
			batchResFile.WriteString(textline);
			batchResFile.Close();
		}

		int lineIdx = 0;
		while (BatchScriptFile.ReadString(textline))
		{
			lineIdx++;
			int index = textline.Find(_T(","));
			if (index == -1)
			{
				m_Status.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
				UpdateData(FALSE);
				message.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
				m_Log.Message(message);
				break;
			}
			CString pathname = textline.Mid(0, index);
			textline = textline.Mid(index + 1);

			index = textline.Find(_T(","));
			if (index == -1)
			{
				m_Status.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
				UpdateData(FALSE);
				message.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
				m_Log.Message(message);
				break;
			}
			CString imagefilename = textline.Mid(0, index);
			textline = textline.Mid(index + 1);
			index = textline.Find(_T(","));
			if (index == -1)
			{
				m_Status.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
				UpdateData(FALSE);
				message.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
				m_Log.Message(message);
				break;
			}
			CString rgncsvfilename = textline.Mid(0, index);
			CString resultfilename = textline.Mid(index + 1);

			index = rgncsvfilename.Find(_T(".csv"));
			resultfilename = rgncsvfilename.Mid(0, index);
			CString filename = pathname + _T("\\") + resultfilename + _T(".hit");
			textline.Format(_T("(%d)%s"), lineIdx, resultfilename);
			m_Log.Message(textline);
			CFile theFile;
			if (theFile.Open(filename, CFile::modeRead))
			{
				CArchive archive(&theFile, CArchive::load);
				int version = 0;
				archive >> version;
				if (version != 1)
				{
					m_Status.Format(_T("Hits Image Data Archive Version Number (=%d) != 1"), version);
					UpdateData(FALSE);
				}
				else
				{
					BOOL success = FALSE;
					int totalCount = 0;
					archive >> totalCount;
					if (totalCount > 0)
					{
						m_HitFinder.SetPixelCountLimits(m_CTCParams.m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT],
							m_CTCParams.m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT]);
						unsigned short CPI_Value;
						archive >> CPI_Value;
						m_RedIntensity = CPI_Value;
						archive >> CPI_Value;
						m_GreenIntensity = CPI_Value;
						archive >> CPI_Value;
						m_BlueIntensity = CPI_Value;
						for (int i = 0; i < totalCount; i++)
						{
							CSingleRegionImageData *data = new CSingleRegionImageData();
							data->Serialize(archive);
							CRGNData *ptr = new CRGNData(data->m_RegionX0Pos, data->m_RegionY0Pos, data->m_RegionWidth, data->m_RegionHeight);
								ptr->SetImages(data->m_RedRegionImage, data->m_GreenRegionImage, data->m_BlueRegionImage);
							data->m_RedRegionImage = NULL;
							data->m_GreenRegionImage = NULL;
							data->m_BlueRegionImage = NULL;
							ptr->m_HitIndex = data->m_HitIndex + 1;
							delete data;
							ptr->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
							ptr->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
							ptr->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
							ptr->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
							ptr->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
							ptr->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
							ptr->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
							ptr->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
							ptr->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
							ptr->SetCPI(RED_COLOR, m_RedIntensity);
							ptr->SetCPI(GREEN_COLOR, m_GreenIntensity);
							ptr->SetCPI(BLUE_COLOR, m_BlueIntensity);
							vector<CRGNData *> cellList;
							m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
							if (cellList.size() > 0)
							{
								m_HitFinder.GetCenterCell(ptr, &cellList, -1, -1, NULL, ptr->m_HitIndex);
								CBlobData *blob = (*ptr->GetBlobData(RED_COLOR))[0];
								message.Format(_T("%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%.2f\n"), resultfilename, ptr->m_HitIndex, ptr->GetScore(), blob->m_Threshold-m_RedIntensity, blob->m_AverageIntensity-m_RedIntensity,
									blob->m_MaxIntensity-m_RedIntensity, blob->m_PixelCount, blob->m_IntensityRange, ptr->m_LongAxisLength, ptr->m_AspectRatio, ptr->m_GreenValue,
									ptr->GetPixels(RB_COLORS), ptr->m_GreenRingValue);
								if (batchResFile.Open(_T("CTCFinderBatchRunResult.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
								{
									batchResFile.SeekToEnd();
									batchResFile.WriteString(message);
									batchResFile.Close();
								}
								message.Format(_T(",0,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%.2f\n"), resultfilename, ptr->m_HitIndex, ptr->GetScore(), blob->m_Threshold - m_RedIntensity, blob->m_AverageIntensity - m_RedIntensity,
									blob->m_MaxIntensity - m_RedIntensity, blob->m_PixelCount, blob->m_IntensityRange, ptr->m_LongAxisLength, ptr->m_AspectRatio, ptr->m_GreenValue,
									ptr->GetPixels(RB_COLORS), ptr->m_GreenRingValue);
								m_Log.Message(message);
								FreeRGNList(&cellList);
							}
							else
							{
								message.Format(_T("%s,%d,%d,%d\n"), resultfilename, ptr->m_HitIndex, 0, ptr->GetScore());
								if (batchResFile.Open(_T("CTCFinderBatchRunResult.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
								{
									batchResFile.SeekToEnd();
									batchResFile.WriteString(message);
									batchResFile.Close();
								}
								message.Format(_T(",0,%s,%d,%d,%d"), resultfilename, ptr->m_HitIndex, 0, ptr->GetScore());
								m_Log.Message(message);
							}
							delete ptr;
						}
					}
				}
				archive.Close();
				theFile.Close();
			}
			else
			{
				message.Format(_T("Failed to load (%d)%s"), lineIdx, filename);
				m_Log.Message(message);
			}
		}
		BatchScriptFile.Close();
	}
	else
	{
		message.Format(_T("Failed to open CTCFinderBatch.txt file"));
		m_Log.Message(message);
	}

	btn = (CButton *)GetDlgItem(IDC_BATCHRUN);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_OPENOVERLAY);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_LOADPARAM);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_SAVEPARAM);
	btn->EnableWindow(TRUE);
	return;
}
