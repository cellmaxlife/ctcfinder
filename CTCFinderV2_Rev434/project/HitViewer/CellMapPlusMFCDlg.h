
// CellMapPlusMFCDlg.h : 標頭檔
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "RGNData.h"
#include "Log.h"
#include <vector>
#include "HitFindingOperation.h"
#include "CTCParams.h"

using namespace std;

// CCellMapPlusMFCDlg 對話方塊
class CCellMapPlusMFCDlg : public CDialogEx
{
// 建構
public:
	CCellMapPlusMFCDlg(CWnd* pParent = NULL);	// 標準建構函式

// 對話方塊資料
	enum { IDD = IDD_CELLMAPPLUSMFC_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支援


// 程式碼實作
protected:
	HICON m_hIcon;
	CImage m_Image;
	CImage m_BlueImage;
	CImage m_RedImage;
	CImage m_GreenImage;
	CImage m_ImageGB;
	CImage m_ImageRB;
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void UpdateImageDisplay(void);
	vector<CRGNData *> m_RGNDataList;
	void DisplayCellRegion(int index);
	void FillCellRegionList(void);
	CLog m_Log;
	void CopyToRGBImage(CRGNData *rgnPtr);
	BYTE GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff);
	CHitFindingOperation m_HitFinder;
	int m_RedIntensity;
	int m_GreenIntensity;
	int m_BlueIntensity;
	CCTCParams m_CTCParams;
	int GetRegionIndex(int HitIndex);

	// 產生的訊息對應函式
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_BlueStain;
	CStatic m_GreenStain;
	CListCtrl m_CellRegionList;
	CStatic m_RedStain;
	CStatic m_RGBImage;
	CString m_Status;
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOpenred();
	int m_CellRegionIdx;
	BOOL PreTranslateMessage(MSG* pMsg);
	CString m_RedFileName;
	CStatic m_GBImage;
	CStatic m_RBImage;
	afx_msg void OnNMClickCellregionlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedSaveparam();
	afx_msg void OnBnClickedLoadparam();
	CListCtrl m_CellScoreList;
	afx_msg void OnColumnclickCellregionlist(NMHDR *pNMHDR, LRESULT *pResult);
	CButton m_UseKey;
	afx_msg void OnBnClickedBatchrun();
};
