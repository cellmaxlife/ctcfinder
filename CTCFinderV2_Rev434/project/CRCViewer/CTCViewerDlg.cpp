﻿
// CTCViewerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTCViewer.h"
#include "CTCViewerDlg.h"
#include "afxdialogex.h"
#include "string.h"
#include "SingleRegionImageData.h"
#include "VersionNumber.h"
#include <atlstr.h>
#include "ColorType.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_MY_MESSAGE (WM_USER+1001)

// CCTCViewerDlg dialog
static bool toggleFlag = true;
static int mouseIndex = 0;
static DWORD WINAPI SortByScoreOperation(LPVOID param);

CCTCViewerDlg::CCTCViewerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCTCViewerDlg::IDD, pParent)
	, m_RGNFilename(_T(""))
	, m_TIFFFilename(_T(""))
	, m_Status(_T(""))
	, m_CTCCount(0)
	, m_ConfirmedCTCNum(0)
	, m_ConfirmedNonCTCNum(0)
	, m_CTCNum(0)
	, m_Description(_T(""))
	, m_GreenFilename(_T(""))
	, m_BlueFilename(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_ROIRect.left = 100;
	m_ROIRect.right = 105;
	m_ROIRect.top = 100;
	m_ROIRect.bottom = 105;
	m_RegionX0 = 0;
	m_RegionY0 = 0;
	m_RegionWidth = 0;
	m_RegionHeight = 0;
	m_RedRegionImage = NULL;
	m_GreenRegionImage = NULL;
	m_BlueRegionImage = NULL;
	m_CTCScoreIndex = 1;
}

void CCTCViewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMAGEDISPLAY, m_ImageDisplay);
	DDX_Text(pDX, IDC_RGNFILENAME, m_RGNFilename);
	DDX_Text(pDX, IDC_TIFFILENAME, m_TIFFFilename);
	DDX_Text(pDX, IDC_STATUS, m_Status);
	DDX_Control(pDX, IDC_BLUE, m_Blue);
	DDX_Control(pDX, IDC_GREEN, m_Green);
	DDX_Control(pDX, IDC_RED, m_Red);
	DDX_Text(pDX, IDC_CTCCount, m_CTCCount);
	DDX_Control(pDX, IDC_COLORPAD, m_ColorPadDisplay);
	DDX_Control(pDX, IDC_CTCRADIO, m_CTCRadio);
	DDX_Control(pDX, IDC_NONCTCRADIO, m_NonCTCRadio);
	DDX_Control(pDX, IDC_CONFIRMEDCTC, m_ConfirmedCTCRadio);
	DDX_Control(pDX, IDC_C_LIGHT_BLUE, m_CLightBlue);
	DDX_Control(pDX, IDC_C_PINK, m_CPink);
	DDX_Control(pDX, IDC_C_RED, m_CRed);
	DDX_Text(pDX, IDC_CONFIRMCTCNUM, m_ConfirmedCTCNum);
	DDX_Text(pDX, IDC_CONFIRMNONCTCNUM, m_ConfirmedNonCTCNum);
	DDX_Text(pDX, IDC_CTCNUM, m_CTCNum);
	DDX_Control(pDX, IDC_CTCINDEX, m_CTCIndex);
	DDX_Control(pDX, IDC_BLUESLIDE, m_BlueSlider);
	DDX_Control(pDX, IDC_GREENSLID, m_GreenSlider);
	DDX_Control(pDX, IDC_REDSLIDE, m_RedSlider);
	DDX_Control(pDX, IDC_CTCONLY, m_CTCOnly);
	DDX_Control(pDX, IDC_WBCONLY, m_WBCOnly);
	DDX_Control(pDX, IDC_VIEWALL, m_ViewAll);
	DDX_Text(pDX, IDC_GREENCONTRAST, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
	DDX_Text(pDX, IDC_GREENCUTOFF, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
	DDX_Text(pDX, IDC_REDCONTRAST, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
	DDX_Text(pDX, IDC_REDCUTOFF, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
	DDX_Text(pDX, IDC_BLUECONTRAST, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
	DDX_Text(pDX, IDC_BLUECUTOFF, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
	DDX_Control(pDX, IDC_USEKEY, m_UseKeyStroke);
	DDX_Text(pDX, IDC_DESCRIPTION, m_Description);
	DDX_Control(pDX, IDC_USEBOX, m_UseIntensityBox);
	DDX_Control(pDX, IDC_MAG1, m_Mag1);
	DDX_Text(pDX, IDC_GREENFILENAME, m_GreenFilename);
	DDX_Text(pDX, IDC_BLUEFILENAME, m_BlueFilename);
	DDX_Control(pDX, IDC_CELLSCORELIST, m_CellScoreList);
	DDX_Text(pDX, IDC_BREDTHR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
	DDX_Text(pDX, IDC_BBLUETHR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
	DDX_Text(pDX, IDC_BGREENTHR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
	DDX_Control(pDX, IDC_CTCSCORE, m_CTCScore);
	DDX_Text(pDX, IDC_MAXBLOBPIXELCOUNT, m_CTCParams.m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT]);
	DDV_MinMaxInt(pDX, m_CTCParams.m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT], 20, 2500);
	DDX_Text(pDX, IDC_MINBLOBPIXELCOUNT, m_CTCParams.m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT]);
	DDV_MinMaxInt(pDX, m_CTCParams.m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT], 10, 800);
	DDX_Text(pDX, IDC_RINGPXL, m_CTCParams.m_CTCParams[(int)PARAM_RINGPIXELS]);
	DDV_MinMaxInt(pDX, m_CTCParams.m_CTCParams[(int)PARAM_RINGPIXELS], 1, 9);
	DDX_Control(pDX, IDC_LOAD3CHANNELS, m_Load3Channels);
}

BEGIN_MESSAGE_MAP(CCTCViewerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CCTCViewerDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_RGNFILE, &CCTCViewerDlg::OnBnClickedRgnfile)
	ON_BN_CLICKED(IDC_BLUE, &CCTCViewerDlg::OnBnClickedBlue)
	ON_BN_CLICKED(IDC_RED, &CCTCViewerDlg::OnBnClickedRed)
	ON_BN_CLICKED(IDC_GREEN, &CCTCViewerDlg::OnBnClickedGreen)
	ON_BN_CLICKED(IDC_NEXT, &CCTCViewerDlg::OnBnClickedNext)
	ON_BN_CLICKED(IDC_PREV, &CCTCViewerDlg::OnBnClickedPrev)
	ON_BN_CLICKED(IDC_SAVERGN, &CCTCViewerDlg::OnBnClickedSavergn)
	ON_BN_CLICKED(IDC_CTCRADIO, &CCTCViewerDlg::OnBnClickedCtcradio)
	ON_BN_CLICKED(IDC_NONCTCRADIO, &CCTCViewerDlg::OnBnClickedNonctcradio)
	ON_BN_CLICKED(IDC_CONFIRMEDCTC, &CCTCViewerDlg::OnBnClickedConfirmedctc)
	ON_BN_CLICKED(IDC_SELECT, &CCTCViewerDlg::OnBnClickedSelect)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_SAVEIMAGE, &CCTCViewerDlg::OnBnClickedSaveimage)
	ON_BN_CLICKED(IDC_REDRESET, &CCTCViewerDlg::OnBnClickedRedreset)
	ON_BN_CLICKED(IDC_GREENRESET, &CCTCViewerDlg::OnBnClickedGreenreset)
	ON_BN_CLICKED(IDC_BLUERESET, &CCTCViewerDlg::OnBnClickedBluereset)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_REDFILE, &CCTCViewerDlg::OnBnClickedRedfile)
	ON_BN_CLICKED(IDC_GREENFILE, &CCTCViewerDlg::OnBnClickedGreenfile)
	ON_BN_CLICKED(IDC_BLUEFILE, &CCTCViewerDlg::OnBnClickedBluefile)
	ON_WM_RBUTTONDOWN()
	ON_BN_CLICKED(IDC_RESETCONTRAST, &CCTCViewerDlg::OnBnClickedResetcontrast)
	ON_BN_CLICKED(IDC_USEBOX, &CCTCViewerDlg::OnBnClickedUsebox)
	ON_BN_CLICKED(IDC_RELOAD, &CCTCViewerDlg::OnBnClickedReload)
	ON_BN_CLICKED(IDC_SAVEDATA, &CCTCViewerDlg::OnBnClickedSavedata)
	ON_BN_CLICKED(IDC_SAVEHIT, &CCTCViewerDlg::OnBnClickedSavehit)
	ON_MESSAGE(WM_MY_MESSAGE, OnMyMessage)
END_MESSAGE_MAP()


// CCTCViewerDlg message handlers

BOOL CCTCViewerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CString version;
	version.Format(_T("CRCViewer(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	SetWindowText(version);
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version);
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_Blue.SetCheck(TRUE);
	m_Green.SetCheck(TRUE);
	m_Red.SetCheck(TRUE);
	m_Status = "Please load Red Channel TIFF Image File";
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	m_ImageWidth = 0;
	m_ImageHeight = 0;
	m_DisplayedRgnIndex = 0;
	m_CTCIndex.SetWindowTextW(_T("0"));
	int nWidth = 40;
	int nHeight = 20;
	for (int i = 0; i < 3; i++)
	{
		m_ColorImage[i].Create(nWidth, -nHeight, 24);
		UINT32 colorCode = 0;
		switch (i)
		{
		case 0:
			colorCode = CTC;
			break;
		case 1:
			colorCode = CTC2;
			break;
		case 2:
			colorCode = NONCTC;
			break;
		}
		
		BYTE *pCursor = (BYTE*)m_ColorImage[i].GetBits();
		int nStride = m_ColorImage[i].GetPitch() - (nWidth * 3);
		for (int y = 0; y<nHeight; y++)
		{
			for (int x = 0; x<nWidth; x++)
			{
				*pCursor++ = (BYTE)((colorCode >> 16) & 0xFF);
				*pCursor++ = (BYTE)((colorCode >> 8) & 0xFF);
				*pCursor++ = (BYTE)(colorCode & 0xFF);
			}
			if (nStride > 0)
				pCursor += nStride;
		}
	}
	m_BlueSlider.SetRange(10, 250, TRUE);
	m_GreenSlider.SetRange(10, 250, TRUE);
	m_RedSlider.SetRange(10, 250, TRUE);
	m_Mag1.SetRange(1, 10, TRUE);
	ResetSlider();
	m_RegionX0 = 0;
	m_RegionY0 = 0;
	m_RegionWidth = 100;
	m_RegionHeight = 100;
	ResetBoxPos();
	m_ViewAll.SetCheck(BST_CHECKED);
	int nSize[] = { 85, 60, 60 };
	LV_COLUMN nListColumn;
	for (int i = 0; i < 3; i++)
	{
		nListColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		nListColumn.fmt = LVCFMT_LEFT;
		nListColumn.cx = nSize[i];
		nListColumn.iSubItem = 0;
		if (i == 0)
			nListColumn.pszText = _T("Name");
		else if (i == 1)
			nListColumn.pszText = _T("Value");
		else if (i == 2)
			nListColumn.pszText = _T("Score");

		m_CellScoreList.InsertColumn(i, &nListColumn);
	}
	m_CellScoreList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_Load3Channels.SetCheck(BST_CHECKED);
	m_HitFinder.m_Log = &m_Log;
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCTCViewerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		CPaintDC dc0(&m_CRed);
		CPaintDC dc1(&m_CPink);
		CPaintDC dc2(&m_CLightBlue);
		CRect rect1;
		m_CRed.GetClientRect(&rect1);
		dc0.SetStretchBltMode(HALFTONE);
		m_ColorImage[0].StretchBlt(dc0.m_hDC, rect1);
		m_CPink.GetClientRect(&rect1);
		dc1.SetStretchBltMode(HALFTONE);
		m_ColorImage[1].StretchBlt(dc1.m_hDC, rect1);
		m_CLightBlue.GetClientRect(&rect1);
		dc2.SetStretchBltMode(HALFTONE);
		m_ColorImage[2].StretchBlt(dc2.m_hDC, rect1);
		if (m_Image != NULL)
		{
			CPaintDC dc(&m_ImageDisplay);
			CRect rect;
			m_ImageDisplay.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_Image.StretchBlt(dc.m_hDC, rect);
		}
		if (m_Pad != NULL)
		{
			CPaintDC dc(&m_ColorPadDisplay);
			CRect rect;
			m_ColorPadDisplay.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_Pad.StretchBlt(dc.m_hDC, rect);
		}
		if ((m_UseIntensityBox.GetCheck() == BST_CHECKED) && (m_DisplayedRgnIndex > 0) && 
			((m_DisplayedRgnIndex - 1) < (int) m_RGNDataArray.size()) && (toggleFlag) && (mouseIndex == 1))
		{
			CDC* pDC = m_ImageDisplay.GetDC();
			DrawROIWindow(pDC, m_ROIRect);
			DisplayBoxDescription();
		}
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCTCViewerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCTCViewerDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRegionImageData();
	FreeRGNData(&m_RGNDataArray);
	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	m_CellScoreList.DeleteAllItems();
	CDialogEx::OnCancel();
}

void CCTCViewerDlg::OnBnClickedRgnfile()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNData(&m_RGNDataArray);
	m_CellScoreList.DeleteAllItems();
	m_CTCCount = 0;
	m_DisplayedRgnIndex = 0;
	m_CTCIndex.SetWindowTextW(_T("0"));
	m_ConfirmedCTCNum = 0;
	m_ConfirmedNonCTCNum = 0;
	m_CTCNum = 0;
	m_CTCScoreIndex = 1;
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	m_RedIntensity = m_RedTIFFData->GetCPI();
	m_GreenIntensity = m_GreenTIFFData->GetCPI();
	m_BlueIntensity = m_BlueTIFFData->GetCPI();
	CString message;
	for (int i = 0; i <= (int)PARAM_LASTONE; i++)
	{
		message.Format(_T("%s=%d"), m_CTCParams.m_CTCParamNames[i], m_CTCParams.m_CTCParams[i]);
		m_Log.Message(message);
	}

	m_HitFinder.SetPixelCountLimits(m_CTCParams.m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT],
		m_CTCParams.m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT]);

	CFileDialog dlg(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			_T("RGN files (*.rgn, *.cz)|*.rgn; *.cz"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_RGNFilename = dlg.GetFileName();
		if (LoadRegionData(filename))
		{
			message.Format(_T("Region File %s loaded successfully, #Hits=%d"), filename, m_CTCCount);
			m_Log.Message(message);
			CountColorCode();
			m_CTCCount = (int)m_RGNDataArray.size();
			if (m_CTCCount > 0)
			{
				m_DisplayedRgnIndex = 0;
				m_Status.Format(_T("Start to sort %d Hits by Score. Please wait..."), m_CTCCount);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				HANDLE thread = ::CreateThread(NULL, 0, SortByScoreOperation, (LPVOID) this, CREATE_SUSPENDED, NULL);

				if (!thread)
				{
					AfxMessageBox(_T("Fail to create thread for sorting"));
					message.Format(_T("Failed to creat a thread for sorting"));
					m_Log.Message(message);
					return;
				}

				::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
				::ResumeThread(thread);
				message.Format(_T("Launched a thread for sorting"));
				m_Log.Message(message);
			}
			else
			{
				m_Status.Format(_T("# of Hits=%d in RGN File %s"), m_CTCCount, m_RGNFilename);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
		}
		else
		{
			m_Status.Format(_T("Failed to load RGN File %s"), m_RGNFilename);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}
	else
	{
		m_Status = _T("Failed to provide RGN File Name");
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

BOOL CCTCViewerDlg::LoadRegionData(CString filename)
{
	BOOL ret = FALSE;
	int failureLocation = 0;
	CFile theFile;
	CString message;
	message.Format(_T("Start to Load %s"), filename);
	m_Log.Message(message);
	UINT32 color = 0;
	int index = 0;

	try
	{
		if (theFile.Open(filename, CFile::modeRead))
		{
			failureLocation = 1;
			BYTE* buf = new BYTE[512];
			int count = 0;
			int offset = 0;
			char *ptr = NULL;
			char *ptr1 = NULL;
			int x0 = 0;
			int y0 = 0;

			while (TRUE)
			{
				failureLocation = 2;
				count = theFile.Read(&buf[offset], 1);
				if (count == 1)
				{
					failureLocation = 3;
					if ((buf[offset] == '\0') || (buf[offset] == '\n') || (buf[offset] == '\r'))
					{
						failureLocation = 4;
						if (offset > 0)
						{
							failureLocation = 5;
							if (filename.Find(_T(".rgn"), 0) > 0)
							{
								ptr = strchr((char *)buf, ',');
								if (ptr != NULL)
								{
									failureLocation = 6;
									ptr = strstr(ptr, " 1 ");
									if (ptr != NULL)
									{
										ptr += 3;
										ptr1 = strchr(ptr, ',');
										if (ptr1 != NULL)
										{
											failureLocation = 7;
											*ptr1 = '\0';
											color = (UINT32)atoi(ptr);
											ptr1++;
											ptr = strstr(ptr1, " 2 ");
											if (ptr != NULL)
											{
												failureLocation = 8;
												ptr += 3;
												ptr1 = strchr(ptr, ' ');
												if (ptr1 != NULL)
												{
													failureLocation = 9;
													*ptr1 = '\0';
													x0 = atoi(ptr);
													ptr1++;
													ptr = strchr(ptr1, ',');
													if (ptr != NULL)
													{
														failureLocation = 10;
														*ptr = '\0';
														y0 = atoi(ptr1);
														CRGNData* data = new CRGNData(x0, y0, m_RegionWidth, m_RegionHeight);
														data->SetColorCode(color);
														data->SetCPI(RED_COLOR, m_RedIntensity);
														data->SetCPI(GREEN_COLOR, m_GreenIntensity);
														data->SetCPI(BLUE_COLOR, m_BlueIntensity);
														data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
														data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
														data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
														data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
														data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
														data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
														data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
														data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
														data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
														m_RGNDataArray.push_back(data);
														ret = TRUE;
														offset = 0;
														failureLocation = 11;
													}
												}
											}
										}
									}
								}
							}
							else
							{
								failureLocation = 8;
								int type = 0;
								buf[offset + 1] = '\0';
								ptr1 = NULL;
								ptr = strstr((char *)buf, "<Left>");
								if (ptr != NULL)
									type = 1;
								else
								{
									ptr = strstr((char *)buf, "<Top>");
									if (ptr != NULL)
										type = 2;
									else
									{
										failureLocation = 9;
										type = 0;
										if (buf[offset] == '\0')
											break;
										else
										{
											offset = 0;
											continue;
										}
									}
								}
								if (type == 1)
								{
									failureLocation = 10;
									ptr += 6;
									ptr1 = strchr(ptr, '<');
									*ptr1 = '\0';
									x0 = (int)atof(ptr);
									offset = 0;
									continue;
								}
								else if (type == 2)
								{
									failureLocation = 11;
									ptr += 5;
									ptr1 = strchr(ptr, '<');
									*ptr1 = '\0';
									y0 = (int)atof(ptr);
									CRGNData* data = new CRGNData(x0, y0, m_RegionWidth, m_RegionHeight);
									data->SetColorCode(color);
									data->SetCPI(RED_COLOR, m_RedIntensity);
									data->SetCPI(GREEN_COLOR, m_GreenIntensity);
									data->SetCPI(BLUE_COLOR, m_BlueIntensity);
									data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
									data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
									data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
									data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
									data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
									data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
									data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
									data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
									data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
									m_RGNDataArray.push_back(data);
									ret = TRUE;
									offset = 0;
									continue;
								}
							}
						}
						else
						{
							if (buf[offset] == '\0')
							{
								failureLocation = 12;
								break;
							}
							else
							{
								failureLocation = 13;
								offset = 0;
								continue;
							}
						}
					}
					else
					{
						failureLocation = 14;
						offset++;
					}
				}
				else
				{
					failureLocation = 15;
					break;
				}
			}

			theFile.Close();
			delete[] buf;
			buf = NULL;
		}
	}
	catch (CException *e)
	{
		TCHAR errCause[255];
		e->GetErrorMessage(errCause, 255);
		CString msg;
		msg.Format(_T("Cought Exception %s, Failre Location = %d"), errCause, failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	catch (...)
	{
		CString msg;
		msg.Format(_T("Cought Exception, Failre Location = %d"), failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	return ret;
}

// Copies the content of a byte buffer to a MFC image with respect to the image's alignment
void CCTCViewerDlg::CopyToRGBImage(unsigned short *pBlueBuffer, unsigned short *pGreenBuffer, unsigned short *pRedBuffer, 
	CImage *pOutImage, CRGNData *hitData)
{
	if (NULL != *pOutImage)
	{
		BYTE *pCursor = (BYTE*)pOutImage->GetBits();
		int nHeight = m_ImageHeight;
		int nWidth = m_ImageWidth;
		int BlueMax = m_BlueSlider.GetPos();
		int GreenMax = m_GreenSlider.GetPos();
		int RedMax = m_RedSlider.GetPos();
		
		int nStride = pOutImage->GetPitch() - (nWidth * 3);

		for (int y = 0; y<nHeight; y++)
		{
			for (int x = 0; x<nWidth; x++)
			{
				if (pBlueBuffer != NULL)
					*pCursor++ = GetContrastEnhancedByte(*pBlueBuffer++, BlueMax, BLUE_COLOR);
				else
					*pCursor++ = (BYTE)0;
				if (pGreenBuffer != NULL)
					*pCursor++ = GetContrastEnhancedByte(*pGreenBuffer++, GreenMax, GREEN_COLOR);
				else
					*pCursor++ = (BYTE)0;
				if (pRedBuffer != NULL)
					*pCursor++ = GetContrastEnhancedByte(*pRedBuffer++, RedMax, RED_COLOR);
				else
					*pCursor++ = (BYTE)0;
			}
			if (nStride > 0)
				pCursor += nStride;
		}

		vector<CBlobData *> *redBlobs = hitData->GetBlobData(RED_COLOR);
		vector<CBlobData *> *greenBlobs = hitData->GetBlobData(GREEN_COLOR);
		vector<CBlobData *> *blueBlobs = hitData->GetBlobData(BLUE_COLOR);
		vector<CBlobData *> *greenRingBlobs = hitData->GetBlobData(GB_COLORS);
		if ((pRedBuffer != NULL) && (redBlobs->size() > 0))
		{
			pCursor = (BYTE*)pOutImage->GetBits();
			int pitch = pOutImage->GetPitch();
			for (int i = 0; i < (int)redBlobs->size(); i++)
			{
				for (int j = 0; j < (int)(*redBlobs)[i]->m_Boundary->size(); j++)
				{
					unsigned int pos = (*(*redBlobs)[i]->m_Boundary)[j];
					int x0 = m_HitFinder.GetX(pos);
					int y0 = m_HitFinder.GetY(pos);
					pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
					pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
					pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
				}
			}
		}

		if ((pGreenBuffer != NULL) && (pRedBuffer == NULL) && (pBlueBuffer == NULL) && (greenBlobs->size() > 0))
		{
			pCursor = (BYTE*)pOutImage->GetBits();
			int pitch = pOutImage->GetPitch();
			for (int i = 0; i < (int)greenBlobs->size(); i++)
			{
				for (int j = 0; j < (int)(*greenBlobs)[i]->m_Boundary->size(); j++)
				{
					unsigned int pos = (*(*greenBlobs)[i]->m_Boundary)[j];
					int x0 = m_HitFinder.GetX(pos);
					int y0 = m_HitFinder.GetY(pos);
					pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
					pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
					pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
				}
			}
		}

		if ((pGreenBuffer != NULL) && (pRedBuffer == NULL) && (pBlueBuffer == NULL) && (greenRingBlobs->size() > 0))
		{
			pCursor = (BYTE*)pOutImage->GetBits();
			int pitch = pOutImage->GetPitch();
			for (int i = 0; i < (int)greenRingBlobs->size(); i++)
			{
				for (int j = 0; j < (int)(*greenRingBlobs)[i]->m_Boundary->size(); j++)
				{
					unsigned int pos = (*(*greenRingBlobs)[i]->m_Boundary)[j];
					int x0 = m_HitFinder.GetX(pos);
					int y0 = m_HitFinder.GetY(pos);
					pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
					pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
					pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
				}
			}
		}

		if ((pBlueBuffer != NULL) && (pRedBuffer == NULL) && (pGreenBuffer == NULL) && (blueBlobs->size() > 0))
		{
			pCursor = (BYTE*)pOutImage->GetBits();
			int pitch = pOutImage->GetPitch();
			for (int i = 0; i < (int)blueBlobs->size(); i++)
			{
				for (int j = 0; j < (int)(*blueBlobs)[i]->m_Boundary->size(); j++)
				{
					unsigned int pos = (*(*blueBlobs)[i]->m_Boundary)[j];
					int x0 = m_HitFinder.GetX(pos);
					int y0 = m_HitFinder.GetY(pos);
					pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
					pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
					pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
				}
			}
		}
	}
}

void CCTCViewerDlg::ClickColor()
{
	// TODO:  在此加入控制項告知處理常式程式碼	
	unsigned short *blue = m_BlueRegionImage;
	unsigned short *green = m_GreenRegionImage;
	unsigned short *red = m_RedRegionImage;
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_ImageWidth = 0;
		m_ImageHeight = 0;
	}
	m_ImageWidth = m_RegionWidth;
	m_ImageHeight = m_RegionHeight;
	m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

	CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), &m_Image,
		m_RGNDataArray[m_DisplayedRgnIndex-1]);

	RECT rect1;
	m_ImageDisplay.GetWindowRect(&rect1);
	ScreenToClient(&rect1);
	InvalidateRect(&rect1, false);
	m_Status.Format(_T("Displayed ROI Image of Rgn No.%d, UpLeftCorner: X=%d,Y=%d"), m_DisplayedRgnIndex, m_RegionX0, m_RegionY0);
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}


void CCTCViewerDlg::OnBnClickedRed()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	ClickColor();
}

void CCTCViewerDlg::OnBnClickedGreen()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	ClickColor();
}

void CCTCViewerDlg::OnBnClickedBlue()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	ClickColor();
}

bool CCTCViewerDlg::GetNextIndex(int *index)
{
	bool ret = false;

	*index = m_DisplayedRgnIndex - 1;
	for (int i = m_DisplayedRgnIndex; i < m_CTCCount; i++)
	{
		unsigned int color = m_RGNDataArray[i]->GetColorCode();
		if ((m_CTCOnly.GetCheck() == BST_CHECKED) &&
			((color == CTC) || (color == CTC2)))
		{
			*index = i;
			ret = true;
			break;
		}
		else if ((m_WBCOnly.GetCheck() == BST_CHECKED) &&
			(color == NONCTC))
		{
			*index = i;
			ret = true;
			break;
		}
	}
	return ret;
}

bool CCTCViewerDlg::GetPrevIndex(int *index)
{
	bool ret = false;

	*index = m_DisplayedRgnIndex - 1;
	for (int i = m_DisplayedRgnIndex - 2; i >= 0; i--)
	{
		unsigned int color = m_RGNDataArray[i]->GetColorCode();
		if ((m_CTCOnly.GetCheck() == BST_CHECKED) &&
			((color == CTC) || (color == CTC2)))
		{
			*index = i;
			ret = true;
			break;
		}
		else if ((m_WBCOnly.GetCheck() == BST_CHECKED) &&
			(color == NONCTC))
		{
			*index = i;
			ret = true;
			break;
		}
	}
	return ret;
}

void CCTCViewerDlg::OnBnClickedNext()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	int nextIndex = 0;
	if (m_CTCScore.GetCheck() == BST_CHECKED)
	{
		m_DisplayedRgnIndex = GetNextCTCScoreIndex();
	}
	else if (((m_ViewAll.GetCheck() == BST_CHECKED) && ((m_DisplayedRgnIndex + 1) <= m_CTCCount)) ||
		(((m_CTCOnly.GetCheck() == BST_CHECKED) || (m_WBCOnly.GetCheck() == BST_CHECKED)) && GetNextIndex(&nextIndex)))
	{
		if (m_ViewAll.GetCheck() == BST_CHECKED)
			m_DisplayedRgnIndex++;
		else
			m_DisplayedRgnIndex = nextIndex + 1;
	}
	CString indexStr;
	if (m_CTCScore.GetCheck() == BST_CHECKED)
		indexStr.Format(_T("%d"), m_CTCScoreIndex);
	else
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
	m_CTCIndex.SetWindowTextW(indexStr);
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	ResetSlider();
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
}


void CCTCViewerDlg::OnBnClickedPrev()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	int nextIndex = 0;
	if (m_CTCScore.GetCheck() == BST_CHECKED)
	{
		m_DisplayedRgnIndex = GetPrevCTCScoreIndex();
	}
	else if (((m_ViewAll.GetCheck() == BST_CHECKED) && ((m_DisplayedRgnIndex - 1) > 0)) ||
		(((m_CTCOnly.GetCheck() == BST_CHECKED) || (m_WBCOnly.GetCheck() == BST_CHECKED)) && GetPrevIndex(&nextIndex)))
	{
		if (m_ViewAll.GetCheck() == BST_CHECKED)
			m_DisplayedRgnIndex--;
		else
			m_DisplayedRgnIndex = nextIndex + 1;
	}
	CString indexStr;
	if (m_CTCScore.GetCheck() == BST_CHECKED)
		indexStr.Format(_T("%d"), m_CTCScoreIndex);
	else
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
	m_CTCIndex.SetWindowTextW(indexStr);
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	ResetSlider();
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
}

void CCTCViewerDlg::CopyToRGBPad(unsigned int color)
{
	if (m_Pad != NULL)
	{
		m_Pad.Destroy();
	}
	int nHeight = 48;
	int nWidth = 48;
	m_Pad.Create(nWidth, -nHeight, 24);

	BYTE *pCursor = (BYTE*)m_Pad.GetBits();
	int nStride = m_Pad.GetPitch() - (nWidth * 3);

	BYTE blue = ((color >> 16) & 0xFF);
	BYTE green = ((color >> 8) & 0xFF);
	BYTE red = (color & 0xFF);
	for (int y = 0; y<nHeight; y++)
	{
		for (int x = 0; x<nWidth; x++)
		{
			*pCursor++ = blue;
			*pCursor++ = green;
			*pCursor++ = red;
		}
		if (nStride > 0)
			pCursor += nStride;
	}
}

void CCTCViewerDlg::FreeRGNData(vector<CRGNData *> *rgnList)
{
	if (rgnList->size() != 0)
	{
		for (int i = 0; i < (int) rgnList->size(); i++)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
		rgnList->clear();
	}
}

BOOL  CCTCViewerDlg::DisplayROI(int index)
{
	BOOL ret = FALSE;
	CString message;

	int index1 = index - 1;
	unsigned int color = m_RGNDataArray[index1]->GetColorCode();
	if (m_CTCRadio.GetCheck())
		m_CTCRadio.SetCheck(FALSE);
	if (m_ConfirmedCTCRadio.GetCheck())
		m_ConfirmedCTCRadio.SetCheck(FALSE);
	if (m_NonCTCRadio.GetCheck())
		m_NonCTCRadio.SetCheck(FALSE);
	switch (color)
	{
	case CTC:
		m_CTCRadio.SetCheck(TRUE);
		break;
	case CTC2:
		m_ConfirmedCTCRadio.SetCheck(TRUE);
		break;
	default:
		m_NonCTCRadio.SetCheck(TRUE);
		break;
	}
	CopyToRGBPad(color);
	RECT rect1;
	m_ColorPadDisplay.GetWindowRect(&rect1);
	ScreenToClient(&rect1);
	InvalidateRect(&rect1, false);

	unsigned short *blue = m_BlueRegionImage;
	unsigned short *green = m_GreenRegionImage;
	unsigned short *red = m_RedRegionImage;
	if ((blue == NULL) || (green == NULL) || (red == NULL))
	{
		message.Format(_T("Failed to get ROI Image Data for Rgn No.%d"), index);
		m_Log.Message(message);
		m_Status = message;
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	else
	{
		ret = TRUE;
		if (m_Image != NULL)
		{
			m_Image.Destroy();
			m_ImageWidth = 0;
			m_ImageHeight = 0;
		}
		m_ImageWidth = m_RegionWidth;
		m_ImageHeight = m_RegionHeight;
		m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

		CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), 
			&m_Image, m_RGNDataArray[index1]);

		RECT rect1;
		m_ImageDisplay.GetWindowRect(&rect1);
		ScreenToClient(&rect1);
		InvalidateRect(&rect1, false);
		m_Status.Format(_T("Displayed ROI Image of Rgn No.%d, UpLeftCorner: X=%d,Y=%d"), index, m_RegionX0, m_RegionY0);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	return ret;
}

void CCTCViewerDlg::OnBnClickedSavergn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if (m_RGNDataArray.size() > 0)
	{
		SaveRGNFile();
	}
	else
	{
		m_Status = "Cell Region # is 0";
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

void CCTCViewerDlg::OnBnClickedCtcradio()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(CTC);
		UpdateColorCodeSelection();
	}
}

void CCTCViewerDlg::OnBnClickedNonctcradio()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(NONCTC);
		UpdateColorCodeSelection();
	}
}

void CCTCViewerDlg::OnBnClickedConfirmedctc()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(CTC2);
		UpdateColorCodeSelection();
	}
}

void CCTCViewerDlg::UpdateColorCodeSelection(void)
{
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		CopyToRGBPad(m_RGNDataArray[m_DisplayedRgnIndex - 1]->GetColorCode());
		RECT rect1;
		m_ColorPadDisplay.GetWindowRect(&rect1);
		ScreenToClient(&rect1);
		InvalidateRect(&rect1, false);
		CountColorCode();
	}
}

BOOL CCTCViewerDlg::SaveRGNFile()
{
	BOOL ret = FALSE;
	CString filenameForSave;

	CFileDialog dlg(FALSE,    // save
		CString(".rgn"),    // no default extension
		NULL,    // no initial file name
		OFN_OVERWRITEPROMPT,
		_T("RGN files (*.rgn)|*.rgn||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		filenameForSave = dlg.GetPathName();
		CStdioFile theFile;

		if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			for (int i = 0; i < (int) m_RGNDataArray.size(); i++)
			{
				CString singleLine;
				int x0, y0;
				m_RGNDataArray[i]->GetPosition(&x0, &y0);
				singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
					m_RGNDataArray[i]->GetColorCode(), x0, y0, i + 1);
				theFile.WriteString(singleLine);
			}
			theFile.Close();
			m_Status.Format(_T("%s Saved."), dlg.GetFileName());
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
		else
		{
			m_Status.Format(_T("Failed to Open %s to save."), dlg.GetFileName());
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}
	return ret;
}

void CCTCViewerDlg::OnBnClickedSelect()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CString indexStr; 
	m_CTCIndex.GetWindowTextW(indexStr);
	int newIndex = _wtoi(indexStr);
	if (m_CTCScore.GetCheck() == BST_CHECKED)
	{
		int index = GetDisplayIndexFromScoreIndex(newIndex);
		if (index == -1)
		{
			m_Status.Format(_T("Entered a wrong Score Rank No.(%d)"), newIndex);
			return;
		}
		else
		{
			m_DisplayedRgnIndex = index;
			m_CTCScoreIndex = newIndex;
			indexStr.Format(_T("%d"), m_CTCScoreIndex);
		}
	}
	else
	{
		int index = GetScoreIndexFromDisplayIndex(newIndex);
		if (index == -1)
		{
			m_Status.Format(_T("Entered a wrong Region No.(%d)"), newIndex);
			return;
		}
		else
		{
			m_DisplayedRgnIndex = newIndex;
			m_CTCScoreIndex = index;
			indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		}
	}
	m_CTCIndex.SetWindowTextW(indexStr);
	UpdateColorCodeSelection();	
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	ResetSlider();
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
}

void CCTCViewerDlg::ResetSlider()
{
	m_BlueSlider.SetPos(120);
	m_GreenSlider.SetPos(120);
	m_RedSlider.SetPos(120);
	m_Mag1.SetPos(10);
	m_CellScoreList.DeleteAllItems();
}

void CCTCViewerDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO:  在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值
	if ((pScrollBar == (CScrollBar *)&m_BlueSlider) ||
		(pScrollBar == (CScrollBar *)&m_GreenSlider) ||
		(pScrollBar == (CScrollBar *)&m_RedSlider) ||
		(pScrollBar == (CScrollBar *)&m_Mag1))
	{
		if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		{
			if (pScrollBar == (CScrollBar *)&m_Mag1)
			{
				LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
			}
			else
			{
				if (pScrollBar == (CScrollBar *)&m_BlueSlider)
					m_Blue.SetCheck(TRUE);
				else if (pScrollBar == (CScrollBar *)&m_GreenSlider)
					m_Green.SetCheck(TRUE);
				else if (pScrollBar == (CScrollBar *)&m_RedSlider)
					m_Red.SetCheck(TRUE);
				DisplayROI(m_DisplayedRgnIndex);
			}
		}
	}
	else {
		CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
	}
}

BYTE CCTCViewerDlg::GetContrastEnhancedByte(unsigned short value, int maxValue, PIXEL_COLOR_TYPE color)
{
	BYTE result = 0;
	int intensity = 0;
	if (color == BLUE_COLOR)
	{
		intensity = m_BlueIntensity;
		intensity += m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF];
	}
	else if (color == GREEN_COLOR)
	{
		intensity = m_GreenIntensity;
		intensity += m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF];
	}
	else if (color == RED_COLOR)
	{
		intensity = m_RedIntensity;
		intensity += m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF];
	}
	int maxInten = 100;
	if (color == BLUE_COLOR)
	{
		maxInten = m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST] - intensity;
	}
	else if (color == GREEN_COLOR)
	{
		maxInten = m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST] - intensity;
	}
	else if (color == RED_COLOR)
	{
		maxInten = m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST] - intensity;
	}
	
	if (maxValue > 120)
	{
		maxInten = (int) (((4095.0 - maxInten) * (maxValue - 120.0) / 130.0) + maxInten);
	}
	else if (maxValue < 120)
	{
		maxInten = (int) (maxInten * maxValue / 120.0);
	}
	
	int value1 = value;
	value1 -= intensity;
	if (value1 < 0)
		value1 = 0;
	if (value1 >= maxInten)
		result = (BYTE)255;
	else
		result = (BYTE)(255 * value1 / maxInten);
	return result;
}

BOOL CCTCViewerDlg::PreTranslateMessage(MSG* pMsg)
{
	CEdit *redCutoff = (CEdit *)GetDlgItem(IDC_REDCUTOFF);
	CEdit *greenCutoff = (CEdit *)GetDlgItem(IDC_GREENCUTOFF);
	CEdit *blueCutoff = (CEdit *)GetDlgItem(IDC_BLUECUTOFF);
	CEdit *redContrast = (CEdit *)GetDlgItem(IDC_REDCONTRAST);
	CEdit *greenContrast = (CEdit *)GetDlgItem(IDC_GREENCONTRAST);
	CEdit *blueContrast = (CEdit *)GetDlgItem(IDC_BLUECONTRAST);
	CEdit *ctcIndex = (CEdit *)GetDlgItem(IDC_CTCINDEX);
	CEdit *bRedThre = (CEdit *)GetDlgItem(IDC_BREDTHR);
	CEdit *bBlueThre = (CEdit *)GetDlgItem(IDC_BBLUETHR);
	CEdit *bGreenThre = (CEdit *)GetDlgItem(IDC_BGREENTHR);
	CEdit *bMaxCount = (CEdit *)GetDlgItem(IDC_MAXBLOBPIXELCOUNT);
	CEdit *bMinCount = (CEdit *)GetDlgItem(IDC_MINBLOBPIXELCOUNT);
	CEdit *bRingPxl = (CEdit *)GetDlgItem(IDC_RINGPXL);

	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN) &&
		((GetFocus() == redCutoff) ||
		(GetFocus() == greenCutoff) ||
		(GetFocus() == blueCutoff) ||
		(GetFocus() == redContrast) ||
		(GetFocus() == greenContrast) ||
		(GetFocus() == blueContrast) ||
		(GetFocus() == ctcIndex) ||
		(GetFocus() == bRedThre) ||
		(GetFocus() == bBlueThre) ||
		(GetFocus() == bGreenThre) ||
		(GetFocus() == bMaxCount) ||
		(GetFocus() == bMinCount) ||
		(GetFocus() == bRingPxl)))
	{
		UpdateData(TRUE);
		if ((m_DisplayedRgnIndex > 0) && ((m_DisplayedRgnIndex - 1) < (int) m_RGNDataArray.size()))
			DisplayROI(m_DisplayedRgnIndex);
		return TRUE; // this doesn't need processing anymore
	}
	else if ((m_UseKeyStroke.GetCheck() == BST_CHECKED) &&
		(pMsg->message == WM_KEYDOWN) &&
		((GetFocus() == redCutoff) ||
		(GetFocus() == greenCutoff) ||
		(GetFocus() == blueCutoff) ||
		(GetFocus() == redContrast) ||
		(GetFocus() == greenContrast) ||
		(GetFocus() == blueContrast) ||
		(GetFocus() == ctcIndex) ||
		(GetFocus() == bRedThre) ||
		(GetFocus() == bBlueThre) ||
		(GetFocus() == bGreenThre) ||
		(GetFocus() == bMaxCount) ||
		(GetFocus() == bMinCount) ||
		(GetFocus() == bRingPxl)))
	{
		m_UseKeyStroke.SetCheck(BST_UNCHECKED);
	}
	else if ((m_UseKeyStroke.GetCheck() == BST_CHECKED) 
		&& (pMsg->message == WM_KEYDOWN) &&
		((pMsg->wParam == 'Q') || (pMsg->wParam == 'q') ||
		(pMsg->wParam == 'W') || (pMsg->wParam == 'w') || 
		(pMsg->wParam == 'E') || (pMsg->wParam == 'e')))
	{
		if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		{
			switch (pMsg->wParam)
			{
			case 'Q':
			case 'q':
				m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(CTC);
				break;
			case 'W':
			case 'w':
				m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(CTC2);
				break;
			case 'E':
			case 'e':
				m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(NONCTC);
				break;
			}
		}
		CountColorCode();
		if (m_CTCScore.GetCheck() == BST_CHECKED)
		{
			m_DisplayedRgnIndex = GetNextCTCScoreIndex();
		}
		else if ((m_DisplayedRgnIndex + 1) <= m_CTCCount)
		{
			m_DisplayedRgnIndex++;	
		}
		CString indexStr;
		if (m_CTCScore.GetCheck() == BST_CHECKED)
			indexStr.Format(_T("%d"), m_CTCScoreIndex);
		else
			indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_CTCIndex.SetWindowTextW(indexStr);
		LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CCTCViewerDlg::DrawROIWindow(CDC* dc, RECT rc)
{
	CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255)); // red (255,0,0), green (0.255,0), blue (0.0.255)

	CPen *pOldPen = dc->SelectObject(&CursorPen);
	CBrush brNull;
	LOGBRUSH lb;
	lb.lbStyle = BS_NULL;
	brNull.CreateBrushIndirect(&lb);
	dc->SelectObject(&brNull);
	dc->Ellipse(rc.left, rc.top, rc.right, rc.bottom);
	dc->SelectObject(pOldPen);
}

void CCTCViewerDlg::ResetBoxPos()
{
	RECT rect;
	m_ImageDisplay.GetClientRect(&rect);
	m_ROIRect.left = 20;
	m_ROIRect.bottom = rect.bottom - 20;
	m_BoxWidth = (int) ((rect.right - rect.left) * (10.0 / 0.648) / m_RegionWidth);
	m_BoxHeight = (int) ((rect.bottom - rect.top) * (10.0 / 0.648) / m_RegionHeight);
	m_ROIRect.right = m_ROIRect.left + m_BoxWidth;
	m_ROIRect.top = m_ROIRect.bottom - m_BoxHeight;	
}

void CCTCViewerDlg::DisplayBoxDescription(void)
{
	int width, height;

	width = (int) (10.0 / 0.648);
	height = (int) (10.0 / 0.648);
	int x0, y0;
	int red, green, blue;
	int redmax, greenmax, bluemax;
	GetAverageIntensity(&x0, &y0, width, height, &red, &green, &blue, &redmax, &greenmax, &bluemax);
	m_Description.Format(_T("CPI:RGB(%d,%d,%d),Radius=5(microns),Center(%d,%d),Intensity(mean,max)=R(%d,%d),G(%d,%d)[Circle]"), 
		m_RedIntensity, m_GreenIntensity, m_BlueIntensity, x0, y0, red, redmax, 
		green, greenmax, blue, bluemax);
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}

void CCTCViewerDlg::GetAverageIntensity(int *x0, int *y0, int width, int height, int *red, int *green, int *blue, int *redmax, int *greenmax, int *bluemax)
{
	*x0 = 0;
	*y0 = 0;
	*red = 0;
	*green = 0;
	*blue = 0;
	*redmax = 0;
	*greenmax = 0;
	*bluemax = 0;
	RECT rect;
	m_ImageDisplay.GetClientRect(&rect);
	double xRatio = ((double) m_RegionWidth) / (rect.right - rect.left);
	double yRatio = ((double)m_RegionHeight) / (rect.bottom - rect.top);
	*x0 = (int) (m_ROIRect.left * xRatio);
	int width1 = m_RegionWidth;
	*y0 = (int)(m_ROIRect.top * yRatio);
	UpdateData(TRUE);
	CalculateAverageIntensity(m_RedRegionImage, width1, *x0, *y0, width, height, RED_COLOR, red, redmax);
	CalculateAverageIntensity(m_GreenRegionImage, width1, *x0, *y0, width, height, GREEN_COLOR, green, greenmax);
	CalculateAverageIntensity(m_BlueRegionImage, width1, *x0, *y0, width, height, BLUE_COLOR, blue, bluemax);
}

void  CCTCViewerDlg::CalculateAverageIntensity(unsigned short *image, int width1, int x0, int y0, int width, int height,
	PIXEL_COLOR_TYPE color, int *average, int *max)
{
	int sum = 0;
	int max1 = 0;
	int count = 0;

	int xCenter = width / 2;
	int yCenter = height / 2;
	int radius2 = xCenter * xCenter;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			int dx = j - xCenter;
			dx *= dx;
			int dy = i - yCenter;
			dy *= dy;
			if ((dx + dy) < radius2)
			{
				unsigned short value = *(image + width1 * (i + y0) + (j + x0));
				sum += ((int)value);
				count++;
				if (((int)value) > max1)
					max1 = ((int)value);
			}
		}
	}

	*max = max1;
	*average = (int) (sum / count);
}

void CCTCViewerDlg::OnBnClickedSaveimage()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	BOOL ret = FALSE;
	CString filename;
	CString message;

	CFileDialog dlg(FALSE,    // save
		CString(".bmp"),    // no default extension
		NULL,    // no initial file name
		OFN_OVERWRITEPROMPT,
		_T("Image files (*.bmp)|*.bmp||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		filename = dlg.GetPathName();
		CString filename3 = dlg.GetFileName();
		BOOL ret = Save4BMPFiles(filename);
		if (ret)
		{
			message.Format(_T("4 BMP Files are saved."));
		}
		else
		{
			message.Format(_T("Failed to save 4 BMP Files."));
		}
	}
	else
	{
		message.Format(_T("Failed to specify a filename for saving image"));
	}
	m_Status = message;
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	m_Log.Message(message);
	return;
}

BOOL CCTCViewerDlg::SaveBMPImage(CString filename, CImage *image)
{
	BOOL ret = FALSE;

	CFile fileToSave;
	ret = fileToSave.Open(filename, CFile::modeCreate | CFile::modeWrite);
	if (ret)
	{
		BITMAPINFOHEADER BMIH;
		memset(&BMIH, 0, 40);
		BMIH.biSize = 40;
		BMIH.biSizeImage = image->GetPitch() * image->GetHeight();
		BMIH.biWidth = image->GetWidth();
		BMIH.biHeight = image->GetHeight();
		if (BMIH.biHeight > 0)
			BMIH.biHeight = -BMIH.biHeight;
		BMIH.biPlanes = 1;
		BMIH.biBitCount = 24;
		BMIH.biCompression = BI_RGB;

		BITMAPFILEHEADER bmfh;
		memset(&bmfh, 0, 14);
		int nBitsOffset = 14 + BMIH.biSize;
		LONG lImageSize = BMIH.biSizeImage;
		LONG lFileSize = nBitsOffset + lImageSize;

		bmfh.bfType = 'B' + ('M' << 8);
		bmfh.bfOffBits = nBitsOffset;
		bmfh.bfSize = lFileSize;
		bmfh.bfReserved1 = bmfh.bfReserved2 = 0;

		fileToSave.Write(&bmfh, 14);
		fileToSave.Write(&BMIH, 40);
		fileToSave.Write(image->GetBits(), lImageSize);
		fileToSave.Close();
	}

	return ret;
}

void CCTCViewerDlg::LoadRegionImageFile(int index, int mag)
{
	SaveTempRegionFile();
	CString message;

	int X0, Y0;
	if ((index >= 0) && (index < (int)m_RGNDataArray.size()))
	{
		FreeRegionImageData();
		m_RGNDataArray[index]->GetPosition(&X0, &Y0);
		m_RegionWidth = 1000 / mag;
		m_RegionHeight = 1000 / mag;
		m_RegionX0 = X0 + 50 - m_RegionWidth / 2;
		m_RegionY0 = Y0 + 50 - m_RegionHeight / 2;
		ResetBoxPos();
		m_RedIntensity = m_RedTIFFData->GetCPI();
		m_RedRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
		BOOL redRet = m_RedTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, m_RegionWidth, m_RegionHeight, m_RedRegionImage);
		m_GreenIntensity = m_GreenTIFFData->GetCPI();
		m_GreenRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
		BOOL greenRet = m_GreenTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, m_RegionWidth, m_RegionHeight, m_GreenRegionImage);
		m_BlueIntensity = m_BlueTIFFData->GetCPI();
		m_BlueRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
		BOOL blueRet = m_BlueTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, m_RegionWidth, m_RegionHeight, m_BlueRegionImage);
		if (redRet && greenRet && blueRet)
		{
			if (mag == 10)
				AutoScreen(index);
			DisplayROI(index + 1);
		}
		else
		{
			message.Format(_T("Failed to get Image Data for Region No.%d"), index + 1);
			m_Status = message;
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			m_Log.Message(message);
		}
	}
}

void CCTCViewerDlg::OnBnClickedRedreset()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	m_RedSlider.SetPos(120);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		DisplayROI(m_DisplayedRgnIndex);
}


void CCTCViewerDlg::OnBnClickedGreenreset()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	m_GreenSlider.SetPos(120);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		DisplayROI(m_DisplayedRgnIndex);
}


void CCTCViewerDlg::OnBnClickedBluereset()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	m_BlueSlider.SetPos(120);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		DisplayROI(m_DisplayedRgnIndex);
}


void CCTCViewerDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO:  在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值
	UpdateData(TRUE);
	RECT rect;
	m_ImageDisplay.GetClientRect(&rect);
	int x0 = 27;
	int y0 = 13;
		
	if ((m_DisplayedRgnIndex > 0) &&
				((point.x - x0) >= rect.left) &&
				((point.y - y0) >= rect.top) &&
				((point.x - x0) < rect.right) &&
				((point.y - y0) < rect.bottom))
	{
		if (m_UseIntensityBox.GetCheck() == BST_UNCHECKED)
		{
			double xRatio = ((double)m_RegionWidth) / (rect.right - rect.left);
			double yRatio = ((double)m_RegionHeight) / (rect.bottom - rect.top);
			int x = (int)((point.x - x0) * xRatio);
			int width1 = m_RegionWidth;
			int y = (int)((point.y - y0) * yRatio);
			unsigned short red = *(m_RedRegionImage + width1 * y + x);
			unsigned short green = *(m_GreenRegionImage + width1 * y + x);
			unsigned short blue = *(m_BlueRegionImage + width1 * y + x);
			m_Description.Format(_T("CPI:RGB(%d,%d,%d), Pos(%d,%d), Intensity:RGB(%u,%d,%d)"), m_RedIntensity, m_GreenIntensity, m_BlueIntensity, x, y, red, green, blue);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
		else if ((m_UseIntensityBox.GetCheck() == BST_CHECKED) &&
			((point.x - x0) < (rect.right - m_BoxWidth)) &&
			((point.y - y0) < (rect.bottom - m_BoxHeight)))
		{
			mouseIndex = 1;
			toggleFlag = !toggleFlag;
			m_CellScoreList.DeleteAllItems();
			m_ROIRect.left = point.x - x0 - m_BoxWidth / 2;;
			m_ROIRect.right = m_ROIRect.left + m_BoxWidth;
			m_ROIRect.top = point.y - y0 - m_BoxHeight / 2;
			m_ROIRect.bottom = m_ROIRect.top + m_BoxHeight;

				if (!toggleFlag)
				{
					unsigned short *blue = m_BlueRegionImage;
					unsigned short *green = m_GreenRegionImage;
					unsigned short *red = m_RedRegionImage;
					if (m_Image != NULL)
					{
						m_Image.Destroy();
						m_ImageWidth = 0;
						m_ImageHeight = 0;
					}
					m_ImageWidth = m_RegionWidth;
					m_ImageHeight = m_RegionHeight;
					m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

					CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), 
						&m_Image, m_RGNDataArray[m_DisplayedRgnIndex-1]);
					RECT rect1;
					m_ImageDisplay.GetWindowRect(&rect1);
					ScreenToClient(&rect1);
					InvalidateRect(&rect1, false);
					return;
				}
			CString labelStr;
			int itemIndex = 0;
			labelStr = _T("Base");
			int idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			labelStr = _T("50");
			m_CellScoreList.SetItemText(idx, 2, labelStr);
			int width, height;
			width = (int)(10.0 / 0.648);
			height = (int)(10.0 / 0.648);
			int x1, y1;
			int redAve, greenAve, blueAve;
			int redmax, greenmax, bluemax;
			GetAverageIntensity(&x1, &y1, width, height, &redAve, &greenAve, &blueAve, &redmax, &greenmax, &bluemax);
			int redValue = redAve - m_RedIntensity;
			int redScore = 0;
			CCTCParams *params = &m_CTCParams;
			if (redValue > params->m_CTCParams[PARAM_AVERAGECK20])
				redScore = 20;
			else if (redValue > params->m_CTCParams[PARAM_AVERAGECK15])
				redScore = 15;
			else if (redValue > params->m_CTCParams[PARAM_AVERAGECK10])
				redScore = 10;
			else if (redValue > params->m_CTCParams[PARAM_AVERAGECK5])
				redScore = 5;
			labelStr = _T("RedAvg");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			labelStr.Format(_T("%d"), redValue);
			m_CellScoreList.SetItemText(idx, 1, labelStr);
			labelStr.Format(_T("%d"), redScore);
			m_CellScoreList.SetItemText(idx, 2, labelStr);

			BYTE *map = new unsigned char[m_RegionWidth * m_RegionHeight];
			unsigned char *ptr = map;
			unsigned short *ptr1 = m_RedRegionImage;
			unsigned short *ptr2 = m_BlueRegionImage;
			unsigned short redThreshold = params->m_CTCParams[(int)PARAM_RED_THRESHOLD] + m_RedIntensity;
			unsigned short blueThreshold = params->m_CTCParams[(int)PARAM_BLUE_THRESHOLD] + m_BlueIntensity;
			int combinedPixelCount = 0;
			int bluePixelCount = 0;
			int redPixelCount = 0;
			double rr = width / 2;
			rr *= rr;
			for (int i = 0; i < m_RegionHeight; i++)
			{
				double yy = (i - y1);
				yy *= yy;
				for (int j = 0; j < m_RegionWidth; j++, ptr++, ptr1++, ptr2++)
				{
					double xx = (j - x1);
					xx *= xx;
					if ((xx + yy) <= rr)
					{
						*ptr = (unsigned char)255;
						combinedPixelCount++;
						if (*ptr1 >= redThreshold)
							redPixelCount++;
						if (*ptr2 >= blueThreshold)
							bluePixelCount++;
					}
					else
					{
						*ptr = (unsigned char)0;
					}
				}
			}
			int cellSize = getCellSize(map, m_RegionWidth, m_RegionHeight);
			
			labelStr = _T("CellSize");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			float cellSizeValue = (float)(cellSize * 0.648);
			labelStr.Format(_T("%.1f"), cellSizeValue);
			m_CellScoreList.SetItemText(idx, 1, labelStr);

			int cellSizeScore = 0;
			if (cellSizeValue >= params->m_CTCParams[PARAM_CELLSIZE0])
				cellSizeScore = 0;
			else if (cellSizeValue >= params->m_CTCParams[PARAM_CELLSIZE20])
				cellSizeScore = 20;
			else if (cellSizeValue >= params->m_CTCParams[PARAM_CELLSIZE15])
				cellSizeScore = 15;
			else if (cellSizeValue >= params->m_CTCParams[PARAM_CELLSIZE10])
				cellSizeScore = 10;
			else if (cellSizeValue >= params->m_CTCParams[PARAM_CELLSIZE_5])
				cellSizeScore = 0;
			else if (cellSizeValue < params->m_CTCParams[PARAM_CELLSIZE_10])
				cellSizeScore = -10;
			else if (cellSizeValue < params->m_CTCParams[PARAM_CELLSIZE_5])
				cellSizeScore = -5;
			labelStr.Format(_T("%d"), cellSizeScore);
			m_CellScoreList.SetItemText(idx, 2, labelStr);

			labelStr = _T("Blue Count");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			labelStr.Format(_T("%d"), bluePixelCount);
			m_CellScoreList.SetItemText(idx, 1, labelStr);

			labelStr = _T("Red Count");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			labelStr.Format(_T("%d"), redPixelCount);
			m_CellScoreList.SetItemText(idx, 1, labelStr);

			labelStr = _T("N/C Ratio");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			float ncRatio = 0.0;
			int ncScore = 0;
			if (redPixelCount > 0)
			{
				float ncRatio = 0;

				if (redPixelCount > 0)
					ncRatio = (float)(100.0 * ((double)bluePixelCount) / ((double)redPixelCount));

				if (ncRatio >= params->m_CTCParams[PARAM_NCRATIO0])
					ncScore = -10;
				else if (ncRatio >= (params->m_CTCParams[PARAM_NCRATIO0] - 1))
					ncScore = 0;
				else if (ncRatio > params->m_CTCParams[PARAM_NCRATIO15])
					ncScore = 15;
				else if (ncRatio > params->m_CTCParams[PARAM_NCRATIO10])
					ncScore = 10;
				else if (ncRatio > params->m_CTCParams[PARAM_NCRATIO5])
					ncScore = 5;
				else if (ncRatio >= params->m_CTCParams[PARAM_NCRATIO_5])
					ncScore = 0;
				else if (ncRatio < params->m_CTCParams[PARAM_NCRATIO_15])
					ncScore = -15;
				else if (ncRatio < params->m_CTCParams[PARAM_NCRATIO_10])
					ncScore = -10;
				else if (ncRatio < params->m_CTCParams[PARAM_NCRATIO_5])
					ncScore = -5;
				labelStr.Format(_T("%.1f"), ncRatio);
				m_CellScoreList.SetItemText(idx, 1, labelStr);
				labelStr.Format(_T("%d"), ncScore);
				m_CellScoreList.SetItemText(idx, 2, labelStr);
			}

			int greenValue = greenAve - m_GreenIntensity;
			int greenScore = 0;
			if (greenValue > params->m_CTCParams[PARAM_CD45AVERAGE25])
				greenScore = -25;
			else if (greenValue > params->m_CTCParams[PARAM_CD45AVERAGE20])
				greenScore = -20;
			else if (greenValue > params->m_CTCParams[PARAM_CD45AVERAGE15])
				greenScore = -15;
			else if (greenValue > params->m_CTCParams[PARAM_CD45AVERAGE10])
				greenScore = -10;
			else if (greenValue > params->m_CTCParams[PARAM_CD45AVERAGE5])
				greenScore = -5;
			labelStr = _T("GreenAvg");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			labelStr.Format(_T("%d"), greenValue);
			m_CellScoreList.SetItemText(idx, 1, labelStr);
			labelStr.Format(_T("%d"), greenScore);
			m_CellScoreList.SetItemText(idx, 2, labelStr);

			labelStr = _T("Ring");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			labelStr.Format(_T("%d"), 0);
			m_CellScoreList.SetItemText(idx, 1, labelStr);

			labelStr = _T("%Brighter");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);

			double cValue = 0.0;
			labelStr.Format(_T("%.1f"), cValue);
			m_CellScoreList.SetItemText(idx, 1, labelStr);
			int cScore = 0;
			if (cValue > params->m_CTCParams[PARAM_CD45RING25])
				cScore = -25;
			else if (cValue > params->m_CTCParams[PARAM_CD45RING20])
				cScore = -20;
			else if (cValue > params->m_CTCParams[PARAM_CD45RING15])
				cScore = -15;
			else if (cValue > params->m_CTCParams[PARAM_CD45RING10])
				cScore = -10;
			else if (cValue > params->m_CTCParams[PARAM_CD45RING5])
				cScore = -5;
			labelStr.Format(_T("%d"), cScore);
			m_CellScoreList.SetItemText(idx, 2, labelStr);

			labelStr = _T("Total");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			int total = 50 + redScore + cellSizeScore + ncScore + greenScore + cScore;
			labelStr.Format(_T("%d"), total);
			m_CellScoreList.SetItemText(idx, 2, labelStr);
						
			delete map;
			/* CTC Score Calculation End */
			unsigned short *blue = m_BlueRegionImage;
			unsigned short *green = m_GreenRegionImage;
			unsigned short *red = m_RedRegionImage;
			if (m_Image != NULL)
			{
				m_Image.Destroy();
				m_ImageWidth = 0;
				m_ImageHeight = 0;
			}
			m_ImageWidth = m_RegionWidth;
			m_ImageHeight = m_RegionHeight;
			m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

			//((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), &m_Image);
			RECT rect1;
			m_ImageDisplay.GetWindowRect(&rect1);
			ScreenToClient(&rect1);
			InvalidateRect(&rect1, false);
		}
	}
	CDialogEx::OnLButtonDown(nFlags, point);
}

void CCTCViewerDlg::CountColorCode(void)
{
	m_ConfirmedCTCNum = 0;
	m_ConfirmedNonCTCNum = 0;
	m_CTCNum = 0;
	for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
	{
		unsigned int color = m_RGNDataArray[i]->GetColorCode();
		switch (color)
		{
		case CTC:
			m_CTCNum++;
			break;
		case CTC2:
			m_ConfirmedCTCNum++;
			break;
		case NONCTC:
			m_ConfirmedNonCTCNum++;
			break;
		}
	}
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}

void CCTCViewerDlg::SaveTempRegionFile(void)
{
	CString message;
	CString filenameForSave = _T("C:\\CTCReviewerLog\\temp.rgn");
	CStdioFile theFile;

	if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
		{
			CString singleLine;
			int x0, y0;
			m_RGNDataArray[i]->GetPosition(&x0, &y0);
			singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
				m_RGNDataArray[i]->GetColorCode(), x0, y0, i + 1);
			theFile.WriteString(singleLine);
		}
		theFile.Close();
		message.Format(_T("%s Saved. DisplayedRgnIndex=%d"), filenameForSave, m_DisplayedRgnIndex);
		m_Log.Message(message);
	}
	else
	{
		message.Format(_T("Failed to Open %s to save. DisplayedRgnIndex=%d"), filenameForSave, m_DisplayedRgnIndex),
			m_Log.Message(message);
	}
}

void CCTCViewerDlg::OnBnClickedRedfile()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	const CString LEICA_RED_FILEPREFIX = _T("TRITC-Rhoadmine Selection");
	const CString LEICA_GREEN_FILEPREFIX = _T("FITC Selection");
	const CString LEICA_BLUE_FILEPREFIX = _T("DAPI Selection");
	const CString ZEISS_RED_POSTFIX = _T("_c3_ORG.tif");
	const CString ZEISS_GREEN_POSTFIX = _T("_c4_ORG.tif");
	const CString ZEISS_BLUE_POSTFIX = _T("_c1_ORG.tif");
	FreeRGNData(&m_RGNDataArray);
	m_CellScoreList.DeleteAllItems();
	m_CTCCount = 0;
	m_DisplayedRgnIndex = 0;
	m_CTCIndex.SetWindowTextW(_T("0"));
	m_ConfirmedCTCNum = 0;
	m_ConfirmedNonCTCNum = 0;
	m_CTCNum = 0;
	m_RGNFilename = _T("");
	m_TIFFFilename = _T("");
	m_GreenFilename = _T("");
	m_BlueFilename = _T("");
	m_CTCScoreIndex = 1;
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	m_Status = _T("Start to load Red Channel TIFF File. Please wait...");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_TIFFFilename = dlg.GetFileName();
		BOOL ret = m_RedTIFFData->LoadRawTIFFFile(filename);
		if (ret)
		{
			CString message;
			message.Format(_T("Red TIFF File %s loaded successfully, CPI=%d"), filename, m_RedTIFFData->GetCPI());
			m_Log.Message(message);
			if (m_Load3Channels.GetCheck() == BST_UNCHECKED)
			{
				m_Status = _T("Please load Green Channel TIFF File");
			}
			else
			{
				int index = m_TIFFFilename.Find(LEICA_RED_FILEPREFIX, 0);
				if (index == -1)
				{
					int postfixIndex = filename.Find(ZEISS_RED_POSTFIX);
					if (postfixIndex == -1)
					{
						m_Status.Format(_T("Red TIFF Filename %s doesn't have PREFIX %s or POSTFIX %s"), m_TIFFFilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
						return;
					}
					else
					{
						CString samplePathName = filename.Mid(0, postfixIndex);
						postfixIndex = m_TIFFFilename.Find(ZEISS_RED_POSTFIX);
						CString sampleName = m_TIFFFilename.Mid(0, postfixIndex);
						CString filename2 = samplePathName + ZEISS_GREEN_POSTFIX;
						m_GreenFilename = sampleName + ZEISS_GREEN_POSTFIX;
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
						BOOL ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
						if (ret)
						{
							filename2 = samplePathName + ZEISS_BLUE_POSTFIX;
							m_BlueFilename = sampleName + ZEISS_BLUE_POSTFIX;
							::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
							BOOL ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
							if (ret)
							{
								message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
								m_CTCParams.LoadCTCParams(message);
								m_Status = _T("Please load RGN File.");
								::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
								return;
							}
							else
							{
								m_Status = _T("Failed to load Blue TIFF File");
								::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
								return;
							}
						}
						else
						{
							m_Status = _T("Failed to load Green TIFF File");
							::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
							return;
						}
					}
				}
				else
				{
					CString postfix = m_TIFFFilename.Mid(LEICA_RED_FILEPREFIX.GetLength());
					index = filename.Find(LEICA_RED_FILEPREFIX, 0);
					CString filename1 = filename.Mid(0, index);
					CString filename2;
					filename2.Format(_T("%s%s%s"), filename1, LEICA_GREEN_FILEPREFIX, postfix);
					m_GreenFilename.Format(_T("%s%s"), LEICA_GREEN_FILEPREFIX, postfix);
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
					if (!ret)
					{
						m_Status.Format(_T("Failed to load %s%s"), LEICA_GREEN_FILEPREFIX, postfix);
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					}
					else
					{
						message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
						m_Log.Message(message);
						filename1 = filename.Mid(0, index);
						filename2.Format(_T("%s%s%s"), filename1, LEICA_BLUE_FILEPREFIX, postfix);
						m_BlueFilename.Format(_T("%s%s"), LEICA_BLUE_FILEPREFIX, postfix);
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
						ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
						if (!ret)
						{
							m_Status.Format(_T("Failed to load %s%s"), LEICA_BLUE_FILEPREFIX, filename.Mid(index + LEICA_RED_FILEPREFIX.GetLength()));
							::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
						}
						else
						{
							message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
							m_CTCParams.LoadCTCParams(message);
							message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
							m_Log.Message(message);
							m_Status = _T("Please load RGN File.");
						}
					}
				}
			}
		}
		else
			m_Status = _T("Failed to load Red Channel TIFF File.");
	}
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}


void CCTCViewerDlg::OnBnClickedGreenfile()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	m_Status = _T("Start to load Green Channel TIFF File. Please wait...");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_GreenFilename = dlg.GetFileName();
		BOOL ret = m_GreenTIFFData->LoadRawTIFFFile(filename);
		if (ret)
		{
			int width1 = m_RedTIFFData->GetImageWidth();
			int height1 = m_RedTIFFData->GetImageHeight();
			int width2 = m_GreenTIFFData->GetImageWidth();
			int height2 = m_GreenTIFFData->GetImageHeight();
			if ((width1 != width2) || (height1 != height2))
			{
				m_Status.Format(_T("RedWidth(%d) != GreenWidth(%d), or RedHeight(%d) != GreenHeight(%d)"), width1, width2, height1, height2);
			}
			else
			{
				CString message;
				message.Format(_T("Green TIFF File %s loaded successfully, CPI=%d"), filename, m_GreenTIFFData->GetCPI());
				m_Log.Message(message);
				m_Status = _T("Please load Blue Channel TIFF File");
			}
		}
		else
			m_Status = _T("Failed to load Green Channel TIFF File.");
	}
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}


void CCTCViewerDlg::OnBnClickedBluefile()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	m_Status = _T("Start to load Blue Channel TIFF File. Please wait...");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_BlueFilename = dlg.GetFileName();
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		BOOL ret = m_BlueTIFFData->LoadRawTIFFFile(filename);
		if (ret)
		{
			int width1 = m_RedTIFFData->GetImageWidth();
			int height1 = m_RedTIFFData->GetImageHeight();
			int width2 = m_BlueTIFFData->GetImageWidth();
			int height2 = m_BlueTIFFData->GetImageHeight();
			if ((width1 != width2) || (height1 != height2))
			{
				m_Status.Format(_T("RedWidth(%d) != BlueWidth(%d), or RedHeight(%d) != BlueHeight(%d)"), width1, width2, height1, height2);
			}
			else
			{
				CString message;
				if (m_BlueTIFFData->IsZeissData())
				{
					message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
					m_CTCParams.LoadCTCParams(message);
				}
				else
				{
					message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
					m_CTCParams.LoadCTCParams(message);
				}
				message.Format(_T("Blue TIFF File %s loaded successfully, CPI=%d"), filename, m_BlueTIFFData->GetCPI());
				m_Log.Message(message);
				m_Status = _T("Please load RGN File.");
			}
		}
		else
			m_Status = _T("Failed to load Blue Channel TIFF File.");
	}
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}


void CCTCViewerDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO:  在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值
	UpdateData(TRUE);
	mouseIndex = 2;
	m_CellScoreList.DeleteAllItems();
	unsigned short *blue = m_BlueRegionImage;
	unsigned short *green = m_GreenRegionImage;
	unsigned short *red = m_RedRegionImage;
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_ImageWidth = 0;
		m_ImageHeight = 0;
	}
	m_ImageWidth = m_RegionWidth;
	m_ImageHeight = m_RegionHeight;
	m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);
	CRGNData *rgnPtr = m_RGNDataArray[m_DisplayedRgnIndex - 1];
	m_HitFinder.FreeBlobList(rgnPtr->GetBlobData(RED_COLOR));
	m_HitFinder.FreeBlobList(rgnPtr->GetBlobData(GREEN_COLOR));
	m_HitFinder.FreeBlobList(rgnPtr->GetBlobData(BLUE_COLOR));
	m_HitFinder.FreeBlobList(rgnPtr->GetBlobData(GB_COLORS));
	CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), 
		&m_Image, m_RGNDataArray[m_DisplayedRgnIndex-1]);
	RECT rect1;
	m_ImageDisplay.GetWindowRect(&rect1);
	ScreenToClient(&rect1);
	InvalidateRect(&rect1, false);
	toggleFlag = !toggleFlag;
	if (!toggleFlag)
	{
		return;
	}

	RECT rect;
	m_ImageDisplay.GetClientRect(&rect);
	int x0 = 27;
	int y0 = 13;
	if ((m_DisplayedRgnIndex > 0) &&
		(m_RegionWidth == 100) && (m_RegionHeight == 100) &&
		((point.x - x0) >= rect.left) &&
		((point.y - y0) >= rect.top) &&
		((point.x - x0) < rect.right) &&
		((point.y - y0) < rect.bottom))
	{
		CRGNData *ptr = m_RGNDataArray[m_DisplayedRgnIndex - 1];
		ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
		double xRatio = ((double)m_RegionWidth) / (rect.right - rect.left);
		double yRatio = ((double)m_RegionHeight) / (rect.bottom - rect.top);
		int x = (int)((point.x - x0) * xRatio);
		int y = (int)((point.y - y0) * yRatio);
		ptr->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
		ptr->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
		ptr->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
		ptr->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
		ptr->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
		ptr->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
		ptr->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
		ptr->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
		ptr->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
		ptr->SetCPI(RED_COLOR, m_RedIntensity);
		ptr->SetCPI(GREEN_COLOR, m_GreenIntensity);
		ptr->SetCPI(BLUE_COLOR, m_BlueIntensity);
		unsigned int color = ptr->GetColorCode();
		vector<CRGNData *> cellList;
		m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
		if (cellList.size() > 0)
		{
			m_HitFinder.GetCenterCell(ptr, &cellList, x, y, &m_CellScoreList, m_DisplayedRgnIndex);
			FreeRGNData(&cellList);
		}
		else
		{
			ptr->SetScore(0);
		}
		ptr->SetColorCode(color);
		ptr->NullImages();
		blue = m_BlueRegionImage;
		green = m_GreenRegionImage;
		red = m_RedRegionImage;
		if (m_Image != NULL)
		{
			m_Image.Destroy();
			m_ImageWidth = 0;
			m_ImageHeight = 0;
		}
		m_ImageWidth = m_RegionWidth;
		m_ImageHeight = m_RegionHeight;
		m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);
		CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), &m_Image, ptr);
		RECT rect1;
		m_ImageDisplay.GetWindowRect(&rect1);
		ScreenToClient(&rect1);
		InvalidateRect(&rect1, false);
	}
	CDialogEx::OnRButtonDown(nFlags, point);
}

void CCTCViewerDlg::OnBnClickedResetcontrast()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	m_BlueSlider.SetPos(120);
	m_GreenSlider.SetPos(120);
	m_RedSlider.SetPos(120);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		DisplayROI(m_DisplayedRgnIndex);
}

int CCTCViewerDlg::getCellSize(BYTE *image, int width, int height)
{
	int size = 0;
	int maxI = 0;
	int minI = height;
	int maxJ = 0;
	int minJ = width;

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (image[width * i + j] > 0)
			{
				if (i > maxI)
					maxI = i;
				if (i < minI)
					minI = i;
				if (j > maxJ)
					maxJ = j;
				if (j < minJ)
					minJ = j;
			}
		}
	}

	if ((maxI - minI) > (maxJ - minJ))
	{
		size = maxI - minI;
	}
	else
	{
		size = maxJ - minJ;
	}
	return size;
}

void CCTCViewerDlg::Get8BitImageData(CImage *rgb, CImage *red, CImage *green, CImage *blue)
{
	BYTE *pCursor = (BYTE*)rgb->GetBits();
	BYTE *pRedCursor = (BYTE*)red->GetBits();
	BYTE *pGreenCursor = (BYTE*)green->GetBits();
	BYTE *pBlueCursor = (BYTE*)blue->GetBits();
	int nHeight = m_ImageHeight;
	int nWidth = m_ImageWidth;
	int BlueMax = m_BlueSlider.GetPos();
	int GreenMax = m_GreenSlider.GetPos();
	int RedMax = m_RedSlider.GetPos();
	int nStride = rgb->GetPitch() - (nWidth * 3);
	UpdateData(TRUE);
	unsigned short *pBlueBuffer = m_BlueRegionImage;
	unsigned short *pGreenBuffer = m_GreenRegionImage;
	unsigned short *pRedBuffer = m_RedRegionImage;

	for (int y = 0; y<nHeight; y++)
	{
		for (int x = 0; x<nWidth; x++)
		{
			BYTE redValue = GetContrastEnhancedByte(*pRedBuffer++, RedMax, RED_COLOR);
			BYTE greenValue = GetContrastEnhancedByte(*pGreenBuffer++, GreenMax, GREEN_COLOR);
			BYTE blueValue = GetContrastEnhancedByte(*pBlueBuffer++, BlueMax, BLUE_COLOR);
			*pCursor++ = blueValue;
			*pCursor++ = greenValue;
			*pCursor++ = redValue;
			*pBlueCursor++ = blueValue;
			*pBlueCursor++ = (BYTE) 0;
			*pBlueCursor++ = (BYTE) 0;
			*pGreenCursor++ = (BYTE) 0;
			*pGreenCursor++ = greenValue;
			*pGreenCursor++ = (BYTE) 0;
			*pRedCursor++ = (BYTE) 0;
			*pRedCursor++ = (BYTE) 0;
			*pRedCursor++ = redValue;
					
		}
		if (nStride > 0)
		{
			pCursor += nStride;
			pBlueCursor += nStride;
			pGreenCursor += nStride;
			pRedCursor += nStride;
		}
	}
}

BOOL CCTCViewerDlg::Save4BMPFiles(CString filename)
{
	BOOL ret = FALSE;
	CImage rgb;
	CImage red;
	CImage green;
	CImage blue;

	rgb.Create(m_RegionWidth, -m_RegionHeight, 24);
	red.Create(m_RegionWidth, -m_RegionHeight, 24);
	green.Create(m_RegionWidth, -m_RegionHeight, 24);
	blue.Create(m_RegionWidth, -m_RegionHeight, 24);
	Get8BitImageData(&rgb, &red, &green, &blue);

	char filename1[512];
	size_t size;
	wcstombs_s(&size, filename1, filename.GetBuffer(), filename.GetLength());
	char *ptr = strstr(filename1, ".bmp");
	if (ptr != NULL)
	{
		*ptr = '\0';
		CString pathname = CString(filename1);
		CString pathname1;
		pathname1.Format(_T("%s_RGB.bmp"), pathname);
		SaveBMPImage(pathname1, &rgb);
		pathname1.Format(_T("%s_RED.bmp"), pathname);
		SaveBMPImage(pathname1, &red);
		pathname1.Format(_T("%s_GREEN.bmp"), pathname);
		SaveBMPImage(pathname1, &green);
		pathname1.Format(_T("%s_BLUE.bmp"), pathname);
		SaveBMPImage(pathname1, &blue);
		ret = TRUE;
	}
	return ret;
}

void CCTCViewerDlg::OnBnClickedUsebox()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	mouseIndex = 0;
	m_CellScoreList.DeleteAllItems();
		
	m_Description = _T("");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	unsigned short *blue = m_BlueRegionImage;
	unsigned short *green = m_GreenRegionImage;
	unsigned short *red = m_RedRegionImage;
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_ImageWidth = 0;
		m_ImageHeight = 0;
	}
	m_ImageWidth = m_RegionWidth;
	m_ImageHeight = m_RegionHeight;
	m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

	CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), &m_Image,
		m_RGNDataArray[m_DisplayedRgnIndex-1]);
	RECT rect1;
	m_ImageDisplay.GetWindowRect(&rect1);
	ScreenToClient(&rect1);
	InvalidateRect(&rect1, false);
}


void CCTCViewerDlg::OnBnClickedReload()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CFileDialog dlg(TRUE,    // open
		CString(".txt"),    // with default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("CTCParam files (*.txt)|*.txt||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_CTCParams.LoadCTCParams(dlg.GetPathName());
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

void CCTCViewerDlg::OnBnClickedSavedata()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	CFileDialog dlg(FALSE,    // save
		CString(".txt"),    // with default extension
		NULL,    // no initial file name
		OFN_OVERWRITEPROMPT,
		_T("CTCParam files (*.txt)|*.txt||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_CTCParams.SaveCTCParams(dlg.GetPathName());
	}
}

void CCTCViewerDlg::OnBnClickedSavehit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CString message;

	CFileDialog dlg(FALSE,    // open
		CString(".hit"),
		NULL,    // no initial file name
		OFN_OVERWRITEPROMPT,
		_T("Hits Image Data files (*.hit)|*.hit||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		CFile theFile;
		if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite))
		{
			CArchive archive(&theFile, CArchive::store);
			archive << (int)1; // Version Number
			int count = 0;
			for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
			{
				unsigned int color = m_RGNDataArray[i]->GetColorCode();
				if ((color == CTC) || (color == CTC2))
					count++;
			}
			archive << count;
			archive << m_RedTIFFData->GetCPI();
			archive << m_GreenTIFFData->GetCPI();
			archive << m_BlueTIFFData->GetCPI();
			bool status = false;
			for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
			{
				CSingleRegionImageData *data = new CSingleRegionImageData();
				CRGNData *ptr = m_RGNDataArray[i];
				unsigned int color = ptr->GetColorCode();
				if ((color == CTC) || (color == CTC2))
				{
					data->m_HitIndex = i;
					int x0, y0;
					ptr->GetPosition(&x0, &y0);
					data->m_RegionX0Pos = x0;
					data->m_RegionY0Pos = y0;
					data->m_RegionWidth = ptr->GetWidth();
					data->m_RegionHeight = ptr->GetHeight();
					data->m_RedRegionImage = new unsigned short[data->m_RegionWidth * data->m_RegionHeight];
					status = m_RedTIFFData->GetImageRegion(data->m_RegionX0Pos, data->m_RegionY0Pos,
						data->m_RegionWidth, data->m_RegionHeight, data->m_RedRegionImage);
					if (status)
					{
						data->m_GreenRegionImage = new unsigned short[data->m_RegionWidth * data->m_RegionHeight];
						status = m_GreenTIFFData->GetImageRegion(data->m_RegionX0Pos, data->m_RegionY0Pos,
							data->m_RegionWidth, data->m_RegionHeight, data->m_GreenRegionImage);
						if (status)
						{
							data->m_BlueRegionImage = new unsigned short[data->m_RegionWidth * data->m_RegionHeight];
							status = m_BlueTIFFData->GetImageRegion(data->m_RegionX0Pos, data->m_RegionY0Pos,
								data->m_RegionWidth, data->m_RegionHeight, data->m_BlueRegionImage);
							if (status)
								data->Serialize(archive);
						}
					}
					else
					{
						message.Format(_T("Failed to save Hit No.%d image data"), i + 1);
						m_Log.Message(message);
						break;
					}
				}
				delete data;
			}
			archive.Close();
			theFile.Close();
			if (status)
			{
				message.Format(_T("Saved %d Hits to Hit Archive File %s"), count, filename);
				m_Log.Message(message);
			}
			int index = filename.Find(_T(".hit"));
			if (index != -1)
			{
				filename = filename.Mid(0, index) + _T(".csv");
				m_HitFinder.DumpRegionDataToCSVFile(filename, &m_RGNDataArray, false);
			}
		}
		else
		{
			message.Format(_T("Failed to open %s"), filename);
			m_Status = message;
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			m_Log.Message(message);
		}
	}
	else
	{
		m_Status = _T("OpenFileDialog returned FALSE");
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

void CCTCViewerDlg::AutoScreen(int index)
{
	m_CellScoreList.DeleteAllItems();
	FreeRegionImageData();
	m_RegionWidth = 100;
	m_RegionHeight = 100;
	m_RedRegionImage = new unsigned short[sizeof(unsigned short) * m_RegionWidth * m_RegionHeight];
	m_GreenRegionImage = new unsigned short[sizeof(unsigned short) * m_RegionWidth * m_RegionHeight];
	m_BlueRegionImage = new unsigned short[sizeof(unsigned short) * m_RegionWidth * m_RegionHeight];
	CRGNData *ptr = m_RGNDataArray[index];
	int x0, y0;
	ptr->GetPosition(&x0, &y0);
	m_RegionX0 = x0;
	m_RegionY0 = y0;
	m_RedTIFFData->GetImageRegion(x0, y0, m_RegionWidth, m_RegionHeight, m_RedRegionImage);
	m_GreenTIFFData->GetImageRegion(x0, y0, m_RegionWidth, m_RegionHeight, m_GreenRegionImage);
	m_BlueTIFFData->GetImageRegion(x0, y0, m_RegionWidth, m_RegionHeight, m_BlueRegionImage);
	ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
	ptr->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
	ptr->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
	ptr->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
	ptr->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
	ptr->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
	ptr->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
	ptr->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
	ptr->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
	ptr->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
	ptr->SetCPI(RED_COLOR, m_RedIntensity);
	ptr->SetCPI(GREEN_COLOR, m_GreenIntensity);
	ptr->SetCPI(BLUE_COLOR, m_BlueIntensity);
	unsigned int color = ptr->GetColorCode();
	vector<CRGNData *> cellList;
	m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
	if (cellList.size() > 0)
	{
		m_HitFinder.GetCenterCell(ptr, &cellList, -1, -1, &m_CellScoreList, index + 1);
		FreeRGNData(&cellList);
	}
	else
	{
		ptr->SetScore(0);
	}
	ptr->SetColorCode(color);
	ptr->NullImages();
}

int CCTCViewerDlg::GetNextCTCScoreIndex()
{
	int index = 1;
	int CTCScoreIndex = m_CTCScoreIndex + 1;
	if (CTCScoreIndex > (int)m_RGNDataArray.size())
		CTCScoreIndex = (int)m_RGNDataArray.size();
	for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
	{
		if (m_RGNDataArray[i]->GetScoreIndex() == CTCScoreIndex)
		{
			m_CTCScoreIndex = CTCScoreIndex;
			index = i + 1;
			break;
		}
	}
	CString message;
	message.Format(_T("exit GetNextCTCScoreIndex(): m_DisplayedRgnIndex=%d, m_CTCScoreIndex=%d"), index, m_CTCScoreIndex);
	m_Log.Message(message);
	return index;
}

int CCTCViewerDlg::GetPrevCTCScoreIndex()
{
	int index = 1;
	int CTCScoreIndex = m_CTCScoreIndex - 1;
	if (CTCScoreIndex < 0)
		CTCScoreIndex = 1;
	for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
	{
		if (m_RGNDataArray[i]->GetScoreIndex() == CTCScoreIndex)
		{
			m_CTCScoreIndex = CTCScoreIndex;
			index = i + 1;
			break;
		}
	}
	CString message;
	message.Format(_T("exit GetPrevCTCScoreIndex(): m_DisplayedRgnIndex=%d, m_CTCScoreIndex=%d"), index, m_CTCScoreIndex);
	m_Log.Message(message);
	return index;
}

void CCTCViewerDlg::FreeRegionImageData()
{
	if (m_RedRegionImage != NULL)
	{
		delete[] m_RedRegionImage;
		m_RedRegionImage = NULL;
	}
	if (m_GreenRegionImage != NULL)
	{
		delete[] m_GreenRegionImage;
		m_GreenRegionImage = NULL;
	}
	if (m_BlueRegionImage != NULL)
	{
		delete[] m_BlueRegionImage;
		m_BlueRegionImage = NULL;
	}
	m_RegionX0 = 0;
	m_RegionY0 = 0;
	m_RegionWidth = 0;
	m_RegionHeight = 0;
}

static DWORD WINAPI SortByScoreOperation(LPVOID param)
{
	CCTCViewerDlg *ptr = (CCTCViewerDlg *)param;
	ptr->SortbyScore(ptr);
	return 0;
}

void CCTCViewerDlg::SortbyScore(CCTCViewerDlg *dlg)
{
	FreeRegionImageData();
	m_RegionWidth = 100;
	m_RegionHeight = 100;
	m_RedRegionImage = new unsigned short[sizeof(unsigned short) * m_RegionWidth * m_RegionHeight];
	m_GreenRegionImage = new unsigned short[sizeof(unsigned short) * m_RegionWidth * m_RegionHeight];
	m_BlueRegionImage = new unsigned short[sizeof(unsigned short) * m_RegionWidth * m_RegionHeight];

	for (int i = 0; i < m_CTCCount; i++)
	{
		CRGNData *data = m_RGNDataArray[i];
		int x0, y0;
		data->GetPosition(&x0, &y0);
		m_RegionX0 = x0;
		m_RegionY0 = y0;
		m_RedTIFFData->GetImageRegion(x0, y0, m_RegionWidth, m_RegionHeight, m_RedRegionImage);
		m_GreenTIFFData->GetImageRegion(x0, y0, m_RegionWidth, m_RegionHeight, m_GreenRegionImage);
		m_BlueTIFFData->GetImageRegion(x0, y0, m_RegionWidth, m_RegionHeight, m_BlueRegionImage);
		data->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
		data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
		data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
		data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
		data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
		data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
		data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
		data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
		data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
		data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
		data->SetCPI(RED_COLOR, m_RedIntensity);
		data->SetCPI(GREEN_COLOR, m_GreenIntensity);
		data->SetCPI(BLUE_COLOR, m_BlueIntensity);
		unsigned int color = data->GetColorCode();
		vector<CRGNData *> cellList;
		m_HitFinder.ProcessOneROI(&m_CTCParams, data, &cellList);
		if (cellList.size() > 0)
		{
			m_HitFinder.GetCenterCell(data, &cellList, -1, -1, NULL, i + 1);
			FreeRGNData(&cellList);
		}
		else
		{
			data->SetScore(0);
		}
		data->SetColorCode(color);
		data->NullImages();
	}

	if (m_CTCCount > 0)
	{
		for (int i = 0; i < m_CTCCount; i++)
		{
			int maxScore = -1;
			int maxIndex = -1;
			for (int j = 0; j < m_CTCCount; j++)
			{
				CRGNData *ptr = m_RGNDataArray[j];
				if (ptr->GetScoreIndex() == 0)
				{
					if (ptr->GetScore() > maxScore)
					{
						maxScore = ptr->GetScore();
						maxIndex = j;
					}
				}
			}
			m_RGNDataArray[maxIndex]->SetScoreIndex(i + 1);
		}
		m_DisplayedRgnIndex = 1;
		if (m_CTCScore.GetCheck() == BST_CHECKED)
		{
			m_CTCScoreIndex = 0;
			m_DisplayedRgnIndex = GetNextCTCScoreIndex();
		}
		else if (m_CTCOnly.GetCheck() == BST_CHECKED) 
		{
			int index1 = 0;
			m_DisplayedRgnIndex = 1;
			if (GetNextIndex(&index1))
			{
				m_DisplayedRgnIndex = index1 + 1;
			}
			else
				m_DisplayedRgnIndex = 1;
		}
		else if (m_WBCOnly.GetCheck() == BST_CHECKED)
		{
			int index1 = 0;
			m_DisplayedRgnIndex = 1;
			if (GetNextIndex(&index1))
			{
				m_DisplayedRgnIndex = index1 + 1;
			}
			else
				m_DisplayedRgnIndex = 1;
		}
		CString indexStr;
		if (m_CTCScore.GetCheck() == BST_CHECKED)
			indexStr.Format(_T("%d"), m_CTCScoreIndex);
		else
			indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_CTCIndex.SetWindowTextW(indexStr);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ResetSlider();
		if ((m_DisplayedRgnIndex > 0) && ((m_DisplayedRgnIndex - 1) < (int)m_RGNDataArray.size()) && (m_RGNDataArray[m_DisplayedRgnIndex - 1] != NULL))
		{
			LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
		}
	}
}

LRESULT CCTCViewerDlg::OnMyMessage(WPARAM wparam, LPARAM lparam)
{
	UpdateData(FALSE);
	return 0;
}

int CCTCViewerDlg::GetDisplayIndexFromScoreIndex(int scoreIndex)
{
	int index = -1;
	for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
	{
		if (m_RGNDataArray[i]->GetScoreIndex() == scoreIndex)
		{
			index = i + 1;
			break;
		}
	}
	return index;
}

int CCTCViewerDlg::GetScoreIndexFromDisplayIndex(int displayIndex)
{
	int index = displayIndex - 1;
	if ((index >= 0) && (index < m_CTCCount))
	{
		return m_RGNDataArray[index]->GetScoreIndex();
	}
	else
	{
		return -1;
	}
}
