
// AutoScanDlg.cpp : 實作檔
//

#include "stdafx.h"
#include "AutoScan.h"
#include "AutoScanDlg.h"
#include "afxdialogex.h"
#include "VersionNumber.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_MY_MESSAGE (WM_USER+1001)
#define ENABLE_MESSAGE (WM_USER+1002)

static DWORD WINAPI BatchOperation(LPVOID param);

// CAutoScanDlg 對話方塊
CAutoScanDlg::CAutoScanDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAutoScanDlg::IDD, pParent)
	, m_RedFileName(_T(""))
	, m_Status(_T(""))
	, m_CellRegionIdx(0)
	, m_RGNDataList(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_RedIntensity = 200;
	m_GreenIntensity = 200;
	m_BlueIntensity = 200;
	m_RegionIndexOnly = 0;
	m_ImageWidth = 100;
	m_ImageHeight = 100;
	m_RedRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
	m_GreenRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
	m_BlueRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
}

void CAutoScanDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BLUESTAIN, m_BlueStain);
	DDX_Control(pDX, IDC_GREENSTAIN, m_GreenStain);
	DDX_Control(pDX, IDC_CELLREGIONLIST, m_CellRegionList);
	DDX_Text(pDX, IDC_OVERLAYFILE, m_RedFileName);
	DDX_Control(pDX, IDC_REDSTAIN, m_RedStain);
	DDX_Control(pDX, IDC_RGBIMAGE, m_RGBImage);
	DDX_Text(pDX, IDC_STATUS, m_Status);
	DDX_Text(pDX, IDC_CELLIDX, m_CellRegionIdx);
	DDX_Control(pDX, IDC_GBIMAGE, m_GBImage);
	DDX_Control(pDX, IDC_RBIMAGE, m_RBImage);
	DDX_Control(pDX, IDC_USEKEY2, m_UseKey);
	DDX_Control(pDX, IDC_CELLSCOREList, m_CellScoreList);
}

BEGIN_MESSAGE_MAP(CAutoScanDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CAutoScanDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_OPENOVERLAY, &CAutoScanDlg::OnBnClickedOpenred)
	ON_NOTIFY(NM_CLICK, IDC_CELLREGIONLIST, &CAutoScanDlg::OnNMClickCellregionlist)
	ON_BN_CLICKED(IDC_LOADPARAM, &CAutoScanDlg::OnBnClickedLoadparam)
	ON_BN_CLICKED(IDC_SAVEREGION, &CAutoScanDlg::OnBnClickedSaveregion)
	ON_MESSAGE(WM_MY_MESSAGE, OnMyMessage)
	ON_MESSAGE(ENABLE_MESSAGE, OnEnableMessage)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_CELLREGIONLIST, &CAutoScanDlg::OnColumnclickCellregionlist)
	ON_BN_CLICKED(IDC_BATCH, &CAutoScanDlg::OnBnClickedBatch)
	ON_BN_CLICKED(IDC_COMPARE2, &CAutoScanDlg::OnBnClickedCompare2)
END_MESSAGE_MAP()


// CAutoScanDlg 訊息處理常式

BOOL CAutoScanDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 設定此對話方塊的圖示。當應用程式的主視窗不是對話方塊時，
	// 框架會自動從事此作業
	SetIcon(m_hIcon, TRUE);			// 設定大圖示
	SetIcon(m_hIcon, FALSE);		// 設定小圖示

	// TODO:  在此加入額外的初始設定
	CString version;
	version.Format(_T("AutoScanForConfirmedCTCs(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	SetWindowText(version);
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version);
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_Status = "Please load Image Files";
	UpdateData(FALSE);
	m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);
	m_RedImage.Create(m_ImageWidth, -m_ImageHeight, 24);
	m_GreenImage.Create(m_ImageWidth, -m_ImageHeight, 24);
	m_BlueImage.Create(m_ImageWidth, -m_ImageHeight, 24);
	m_ImageGB.Create(m_ImageWidth, -m_ImageHeight, 24);
	m_ImageRB.Create(m_ImageWidth, -m_ImageHeight, 24);

	int nSize[] = { 70, 65, 70, 70 };
	LV_COLUMN nListColumn;
	for (int i = 0; i < 4; i++)
	{
		nListColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		nListColumn.fmt = LVCFMT_LEFT;
		nListColumn.cx = nSize[i];
		nListColumn.iSubItem = 0;
		if (i == 0)
			nListColumn.pszText = _T("RgnIdx");
		else if (i == 1)
			nListColumn.pszText = _T("Score");
		else if (i == 2)
			nListColumn.pszText = _T("X0");
		else if (i == 3)
			nListColumn.pszText = _T("Y0");
		
		m_CellRegionList.InsertColumn(i, &nListColumn);
	}
	m_CellRegionList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	
	int nSize1[] = { 100, 60, 60 };
	LV_COLUMN nListColumn1;
	for (int i = 0; i < 3; i++)
	{
		nListColumn1.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		nListColumn1.fmt = LVCFMT_LEFT;
		nListColumn1.cx = nSize1[i];
		nListColumn1.iSubItem = 0;
		if (i == 0)
			nListColumn1.pszText = _T("Name");
		else if (i == 1)
			nListColumn1.pszText = _T("Value");
		else if (i == 2)
			nListColumn1.pszText = _T("Score");

		m_CellScoreList.InsertColumn(i, &nListColumn1);
	}
	m_CellScoreList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_HitFinder.m_Log = &m_Log;
	return TRUE;  // 傳回 TRUE，除非您對控制項設定焦點
}

// 如果將最小化按鈕加入您的對話方塊，您需要下列的程式碼，
// 以便繪製圖示。對於使用文件/檢視模式的 MFC 應用程式，
// 框架會自動完成此作業。

void CAutoScanDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 繪製的裝置內容

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 將圖示置中於用戶端矩形
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 描繪圖示
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

		if (m_Image != NULL)
		{
			CPaintDC dc(&m_RGBImage);
			CRect rect;
			m_RGBImage.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_Image.StretchBlt(dc.m_hDC, rect);
		}
		if (m_BlueImage != NULL)
		{
			CPaintDC dc(&m_BlueStain);
			CRect rect;
			m_BlueStain.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_BlueImage.StretchBlt(dc.m_hDC, rect);
		}
		if (m_RedImage != NULL)
		{
			CPaintDC dc(&m_RedStain);
			CRect rect;
			m_RedStain.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_RedImage.StretchBlt(dc.m_hDC, rect);
		}
		if (m_GreenImage != NULL)
		{
			CPaintDC dc(&m_GreenStain);
			CRect rect;
			m_GreenStain.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_GreenImage.StretchBlt(dc.m_hDC, rect);
		}
		if (m_ImageGB != NULL)
		{
			CPaintDC dc(&m_GBImage);
			CRect rect;
			m_GBImage.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_ImageGB.StretchBlt(dc.m_hDC, rect);
		}
		if (m_ImageRB != NULL)
		{
			CPaintDC dc(&m_RBImage);
			CRect rect;
			m_RBImage.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_ImageRB.StretchBlt(dc.m_hDC, rect);
		}
	}
}

// 當使用者拖曳最小化視窗時，
// 系統呼叫這個功能取得游標顯示。
HCURSOR CAutoScanDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CAutoScanDlg::UpdateImageDisplay(void)
{
	RECT rect;
	m_RGBImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_BlueStain.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_GreenStain.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_RedStain.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_GBImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_RBImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}

void CAutoScanDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_RGNDataList);
	m_CellRegionList.DeleteAllItems();
	m_CellScoreList.DeleteAllItems();

	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	delete[] m_RedRegionImage;
	delete[] m_GreenRegionImage;
	delete[] m_BlueRegionImage;
	CDialogEx::OnCancel();
}

void CAutoScanDlg::OnBnClickedOpenred()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	const CString RED_FILEPREFIX = _T("TRITC-Rhoadmine Selection");
	FreeRGNList(&m_RGNDataList);
	m_CellScoreList.DeleteAllItems();
	m_CellRegionList.DeleteAllItems();
	m_RedFileName = _T("");
	m_CellRegionIdx = 0;
	m_Status.Format(_T("Please select the TIFF file with Prefix %s"), RED_FILEPREFIX);
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	CString message;

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_RedFileName = dlg.GetFileName();
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		int index = m_RedFileName.Find(RED_FILEPREFIX, 0);
		if (index == -1)
		{
			m_Status.Format(_T("%s doesn't have Prefix %s"), m_RedFileName, RED_FILEPREFIX);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
		else
		{
			m_Status.Format(_T("Please select rgn.csv file of this sample"));
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			CFileDialog dlg1(TRUE,    // open
							NULL,    // no default extension
							NULL,    // no initial file name
							OFN_FILEMUSTEXIST
							| OFN_HIDEREADONLY,
							_T("Rgn.csv files (*.csv)|*.csv||"), NULL, 0, TRUE);
			if (dlg1.DoModal() == IDOK)
			{
				CString filename1 = dlg1.GetPathName();
				int manualHits = 0;
				int autoscanHits = 0;
				bool ret = ReadFilesAndRun(filename, m_RedFileName, filename1, &manualHits, &autoscanHits, false);
				if (ret)
				{
					m_Status.Format(_T("#Hits=%d"), autoscanHits);
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				}
				else
				{
					m_Status.Format(_T("AutoScan failed"));
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				}
			}
			else
			{
				m_Status.Format(_T("Failed to specify a filename for ConfirmedCTC Hit File"));
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
		}
	}
	else
	{
		m_Status.Format(_T("Failed to select a TIFF File with Prefix %s"), RED_FILEPREFIX);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

void CAutoScanDlg::DisplayCellRegion(int index)
{
	CopyToRGBImage(m_RGNDataList[index]);
	UpdateImageDisplay();
}

void CAutoScanDlg::CopyToRGBImage(CRGNData *rgnPtr)
{
	BYTE *pCursor = (BYTE*)m_Image.GetBits();
	BYTE *pCursorBlue = (BYTE*)m_BlueImage.GetBits();
	BYTE *pCursorRed = (BYTE*)m_RedImage.GetBits();
	BYTE *pCursorGreen = (BYTE*)m_GreenImage.GetBits();
	BYTE *pCursorGB = (BYTE*)m_ImageGB.GetBits();
	BYTE *pCursorRB = (BYTE*)m_ImageRB.GetBits();

	int nHeight = rgnPtr->GetHeight();
	int nWidth = rgnPtr->GetWidth();
	int nStride = m_Image.GetPitch() - (nWidth * 3);

	unsigned short *pBlueBuffer = rgnPtr->GetImage(BLUE_COLOR);
	unsigned short *pGreenBuffer = rgnPtr->GetImage(GREEN_COLOR);
	unsigned short *pRedBuffer = rgnPtr->GetImage(RED_COLOR);
	unsigned short *ptrRed1 = pRedBuffer;
	unsigned short *ptrGreen1 = pGreenBuffer;
	unsigned short *ptrBlue1 = pBlueBuffer;
	unsigned short redMax = rgnPtr->m_RedFrameMax;
	unsigned short greenMax = rgnPtr->m_GreenFrameMax;
	unsigned short blueMax = rgnPtr->m_BlueFrameMax;

	BYTE bluePixelValue = 0;
	BYTE greenPixelValue = 0;
	BYTE redPixelValue = 0;

	for (int y = 0; y<nHeight; y++)
	{
		for (int x = 0; x < nWidth; x++)
		{
			if (pBlueBuffer != NULL)
				bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, rgnPtr->GetCutoff(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR));
			else
				bluePixelValue = 0;
			if (pGreenBuffer != NULL)
				greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, greenMax, rgnPtr->GetCutoff(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR));
			else
				greenPixelValue = 0;
			if (pRedBuffer != NULL)
				redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, rgnPtr->GetCutoff(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR));
			else
				redPixelValue = 0;
		
			*pCursor++ = bluePixelValue;
			*pCursor++ = greenPixelValue;
			*pCursor++ = redPixelValue;

			*pCursorBlue++ = bluePixelValue;
			*pCursorBlue++ = (BYTE)0;
			*pCursorBlue++ = (BYTE)0;

			*pCursorGreen++ = (BYTE)0;
			*pCursorGreen++ = greenPixelValue;
			*pCursorGreen++ = (BYTE)0;

			*pCursorRed++ = (BYTE)0;
			*pCursorRed++ = (BYTE)0;
			*pCursorRed++ = redPixelValue;

			*pCursorRB++ = bluePixelValue;
			*pCursorRB++ = (BYTE)0;
			*pCursorRB++ = redPixelValue;

			*pCursorGB++ = bluePixelValue;
			*pCursorGB++ = greenPixelValue;
			*pCursorGB++ = (BYTE)0;
		}
		if (nStride > 0)
		{
			pCursor += nStride;
			pCursorBlue += nStride;
			pCursorRed += nStride;
			pCursorGreen += nStride;
			pCursorGB += nStride;
			pCursorRB += nStride;
		}
	}

	vector<CBlobData *> *redBlobs = rgnPtr->GetBlobData(RED_COLOR);
	vector<CBlobData *> *greenBlobs = rgnPtr->GetBlobData(GREEN_COLOR);
	vector<CBlobData *> *blueBlobs = rgnPtr->GetBlobData(BLUE_COLOR);
	vector<CBlobData *> *greenRingBlobs = rgnPtr->GetBlobData(GB_COLORS);

	if (redBlobs->size() > 0)
	{
		pCursor = (BYTE*)m_Image.GetBits();
		pCursorRed = (BYTE*)m_RedImage.GetBits();
		pCursorRB = (BYTE*)m_ImageRB.GetBits();
		int pitch = m_Image.GetPitch();
		int pitchRed = m_RedImage.GetPitch();
		int pitchRB = m_ImageRB.GetPitch();
		for (int i = 0; i < (int)redBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*redBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*redBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;

				pCursorRed[pitchRed * y0 + 3 * x0] = (BYTE)255;
				pCursorRed[pitchRed * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorRed[pitchRed * y0 + 3 * x0 + 2] = (BYTE)255;

				pCursorRB[pitchRB * y0 + 3 * x0] = (BYTE)255;
				pCursorRB[pitchRB * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorRB[pitchRB * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}

	if (greenBlobs->size() > 0)
	{
		pCursorGreen = (BYTE*)m_GreenImage.GetBits();
		int pitch = m_GreenImage.GetPitch();
		for (int i = 0; i < (int)greenBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*greenBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*greenBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursorGreen[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}

	if (greenRingBlobs->size() > 0)
	{
		pCursorGreen = (BYTE*)m_GreenImage.GetBits();
		int pitch = m_GreenImage.GetPitch();
		for (int i = 0; i < (int)greenRingBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*greenRingBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*greenRingBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursorGreen[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}

	if (blueBlobs->size() > 0)
	{
		pCursorBlue = (BYTE*)m_BlueImage.GetBits();
		int pitch = m_BlueImage.GetPitch();
		for (int i = 0; i < (int)blueBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*blueBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*blueBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursorBlue[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursorBlue[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorBlue[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}
}

BYTE CAutoScanDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int) contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

void CAutoScanDlg::FillCellRegionList(void)
{
	int FoundHitCount = 0;
	m_CellRegionList.DeleteAllItems();
	m_HitFinder.FillCellRegionList(&m_RGNDataList, &m_CellRegionList, 0, &FoundHitCount);
	m_Status.Format(_T("Found HITs=%d(%0.2lf%%)"), FoundHitCount, (100.0*FoundHitCount / m_RGNDataList.size()));
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	if (m_RGNDataList.size() > 0)
	{
		m_CellRegionIdx = m_RGNDataList[0]->m_HitIndex;
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ProcessOneRegionForDisplay(0, m_RGNDataList[0]->m_HitIndex);
	}
	m_RegionIndexOnly = 0;
}

void CAutoScanDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		delete (*rgnList)[i];
		(*rgnList)[i] = NULL;
	}
	(*rgnList).clear();
}

int CAutoScanDlg::GetRegionIndex(int HitIndex, int xLocation, int yLocation)
{
	int index = 1;
	int xPos = 0;
	int yPos = 0;
	for (int i = 0; i < (int)m_RGNDataList.size(); i++)
	{
		if (m_RGNDataList[i]->m_HitIndex == HitIndex)
		{
			m_RGNDataList[i]->GetPosition(&xPos, &yPos);
			if ((xPos == xLocation) && (yPos == yLocation))
			{
				index = i + 1;
				break;
			}
		}
	}
	return index;
}

void CAutoScanDlg::OnNMClickCellregionlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO:  在此加入控制項告知處理常式程式碼
	*pResult = 0;
	UpdateData(TRUE);
	m_CellScoreList.DeleteAllItems();
	int nSel = m_CellRegionList.GetNextItem(-1, LVNI_ALL | LVNI_SELECTED);
	CString text = m_CellRegionList.GetItemText(nSel, 0);
	int itemData = _wtoi(text);
	text = m_CellRegionList.GetItemText(nSel, 2);
	int xLocation = _wtoi(text);
	text = m_CellRegionList.GetItemText(nSel, 3);
	int yLocation = _wtoi(text);
	int RegionIndex = GetRegionIndex(itemData, xLocation, yLocation);
	m_RegionIndexOnly = RegionIndex - 1;
	if ((RegionIndex > 0) && (RegionIndex <= (int)m_RGNDataList.size()))
	{
		m_CellRegionIdx = itemData;
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ProcessOneRegionForDisplay(RegionIndex - 1, itemData);
	}
}

void CAutoScanDlg::OnBnClickedLoadparam()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CFileDialog dlg(TRUE,    // open
		CString(".txt"),    // with default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("CTCParam files (*.txt)|*.txt||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_CTCParams.LoadCTCParams(dlg.GetPathName());
	}
}

BOOL CAutoScanDlg::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN))
	{
		if (m_UseKey.GetCheck() == BST_CHECKED)
			m_UseKey.SetCheck(BST_UNCHECKED);
		return TRUE;
	}
	else if ((m_UseKey.GetCheck() == BST_CHECKED)
		&& (pMsg->message == WM_KEYDOWN))
	{
		if ((pMsg->wParam == VK_DOWN) && (m_RegionIndexOnly < (int)m_RGNDataList.size()))
		{
			m_RegionIndexOnly++;
			m_CellRegionIdx = m_RGNDataList[m_RegionIndexOnly]->m_HitIndex;
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			ProcessOneRegionForDisplay(m_RegionIndexOnly, m_CellRegionIdx);
		}
		else if ((pMsg->wParam == VK_UP) && (m_RegionIndexOnly > 1))
		{
			m_RegionIndexOnly--;
			m_CellRegionIdx = m_RGNDataList[m_RegionIndexOnly]->m_HitIndex;
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			ProcessOneRegionForDisplay(m_RegionIndexOnly, m_CellRegionIdx);
		}

		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CAutoScanDlg::ScanImageToLocateConfirmedCTC(int x0, int y0, int index)
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CRGNData *region = new CRGNData(x0, y0, m_ImageWidth, m_ImageHeight);
	region->m_HitIndex = index;
	vector<CRGNData *> list;
	m_HitFinder.ScanImageForConfirmedCTCs(&m_CTCParams, m_RedTIFFData, m_GreenTIFFData, m_BlueTIFFData, region, &list);
	delete region;
	for (int i = 0; i < (int)list.size(); i++)
	{
		list[i]->m_HitIndex = index;
		m_RGNDataList.push_back(list[i]);
	}
	list.clear();
}

void CAutoScanDlg::OnBnClickedSaveregion()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	BOOL ret = FALSE;
	CString filenameForSave;

	CFileDialog dlg(FALSE,    // save
		CString(".rgn"),    // no default extension
		NULL,    // no initial file name
		OFN_OVERWRITEPROMPT,
		_T("RGN files (*.rgn)|*.rgn||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		filenameForSave = dlg.GetPathName();
		CStdioFile theFile;

		if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			for (int i = 0; i < (int)m_RGNDataList.size(); i++)
			{
				CString singleLine;
				int x0, y0;
				m_RGNDataList[i]->GetPosition(&x0, &y0);
				singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
					m_RGNDataList[i]->GetColorCode(), x0, y0, i + 1);
				theFile.WriteString(singleLine);
			}
			theFile.Close();
			m_Status.Format(_T("%s Saved."), dlg.GetFileName());
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
		else
		{
			m_Status.Format(_T("Failed to Open %s to save."), dlg.GetFileName());
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}
}

void CAutoScanDlg::ProcessOneRegionForDisplay(int index, int HitIndex)
{
	CRGNData *ptr = m_RGNDataList[index];
	int x0, y0;
	ptr->GetPosition(&x0, &y0);
	bool retRed = m_RedTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_RedRegionImage);
	bool retGreen = m_GreenTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_GreenRegionImage);
	bool retBlue = m_BlueTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_BlueRegionImage);
	if (retRed && retGreen && retBlue)
	{
		ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
		vector<CRGNData *> cellList;
		m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
		if (cellList.size() > 0)
		{
			m_HitFinder.GetCenterCell(ptr, &cellList, -1, -1, &m_CellScoreList, HitIndex);
			FreeRGNList(&cellList);
		}
		else
		{
			ptr->SetScore(0);
		}
		CString message;
		message.Format(_T("ClickOnRegionListCell:RgnIdx=%d,RedMax=%d,BlueMax=%d,Score=%d"),
			HitIndex, ptr->m_RedFrameMax, ptr->m_BlueFrameMax, ptr->GetScore());
		m_Log.Message(message);
		DisplayCellRegion(index);
		ptr->NullImages();
	}
}



void CAutoScanDlg::OnColumnclickCellregionlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO:  在此加入控制項告知處理常式程式碼
	int FoundHitCount = 0;
	m_CellRegionList.DeleteAllItems();
	int sortColumn = pNMLV->iSubItem;
	m_HitFinder.FillCellRegionList(&m_RGNDataList, &m_CellRegionList, sortColumn, &FoundHitCount);
	m_Status.Format(_T("Found HITs=%d(%0.2lf%%)"), FoundHitCount, (100.0*FoundHitCount / m_RGNDataList.size()));
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	*pResult = 0;
}


void CAutoScanDlg::OnBnClickedBatch()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_OPENOVERLAY);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_LOADPARAM);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_SAVEREGION);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_COMPARE2);
	btn->EnableWindow(FALSE);
	CStdioFile theFile;
	if (theFile.Open(_T("CTCFinderBatch.txt"), CFile::modeRead | CFile::typeText))
	{
		theFile.Close();
		HANDLE thread = ::CreateThread(NULL, 0, BatchOperation, this, CREATE_SUSPENDED, NULL);
		CString message;
		if (!thread)
		{
			message.Format(_T("Failed to creat a thread for doing Batch Operation"));
			m_Log.Message(message);
			m_Status = _T("Fail to create thread for doing Batch Operation");
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			return;
		}

		::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
		::ResumeThread(thread);
		message.Format(_T("Launched a thread for doing Batch Operation"));
		m_Log.Message(message);
		m_Status = _T("Launched a  thread for doing Batch Operation");
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	else
	{
		m_Status = _T("Failed to Open CTCFinderBatch.txt File");
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}


void CAutoScanDlg::OnBnClickedCompare2()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CString message;
	CString filename;
	CString manualRGNFilename;
	m_Status = _T("Open Region CSV Data File for Comparison");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("CSV files (*.csv)|*.csv||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		filename = dlg.GetPathName();
		manualRGNFilename = dlg.GetFileName();
	}
	else
	{
		m_Status.Format(_T("Failed to specify RegionCSV Filename to read"));
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		return;
	}

	CString filename1;
	CString reportFilename;
	CFileDialog dlg1(FALSE,    // save
			CString(".csv"),    // no default extension
			NULL,    // no initial file name
			OFN_OVERWRITEPROMPT,
			_T("Report files (*.csv)|*.csv||"), NULL, 0, TRUE);
	if (dlg1.DoModal() == IDOK)
	{
		filename1 = dlg1.GetPathName();
		reportFilename = dlg1.GetFileName();
	}
	else
	{
		m_Status.Format(_T("Failed to specify Report Filename to save"));
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		return;
	}

	int manualHits = 0;
	int autoscanHits = 0;
	CompareAndRun(filename, manualRGNFilename, filename1, reportFilename, &manualHits, &autoscanHits);
}

LRESULT CAutoScanDlg::OnMyMessage(WPARAM wparam, LPARAM lparam)
{
	UpdateData(FALSE);
	return 0;
}

LRESULT CAutoScanDlg::OnEnableMessage(WPARAM wparam, LPARAM lparam)
{
	CButton *btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_OPENOVERLAY);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_LOADPARAM);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_SAVEREGION);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_COMPARE2);
	btn->EnableWindow(TRUE);
	return 0;
}


static DWORD WINAPI BatchOperation(LPVOID param)
{
	CStdioFile theFile;
	CAutoScanDlg *ptr = (CAutoScanDlg *)param;
	CString message;

	if (theFile.Open(_T("CTCFinderBatch.txt"), CFile::modeRead | CFile::typeText))
	{
		CStdioFile batchResFile;
		if (batchResFile.Open(_T("CTCFinderBatchRunResult.csv"), CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			CString textline;
			int lineIdx = 0;
			while (theFile.ReadString(textline))
			{
				lineIdx++;
				int index = textline.Find(_T(","));
				if (index == -1)
				{
					ptr->m_Status.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
					::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					message.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
					ptr->m_Log.Message(message);
					break;
				}
				CString pathname = textline.Mid(0, index);
				textline = textline.Mid(index + 1);

				index = textline.Find(_T(","));
				if (index == -1)
				{
					ptr->m_Status.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
					::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					message.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
					ptr->m_Log.Message(message);
					break;
				}
				CString imagefilename = textline.Mid(0, index);
				textline = textline.Mid(index + 1);

				index = textline.Find(_T(","));
				if (index == -1)
				{
					ptr->m_Status.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
					::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					message.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
					ptr->m_Log.Message(message);
					break;
				}
				CString rgncsvfilename = textline.Mid(0, index);
				CString resultfilename = textline.Mid(index + 1);

				int manualHits = 0;
				int autoscanHits = 0;
				ptr->RunAutoScan(pathname, imagefilename, rgncsvfilename, resultfilename, &manualHits, &autoscanHits);
				message.Format(_T("%s,%d,%d,%.2lf%%\n"), resultfilename, manualHits, autoscanHits, (manualHits > 0) ? (100.0*autoscanHits / manualHits) : 0.0);
				batchResFile.WriteString(message);
				message.Format(_T("%s,%d,%d,%.2lf%%"), resultfilename, manualHits, autoscanHits, (manualHits > 0) ? (100.0*autoscanHits / manualHits) : 0.0);
				ptr->m_Log.Message(message);
				ptr->m_Status.Format(_T("%s,%d,%d,%.2lf%%"), resultfilename, manualHits, autoscanHits, (manualHits > 0) ? (100.0*autoscanHits / manualHits) : 0.0);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}

			batchResFile.Close();
		}
		theFile.Close();
	}
	else
	{
		message.Format(_T("Failed to open AutoScanBatch.txt file"));
		ptr->m_Log.Message(message);
		ptr->m_Status = _T("Failed to open AutoScanBatch.txt file");
		::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	::PostMessage(ptr->GetSafeHwnd(), ENABLE_MESSAGE, NULL, NULL);
	return 0;
}

void CAutoScanDlg::RunAutoScan(CString pathname, CString imagefilename, CString rgncsvfilename, CString resultfilename, int *manualHits, int *autoscanHits)
{
	*manualHits = 0;
	*autoscanHits = 0;
	if (ReadFilesAndRun(pathname + _T("\\") + imagefilename, imagefilename, pathname + _T("\\") + rgncsvfilename, manualHits, autoscanHits, true))
	{
		CompareAndRun(pathname + _T("\\") + rgncsvfilename, rgncsvfilename, pathname + _T("\\") + resultfilename, resultfilename, manualHits, autoscanHits);
	}
}

bool CAutoScanDlg::ReadFilesAndRun(CString filename, CString m_RedFileName, CString rgncsvfilename, int *manualHits, int *autoscanHits, bool batchMode)
{
	bool ret = false;
	const CString RED_FILEPREFIX = _T("TRITC-Rhoadmine Selection");
	const CString GREEN_FILEPREFIX = _T("FITC Selection");
	const CString BLUE_FILEPREFIX = _T("DAPI Selection");
	FreeRGNList(&m_RGNDataList);
	m_CellRegionList.DeleteAllItems();
	m_CellScoreList.DeleteAllItems();
	m_CellRegionIdx = 0;
	CString message;

	int index = m_RedFileName.Find(RED_FILEPREFIX, 0);
	if (index == -1)
	{
		m_Status.Format(_T("%s doesn't have Prefix %s"), m_RedFileName, RED_FILEPREFIX);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	else
	{
		CString postfix = m_RedFileName.Mid(RED_FILEPREFIX.GetLength());
		ret = m_RedTIFFData->LoadRawTIFFFile(filename);
		if (!ret)
		{
			m_Status.Format(_T("Failed to load %s"), m_RedFileName);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
		else
		{
			message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), filename, m_RedTIFFData->GetCPI());
			m_Log.Message(message);
			index = filename.Find(RED_FILEPREFIX, 0);
			CString pathname = filename.Mid(0, index);
			CString filename2;
			filename2.Format(_T("%s%s%s"), pathname, GREEN_FILEPREFIX, postfix);
			ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
			if (!ret)
			{
				m_Status.Format(_T("Failed to load %s%s"), GREEN_FILEPREFIX, postfix);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
			else
			{
				message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
				m_Log.Message(message);
				pathname = filename.Mid(0, index);
				filename2.Format(_T("%s%s%s"), pathname, BLUE_FILEPREFIX, postfix);
				ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					m_Status.Format(_T("Failed to load %s%s"), BLUE_FILEPREFIX, filename.Mid(index + RED_FILEPREFIX.GetLength()));
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				}
				else
				{
					message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
					m_Log.Message(message);
					m_Status.Format(_T("Load rgn.csv File"));
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					CStdioFile theFile;
					if (theFile.Open(rgncsvfilename, CFile::modeRead | CFile::typeText))
					{
						CString textline;
						if (theFile.ReadString(textline))
						{
							int index = textline.Find(_T("CRCViewer"));
							if (index == -1)
							{
								m_Status = _T("Load RegionCSVFile saved by CRCViewer SaveHit Function");
								::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
								theFile.Close();
								ret = false;
								theFile.Close();
								return ret;
							}
							else
							{
								theFile.ReadString(textline);
								theFile.ReadString(textline);
								bool failed = false;
								int lineIdx = 3;
								while (theFile.ReadString(textline))
								{
									int index = textline.Find(_T(","));
									if (index == -1)
									{
										failed = true;
										break;
									}
									CString numString = textline.Mid(0, index);
									textline = textline.Mid(index + 1);
									int hitIndex = _wtoi(numString);

									index = textline.Find(_T(","));
									if (index == -1)
									{
										failed = true;
										break;
									}
									numString = textline.Mid(0, index);
									textline = textline.Mid(index + 1);
									UINT32 colorCode = _wtoi(numString);

									index = textline.Find(_T(","));
									if (index == -1)
									{
										failed = true;
										break;
									}
									numString = textline.Mid(0, index);
									textline = textline.Mid(index + 1);
									int score = _wtoi(numString);

									index = textline.Find(_T(","));
									if (index == -1)
									{
										failed = true;
										break;
									}
									numString = textline.Mid(0, index);
									textline = textline.Mid(index + 1);
									int x0 = _wtoi(numString);

									index = textline.Find(_T(","));
									if (index == -1)
									{
										failed = true;
										break;
									}
									numString = textline.Mid(0, index);
									textline = textline.Mid(index + 1);
									int y0 = _wtoi(numString);
									if ((colorCode == CTC) || (colorCode == CTC2))
									{
										*manualHits++;
										ScanImageToLocateConfirmedCTC(x0, y0, lineIdx-2);
									}
									lineIdx++;
								}

								if (failed)
								{
									m_Status.Format(_T("Failed at Line %d of CSV File"), lineIdx);
									::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
									theFile.Close();
									ret = false;
									return ret;
								}
							}
						}
						theFile.Close();

						if (!batchMode)
							FillCellRegionList();
					}
					else
					{
						m_Status.Format(_T("Failed to open rgn.csv File %s"), rgncsvfilename);
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					}
				}
			}
		}
	}
	if (m_RGNDataList.size() > 0)
	{
		*autoscanHits = (int) m_RGNDataList.size();
	}
	return ret;
}

void CAutoScanDlg::CompareAndRun(CString rgncsvfullfile, CString rgncsvfile, CString resultfullfilename, CString resultfilename, int *manualHits, int *autoscanHits)
{
	vector<CRGNData *> manualRGN;
	
	CString message;
	CStdioFile theFile;
	message.Format(_T("Start to Load %s"), rgncsvfullfile);
	m_Log.Message(message);
	if (theFile.Open(rgncsvfullfile, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		if (theFile.ReadString(textline))
		{
			int index = textline.Find(_T("CRCViewer"));
			if (index == -1)
			{
				m_Status = _T("Load RegionCSVFile saved by CRCViewer SaveHit Function");
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				theFile.Close();
				return;
			}
			else
			{
				theFile.ReadString(textline);
				theFile.ReadString(textline);
				bool failed = false;
				int lineIdx = 3;
				while (theFile.ReadString(textline))
				{
					int index = textline.Find(_T(","));
					if (index == -1)
					{
						failed = true;
						break;
					}
					CString numString = textline.Mid(0, index);
					textline = textline.Mid(index + 1);
					int hitIndex = _wtoi(numString);

					index = textline.Find(_T(","));
					if (index == -1)
					{
						failed = true;
						break;
					}
					numString = textline.Mid(0, index);
					textline = textline.Mid(index + 1);
					UINT32 colorCode = _wtoi(numString);

					index = textline.Find(_T(","));
					if (index == -1)
					{
						failed = true;
						break;
					}
					numString = textline.Mid(0, index);
					textline = textline.Mid(index + 1);
					int score = _wtoi(numString);

					index = textline.Find(_T(","));
					if (index == -1)
					{
						failed = true;
						break;
					}
					numString = textline.Mid(0, index);
					textline = textline.Mid(index + 1);
					int x0 = _wtoi(numString);

					index = textline.Find(_T(","));
					if (index == -1)
					{
						failed = true;
						break;
					}
					numString = textline.Mid(0, index);
					textline = textline.Mid(index + 1);
					int y0 = _wtoi(numString);
					int left = 0;
					int top = 0;
					int right = 0;
					int bottom = 0;
					if (score > 0)
					{
						index = textline.Find(_T(","));
						if (index == -1)
						{
							failed = true;
							break;
						}
						numString = textline.Mid(0, index);
						textline = textline.Mid(index + 1);
						left = _wtoi(numString);

						index = textline.Find(_T(","));
						if (index == -1)
						{
							failed = true;
							break;
						}
						numString = textline.Mid(0, index);
						textline = textline.Mid(index + 1);
						top = _wtoi(numString);

						index = textline.Find(_T(","));
						if (index == -1)
						{
							failed = true;
							break;
						}
						numString = textline.Mid(0, index);
						textline = textline.Mid(index + 1);
						right = _wtoi(numString);

						index = textline.Find(_T(","));
						if (index == -1)
						{
							failed = true;
							break;
						}
						numString = textline.Mid(0, index);
						textline = textline.Mid(index + 1);
						bottom = _wtoi(numString);
					}

					CRGNData *data = new CRGNData(x0, y0, m_ImageWidth, m_ImageHeight);
					data->SetColorCode(colorCode);
					data->SetScore(score);
					if (score > 0)
						data->SetBoundingBox(left, top, right, bottom);
					manualRGN.push_back(data);
					lineIdx++;
				}

				if (failed)
				{
					m_Status.Format(_T("Failed at Line %d of CSV File"), lineIdx);
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					theFile.Close();
					return;
				}
			}
		}
		theFile.Close();
	}

	if (manualRGN.size() > 0)
	{
		if (theFile.Open(resultfullfilename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			CString textline;
			textline.Format(_T("Image:%s Autoscreen #Hits=%lu\n"), m_RedFileName, m_RGNDataList.size());
			theFile.WriteString(textline);
			textline.Format(_T("Manualscreen RegionDataFilename=%s #Hits=%lu\n"), rgncsvfile, manualRGN.size());
			theFile.WriteString(textline);
			int numMatchCTCs = 0;
			int numManualCTCs = 0;
			textline.Format(_T("MIDX,MS,MX0,MY0,MXC,MYC,AIDX,AS,AX0,AY0,AXC,AYC\n"));
			theFile.WriteString(textline);
			CRGNData *ptr = NULL;
			for (int i = 0; i < (int)manualRGN.size(); i++)
			{
				ptr = manualRGN[i];
				int x0, y0;
				ptr->GetPosition(&x0, &y0);
				int score0 = ptr->GetScore();
				int left0, top0, right0, bottom0;
				ptr->GetBoundingBox(&left0, &top0, &right0, &bottom0);
				left0 += x0;
				right0 += x0;
				top0 += y0;
				bottom0 += y0;
				UINT32 color = ptr->GetColorCode();
				if ((color == CTC) || (color == CTC2))
					numManualCTCs++;
				else
					continue;
				int MHitIdx = i + 1;
				int AIndex = 0;
				bool matched = false;
				for (int j = 0; j < (int)m_RGNDataList.size(); j++)
				{
					ptr = m_RGNDataList[j];
					int x1, y1, left, top, right, bottom;
					ptr->GetPosition(&x1, &y1);
					ptr->GetBoundingBox(&left, &top, &right, &bottom);
					int centerX1 = x1 + (left + right) / 2;
					int centerY1 = y1 + (top + bottom) / 2;
					int score = ptr->GetScore();
					if ((centerX1 >= left0) && (centerX1 <= right0) && (centerY1 >= top0) && (centerY1 <= bottom0))
					{
						matched = true;
						AIndex = j;
						numMatchCTCs++;
					}
					if (matched)
					{
						textline.Format(_T("%d,%u,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n"),
							MHitIdx, color, score0, left0, top0, right0, bottom0, AIndex + 1, score, x1, y1, centerX1, centerY1);
						theFile.WriteString(textline);
						break;
					}
				}
				if (!matched)
				{
					textline.Format(_T("%d,%u,%d,%d,%d,%d,%d\n"),
						MHitIdx, color, score0, left0, top0, right0, bottom0);
					theFile.WriteString(textline);
				}
			}
			*manualHits = numManualCTCs;
			*autoscanHits = numMatchCTCs;
			textline.Format(_T("\n"));
			theFile.WriteString(textline);
			textline.Format(_T("Manualscreen RegionDataFilename=%s #Hits=%lu #CTCs=%d\n"), rgncsvfile,
				manualRGN.size(), numManualCTCs);
			theFile.WriteString(textline);
			textline.Format(_T("Autoscreen Image=%s #Hits=%lu\n"), m_RedFileName, m_RGNDataList.size());
			theFile.WriteString(textline);
			textline.Format(_T("Autoscreen Captured CTCs=%d(%.2lf%%)"), numMatchCTCs,(100.0 * numMatchCTCs / numManualCTCs));
			theFile.WriteString(textline);
			theFile.Close();
			m_Status.Format(_T("TotalHits=%u,Captured CTCs=%d(%.2lf%%)"), m_RGNDataList.size(), numMatchCTCs, (100.0 * numMatchCTCs / numManualCTCs));
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			for (int i = 0; i < manualRGN.size(); i++)
			{
				CRGNData *region = manualRGN[i];
				delete region;
				manualRGN[i] = NULL;
			}
			manualRGN.clear();
		}
		else
		{
			m_Status.Format(_T("Failed to open Report File %s to save"), resultfilename);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}
}
