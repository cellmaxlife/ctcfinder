
// CellMapPlusMFCDlg.cpp : 實作檔
//

#include "stdafx.h"
#include "CellMapPlusMFC.h"
#include "CellMapPlusMFCDlg.h"
#include "afxdialogex.h"
#include "SingleRegionImageData.h"
#include "VersionNumber.h"
#include <ctime>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_MY_MESSAGE (WM_USER+1001)
#define ENABLE_MESSAGE (WM_USER+1002)

// CCellMapPlusMFCDlg 對話方塊
static DWORD WINAPI BatchOperation(LPVOID param);

static DWORD WINAPI HitScreenOperation(LPVOID param);

CCellMapPlusMFCDlg::CCellMapPlusMFCDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCellMapPlusMFCDlg::IDD, pParent)
	, m_RedFileName(_T(""))
	, m_RedFullFileName(_T(""))
	, m_Status(_T(""))
	, m_CellRegionIdx(0)
	, m_RGNDataList(NULL)
	, m_Confidence(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_RedIntensity = 200;
	m_GreenIntensity = 200;
	m_BlueIntensity = 200;

	m_ImageWidth = 100;
	m_ImageHeight = 100;
	m_RedRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
	m_GreenRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
	m_BlueRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
}

void CCellMapPlusMFCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BLUESTAIN, m_BlueStain);
	DDX_Control(pDX, IDC_GREENSTAIN, m_GreenStain);
	DDX_Control(pDX, IDC_CELLREGIONLIST, m_CellRegionList);
	DDX_Text(pDX, IDC_OVERLAYFILE, m_RedFileName);
	DDX_Control(pDX, IDC_REDSTAIN, m_RedStain);
	DDX_Control(pDX, IDC_RGBIMAGE, m_RGBImage);
	DDX_Text(pDX, IDC_STATUS, m_Status);
	DDX_Text(pDX, IDC_CELLIDX, m_CellRegionIdx);
	DDX_Control(pDX, IDC_GBIMAGE, m_GBImage);
	DDX_Control(pDX, IDC_RBIMAGE, m_RBImage);
	DDX_Control(pDX, IDC_USEKEY2, m_UseKey);
	DDX_Control(pDX, IDC_CELLSCOREList, m_CellScoreList);
	DDX_Text(pDX, IDC_CONFIDENCE, m_Confidence);
}

BEGIN_MESSAGE_MAP(CCellMapPlusMFCDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CCellMapPlusMFCDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_OPENOVERLAY, &CCellMapPlusMFCDlg::OnBnClickedOpenred)
	ON_NOTIFY(NM_CLICK, IDC_CELLREGIONLIST, &CCellMapPlusMFCDlg::OnNMClickCellregionlist)
	ON_BN_CLICKED(IDC_LOADPARAM, &CCellMapPlusMFCDlg::OnBnClickedLoadparam)
	ON_BN_CLICKED(IDC_SAVEREGION, &CCellMapPlusMFCDlg::OnBnClickedSaveregion)
	ON_BN_CLICKED(IDC_COMPARE, &CCellMapPlusMFCDlg::OnBnClickedCompare)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_CELLREGIONLIST, &CCellMapPlusMFCDlg::OnColumnclickCellregionlist)
	ON_MESSAGE(WM_MY_MESSAGE, OnMyMessage)
	ON_MESSAGE(ENABLE_MESSAGE, OnEnableMessage)
	ON_BN_CLICKED(IDC_BATCH, &CCellMapPlusMFCDlg::OnBnClickedBatch)
END_MESSAGE_MAP()


// CCellMapPlusMFCDlg 訊息處理常式

BOOL CCellMapPlusMFCDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 設定此對話方塊的圖示。當應用程式的主視窗不是對話方塊時，
	// 框架會自動從事此作業
	SetIcon(m_hIcon, TRUE);			// 設定大圖示
	SetIcon(m_hIcon, FALSE);		// 設定小圖示

	// TODO:  在此加入額外的初始設定
	CString version;
	version.Format(_T("CTCFinder(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	SetWindowText(version);
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version);
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_Status = "Please load Image Files";
	UpdateData(FALSE);
	m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);
	m_RedImage.Create(m_ImageWidth, -m_ImageHeight, 24);
	m_GreenImage.Create(m_ImageWidth, -m_ImageHeight, 24);
	m_BlueImage.Create(m_ImageWidth, -m_ImageHeight, 24);
	m_ImageGB.Create(m_ImageWidth, -m_ImageHeight, 24);
	m_ImageRB.Create(m_ImageWidth, -m_ImageHeight, 24);

	int nSize[] = { 70, 65, 70, 70 };
	LV_COLUMN nListColumn;
	for (int i = 0; i < 4; i++)
	{
		nListColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		nListColumn.fmt = LVCFMT_LEFT;
		nListColumn.cx = nSize[i];
		nListColumn.iSubItem = 0;
		if (i == 0)
			nListColumn.pszText = _T("RgnIdx");
		else if (i == 1)
			nListColumn.pszText = _T("Score");
		else if (i == 2)
			nListColumn.pszText = _T("X0");
		else if (i == 3)
			nListColumn.pszText = _T("Y0");
		
		m_CellRegionList.InsertColumn(i, &nListColumn);
	}
	m_CellRegionList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	
	int nSize1[] = { 100, 60, 60 };
	LV_COLUMN nListColumn1;
	for (int i = 0; i < 3; i++)
	{
		nListColumn1.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		nListColumn1.fmt = LVCFMT_LEFT;
		nListColumn1.cx = nSize1[i];
		nListColumn1.iSubItem = 0;
		if (i == 0)
			nListColumn1.pszText = _T("Name");
		else if (i == 1)
			nListColumn1.pszText = _T("Value");
		else if (i == 2)
			nListColumn1.pszText = _T("Score");

		m_CellScoreList.InsertColumn(i, &nListColumn1);
	}
	m_CellScoreList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_HitFinder.m_Log = &m_Log;
	return TRUE;  // 傳回 TRUE，除非您對控制項設定焦點
}

// 如果將最小化按鈕加入您的對話方塊，您需要下列的程式碼，
// 以便繪製圖示。對於使用文件/檢視模式的 MFC 應用程式，
// 框架會自動完成此作業。

void CCellMapPlusMFCDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 繪製的裝置內容

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 將圖示置中於用戶端矩形
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 描繪圖示
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

		if (m_Image != NULL)
		{
			CPaintDC dc(&m_RGBImage);
			CRect rect;
			m_RGBImage.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_Image.StretchBlt(dc.m_hDC, rect);
		}
		if (m_BlueImage != NULL)
		{
			CPaintDC dc(&m_BlueStain);
			CRect rect;
			m_BlueStain.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_BlueImage.StretchBlt(dc.m_hDC, rect);
		}
		if (m_RedImage != NULL)
		{
			CPaintDC dc(&m_RedStain);
			CRect rect;
			m_RedStain.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_RedImage.StretchBlt(dc.m_hDC, rect);
		}
		if (m_GreenImage != NULL)
		{
			CPaintDC dc(&m_GreenStain);
			CRect rect;
			m_GreenStain.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_GreenImage.StretchBlt(dc.m_hDC, rect);
		}
		if (m_ImageGB != NULL)
		{
			CPaintDC dc(&m_GBImage);
			CRect rect;
			m_GBImage.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_ImageGB.StretchBlt(dc.m_hDC, rect);
		}
		if (m_ImageRB != NULL)
		{
			CPaintDC dc(&m_RBImage);
			CRect rect;
			m_RBImage.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_ImageRB.StretchBlt(dc.m_hDC, rect);
		}
	}
}

// 當使用者拖曳最小化視窗時，
// 系統呼叫這個功能取得游標顯示。
HCURSOR CCellMapPlusMFCDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCellMapPlusMFCDlg::UpdateImageDisplay(void)
{
	RECT rect;
	m_RGBImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_BlueStain.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_GreenStain.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_RedStain.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_GBImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
	m_RBImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}

void CCellMapPlusMFCDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_RGNDataList);
	m_CellRegionList.DeleteAllItems();
	m_CellScoreList.DeleteAllItems();

	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	delete[] m_RedRegionImage;
	delete[] m_GreenRegionImage;
	delete[] m_BlueRegionImage;
	CDialogEx::OnCancel();
}

void CCellMapPlusMFCDlg::OnBnClickedOpenred()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	const CString ZEISS_RED_POSTFIX = _T("_c3_ORG.tif");
	const CString LEICA_RED_FILEPREFIX = _T("TRITC-Rhoadmine Selection");
	FreeRGNList(&m_RGNDataList);
	m_CellScoreList.DeleteAllItems();
	m_CellRegionList.DeleteAllItems();
	m_RedFileName = _T("");
	m_RedFullFileName = _T("");
	m_CellRegionIdx = 0;
	m_Status.Format(_T("Load File with Prefix %s or Postfix %s"), LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	CString message;

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_RedFullFileName = dlg.GetPathName();
		m_RedFileName = dlg.GetFileName();
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		int index = m_RedFileName.Find(LEICA_RED_FILEPREFIX, 0);
		if (index == -1)
		{
			index = m_RedFileName.Find(ZEISS_RED_POSTFIX, 0);
		}
		if (index == -1)
		{
			m_Status.Format(_T("%s doesn't have Prefix %s or Postfix %s"), m_RedFileName, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			return;
		}
		else
		{
			HANDLE thread = ::CreateThread(NULL, 0, HitScreenOperation, this, CREATE_SUSPENDED, NULL);

			if (!thread)
			{
				AfxMessageBox(_T("Fail to create thread for screening"));
				message.Format(_T("Failed to creat a thread for screening"));
				m_Log.Message(message);
				return;
			}

			::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
			::ResumeThread(thread);
			message.Format(_T("Launched a thread for screening"));
			m_Log.Message(message);
		}
	}
	else
	{
		m_Status.Format(_T("Failed to select a file with Prefix %s or Postfix %s"), LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

void CCellMapPlusMFCDlg::DisplayCellRegion(int index)
{
	CopyToRGBImage(m_RGNDataList[index]);
	UpdateImageDisplay();
}

void CCellMapPlusMFCDlg::CopyToRGBImage(CRGNData *rgnPtr)
{
	BYTE *pCursor = (BYTE*)m_Image.GetBits();
	BYTE *pCursorBlue = (BYTE*)m_BlueImage.GetBits();
	BYTE *pCursorRed = (BYTE*)m_RedImage.GetBits();
	BYTE *pCursorGreen = (BYTE*)m_GreenImage.GetBits();
	BYTE *pCursorGB = (BYTE*)m_ImageGB.GetBits();
	BYTE *pCursorRB = (BYTE*)m_ImageRB.GetBits();

	int nHeight = rgnPtr->GetHeight();
	int nWidth = rgnPtr->GetWidth();
	int nStride = m_Image.GetPitch() - (nWidth * 3);

	unsigned short *pBlueBuffer = rgnPtr->GetImage(BLUE_COLOR);
	unsigned short *pGreenBuffer = rgnPtr->GetImage(GREEN_COLOR);
	unsigned short *pRedBuffer = rgnPtr->GetImage(RED_COLOR);
	unsigned short *ptrRed1 = pRedBuffer;
	unsigned short *ptrGreen1 = pGreenBuffer;
	unsigned short *ptrBlue1 = pBlueBuffer;
	unsigned short redMax = rgnPtr->m_RedFrameMax;
	unsigned short greenMax = rgnPtr->m_GreenFrameMax;
	unsigned short blueMax = rgnPtr->m_BlueFrameMax;

	BYTE bluePixelValue = 0;
	BYTE greenPixelValue = 0;
	BYTE redPixelValue = 0;

	for (int y = 0; y<nHeight; y++)
	{
		for (int x = 0; x < nWidth; x++)
		{
			if (pBlueBuffer != NULL)
				bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, rgnPtr->GetCutoff(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR));
			else
				bluePixelValue = 0;
			if (pGreenBuffer != NULL)
				greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, greenMax, rgnPtr->GetCutoff(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR));
			else
				greenPixelValue = 0;
			if (pRedBuffer != NULL)
				redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, rgnPtr->GetCutoff(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR));
			else
				redPixelValue = 0;
		
			*pCursor++ = bluePixelValue;
			*pCursor++ = greenPixelValue;
			*pCursor++ = redPixelValue;

			*pCursorBlue++ = bluePixelValue;
			*pCursorBlue++ = (BYTE)0;
			*pCursorBlue++ = (BYTE)0;

			*pCursorGreen++ = (BYTE)0;
			*pCursorGreen++ = greenPixelValue;
			*pCursorGreen++ = (BYTE)0;

			*pCursorRed++ = (BYTE)0;
			*pCursorRed++ = (BYTE)0;
			*pCursorRed++ = redPixelValue;

			*pCursorRB++ = bluePixelValue;
			*pCursorRB++ = (BYTE)0;
			*pCursorRB++ = redPixelValue;

			*pCursorGB++ = bluePixelValue;
			*pCursorGB++ = greenPixelValue;
			*pCursorGB++ = (BYTE)0;
		}
		if (nStride > 0)
		{
			pCursor += nStride;
			pCursorBlue += nStride;
			pCursorRed += nStride;
			pCursorGreen += nStride;
			pCursorGB += nStride;
			pCursorRB += nStride;
		}
	}

	vector<CBlobData *> *redBlobs = rgnPtr->GetBlobData(RED_COLOR);
	vector<CBlobData *> *greenBlobs = rgnPtr->GetBlobData(GREEN_COLOR);
	vector<CBlobData *> *blueBlobs = rgnPtr->GetBlobData(BLUE_COLOR);
	vector<CBlobData *> *greenRingBlobs = rgnPtr->GetBlobData(GB_COLORS);

	if (redBlobs->size() > 0)
	{
		pCursor = (BYTE*)m_Image.GetBits();
		pCursorRed = (BYTE*)m_RedImage.GetBits();
		pCursorRB = (BYTE*)m_ImageRB.GetBits();
		int pitch = m_Image.GetPitch();
		int pitchRed = m_RedImage.GetPitch();
		int pitchRB = m_ImageRB.GetPitch();
		for (int i = 0; i < (int)redBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*redBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*redBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;

				pCursorRed[pitchRed * y0 + 3 * x0] = (BYTE)255;
				pCursorRed[pitchRed * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorRed[pitchRed * y0 + 3 * x0 + 2] = (BYTE)255;

				pCursorRB[pitchRB * y0 + 3 * x0] = (BYTE)255;
				pCursorRB[pitchRB * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorRB[pitchRB * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}

	if (greenBlobs->size() > 0)
	{
		pCursorGreen = (BYTE*)m_GreenImage.GetBits();
		int pitch = m_GreenImage.GetPitch();
		for (int i = 0; i < (int)greenBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*greenBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*greenBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursorGreen[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}

	if (greenRingBlobs->size() > 0)
	{
		pCursorGreen = (BYTE*)m_GreenImage.GetBits();
		int pitch = m_GreenImage.GetPitch();
		for (int i = 0; i < (int)greenRingBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*greenRingBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*greenRingBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursorGreen[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorGreen[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}

	if (blueBlobs->size() > 0)
	{
		pCursorBlue = (BYTE*)m_BlueImage.GetBits();
		int pitch = m_BlueImage.GetPitch();
		for (int i = 0; i < (int)blueBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*blueBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*blueBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursorBlue[pitch * y0 + 3 * x0] = (BYTE)255;
				pCursorBlue[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
				pCursorBlue[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
			}
		}
	}
}

BYTE CCellMapPlusMFCDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int) contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

void CCellMapPlusMFCDlg::FillCellRegionList(void)
{
	int FoundHitCount = 0;
	m_CellRegionList.DeleteAllItems();
	m_HitFinder.FillCellRegionList(&m_RGNDataList, &m_CellRegionList, 0, &FoundHitCount);
	m_Status.Format(_T("Found HITs=%d"), FoundHitCount);
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	if (m_RGNDataList.size() > 0)
	{
		m_CellRegionIdx = 1;
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ProcessOneRegionForDisplay(0);
	}
}

void CCellMapPlusMFCDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		delete (*rgnList)[i];
		(*rgnList)[i] = NULL;
	}
	(*rgnList).clear();
}

void CCellMapPlusMFCDlg::OnNMClickCellregionlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO:  在此加入控制項告知處理常式程式碼
	*pResult = 0;
	UpdateData(TRUE);
	int nSel = m_CellRegionList.GetNextItem(-1, LVNI_ALL | LVNI_SELECTED);
	CString text = m_CellRegionList.GetItemText(nSel, 0);
	int itemData = _wtoi(text);
	if ((itemData > 0) && (itemData <= (int)m_RGNDataList.size()))
	{
		m_CellRegionIdx = itemData;
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ProcessOneRegionForDisplay(m_CellRegionIdx - 1);
	}
}

void CCellMapPlusMFCDlg::OnBnClickedLoadparam()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CFileDialog dlg(TRUE,    // open
		CString(".txt"),    // with default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("CTCParam files (*.txt)|*.txt||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_CTCParams.LoadCTCParams(dlg.GetPathName());
	}
}

BOOL CCellMapPlusMFCDlg::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN))
	{
		if (m_UseKey.GetCheck() == BST_CHECKED)
			m_UseKey.SetCheck(BST_UNCHECKED);
		return TRUE;
	}
	else if ((m_UseKey.GetCheck() == BST_CHECKED)
		&& (pMsg->message == WM_KEYDOWN))
	{
		if ((pMsg->wParam >= VK_DOWN) && (m_CellRegionIdx < (int)m_RGNDataList.size()))
		{
			m_CellRegionIdx++;
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			ProcessOneRegionForDisplay(m_CellRegionIdx - 1);
		}
		else if ((pMsg->wParam >= VK_UP) && (m_CellRegionIdx > 1))
		{
			m_CellRegionIdx--;
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			ProcessOneRegionForDisplay(m_CellRegionIdx - 1);
		}

		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CCellMapPlusMFCDlg::GetCTCs(bool batchMode)
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CString message;
	try
	{
		for (int i = 0; i <= (int)PARAM_LASTONE; i++)
		{
			message.Format(_T("%s=%d"), m_CTCParams.m_CTCParamNames[i], m_CTCParams.m_CTCParams[i]);
			m_Log.Message(message);
		}

		m_HitFinder.SetPixelCountLimits(m_CTCParams.m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT],
			m_CTCParams.m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT]);

		m_HitFinder.ScanImage(&m_CTCParams, m_RedTIFFData, m_GreenTIFFData, m_BlueTIFFData, &m_RGNDataList, &m_Confidence);

		if (!batchMode)
		{
			if (m_RGNDataList.size() > 0)
			{
				FillCellRegionList();
			}
			else
			{
				m_Status = _T("Found 0 Hit");
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
		}
	}
	catch (...)
	{
		message.Format(_T("Caught Exception in GetCTCs()"));
		m_Log.Message(message);
	}
}

void CCellMapPlusMFCDlg::OnBnClickedSaveregion()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	BOOL ret = FALSE;
	CString filenameForSave;

	CFileDialog dlg(FALSE,    // save
		CString(".rgn"),    // no default extension
		NULL,    // no initial file name
		OFN_OVERWRITEPROMPT,
		_T("RGN files (*.rgn)|*.rgn||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		filenameForSave = dlg.GetPathName();
		SaveRegionDataFile(filenameForSave);
	}
}

void CCellMapPlusMFCDlg::ProcessOneRegionForDisplay(int index)
{
	CRGNData *ptr = m_RGNDataList[index];
	int x0, y0;
	ptr->GetPosition(&x0, &y0);
	bool ret = m_RedTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_RedRegionImage);
	if (ret)
	{
		ret = m_GreenTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_GreenRegionImage);
		if (ret)
		{
			ret = m_BlueTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_BlueRegionImage);
			if (ret)
			{
				ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
				vector<CRGNData *> cellList;
				m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
				if (cellList.size() > 0)
				{
					m_HitFinder.GetCenterCell(ptr, &cellList, -1, -1, &m_CellScoreList, index + 1);
					FreeRGNList(&cellList);
				}
				else
				{
					ptr->SetScore(0);
				}
				CString message;
				message.Format(_T("ClickOnRegionListCell:RgnIdx=%d,RedMax=%d,BlueMax=%d,Score=%d"),
					index+1, ptr->m_RedFrameMax, ptr->m_BlueFrameMax, ptr->GetScore());
				m_Log.Message(message);
				DisplayCellRegion(m_CellRegionIdx - 1);
				ptr->NullImages();
			}
		}
	}
}

DWORD WINAPI HitScreenOperation(LPVOID param)
{
	CCellMapPlusMFCDlg *ptr = (CCellMapPlusMFCDlg *)param;
	float timeElapsed = 0.0F;
	ptr->ReadFilesAndRun(ptr->m_RedFullFileName, ptr->m_RedFileName, false, &ptr->m_Confidence, &timeElapsed);
	return 0;
}

void CCellMapPlusMFCDlg::OnBnClickedCompare()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CString message;
	CString filename;
	CString manualRGNFilename;
	m_Status = _T("Open Region CSV Data File for Comparison");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("CSV files (*.csv)|*.csv||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		filename = dlg.GetPathName();
		manualRGNFilename = dlg.GetFileName();
	}
	else
	{
		m_Status.Format(_T("Failed to specify Region CSV Filename to read"));
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		return;
	}

	CString reportfullfilename;
	CString reportFilename;

	CFileDialog dlg1(FALSE,    // save
			CString(".csv"),    // no default extension
			NULL,    // no initial file name
			OFN_OVERWRITEPROMPT,
			_T("Report files (*.csv)|*.csv||"), NULL, 0, TRUE);
	if (dlg1.DoModal() == IDOK)
	{
		reportfullfilename = dlg1.GetPathName();
		reportFilename = dlg1.GetFileName();
	}
	else
	{
		m_Status.Format(_T("Failed to specify Report Filename to save"));
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		return;
	}
	int manualHits = 0;
	int autoscanHits = 0;
	int manualCTCs = 0;
	int autoscanCTCs = 0;
	int lastConfirmedCTC = 0;
	CompareAndRun(filename, manualRGNFilename, reportfullfilename, reportFilename, &manualHits, &autoscanHits, 
		&manualCTCs, &autoscanCTCs, &lastConfirmedCTC, NULL, NULL);
}


void CCellMapPlusMFCDlg::OnColumnclickCellregionlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO:  在此加入控制項告知處理常式程式碼
	int FoundHitCount = 0;
	m_CellRegionList.DeleteAllItems();
	int sortColumn = pNMLV->iSubItem;
	m_HitFinder.FillCellRegionList(&m_RGNDataList, &m_CellRegionList, sortColumn, &FoundHitCount);
	m_Status.Format(_T("Found HITs=%d(%0.2lf%%)"), FoundHitCount, (100.0*FoundHitCount / m_RGNDataList.size()));
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	*pResult = 0;
}

LRESULT CCellMapPlusMFCDlg::OnMyMessage(WPARAM wparam, LPARAM lparam)
{
	UpdateData(FALSE);
	return 0;
}

LRESULT CCellMapPlusMFCDlg::OnEnableMessage(WPARAM wparam, LPARAM lparam)
{
	CButton *btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_OPENOVERLAY);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_LOADPARAM);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_SAVEREGION);
	btn->EnableWindow(TRUE);
	btn = (CButton *)GetDlgItem(IDC_COMPARE);
	btn->EnableWindow(TRUE);
	return 0;
}

static DWORD WINAPI BatchOperation(LPVOID param)
{
	CStdioFile theFile;
	CCellMapPlusMFCDlg *ptr = (CCellMapPlusMFCDlg *)param;
	CString message;

	if (theFile.Open(_T("CTCFinderBatch.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		CStdioFile batchResFile;
		CStdioFile attributeDataFile;
		if (attributeDataFile.Open(_T("CTCAttributes.csv"), CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("Sample,ManualRegionIndex,FinderRegionIndex,RedBlobPixelCount,BlueBlobPixelCount,GreenBlobPixelCount,AverageCK20Intensity,CD45AverageIntensity,BlueBlobAverageIntensity,CellSize,NCRatio\n"));
			attributeDataFile.WriteString(textline);
			attributeDataFile.Close();
		}
		if (attributeDataFile.Open(_T("FalseHitAttributes.csv"), CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("Sample,FinderRegionIndex,RedBlobPixelCount,BlueBlobPixelCount,GreenBlobPixelCount,AverageCK20Intensity,CD45AverageIntensity,BlueBlobAverageIntensity,CellSize,NCRatio\n"));
			attributeDataFile.WriteString(textline);
			attributeDataFile.Close();
		}
		if (batchResFile.Open(_T("CTCFinderBatchRunResult.csv"), CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("Sample,ManualHit#,FinderHit#,ManualCTC#,FinderCTC#,Capture(%%),LastCTCIndex,Confidence,Time(sec)\n"));
			batchResFile.WriteString(textline);
			batchResFile.Close();
		}
					
		int lineIdx = 0;
		while (theFile.ReadString(textline))
		{
			lineIdx++;
			int index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("CTCFinder.txt Line#%d Error"), lineIdx);
				ptr->m_Log.Message(message);
				break;
			}
			CString pathname = textline.Mid(0, index);
			textline = textline.Mid(index + 1);

			index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("CTCFinderBatch.txt Line#%d Error"), lineIdx);
				ptr->m_Log.Message(message);
				break;
			}
			CString imagefilename = textline.Mid(0, index);
			textline = textline.Mid(index + 1);

			bool noComparison = true;
			CString rgncsvfilename = _T("");
			CString resultfilename = _T("");
			index = textline.Find(_T(","));
			if (index > -1)
			{
				noComparison = false;
				rgncsvfilename = textline.Mid(0, index);
				resultfilename = textline.Mid(index + 1);
			}

			int manualHits = 0;
			int autoscanHits = 0;
			int manualCTCs = 0;
			int autoscanCTCs = 0;
			int lastConfirmedCTC = 0;
			int confidence = 0;
			float timeElapsed = 0.0F;

			if (!noComparison)
			{
				vector<HIT_ATTRIBUTES> ctcAttributes;
				vector<HIT_ATTRIBUTES> falseHitAttributes;
				ptr->RunAutoScan(pathname, imagefilename, rgncsvfilename, resultfilename, &manualHits, &autoscanHits, &manualCTCs, &autoscanCTCs,
					&lastConfirmedCTC, &confidence, &timeElapsed, &ctcAttributes, &falseHitAttributes);
				message.Format(_T("%s,%d,%d,%d,%d,%.2lf%%,%d,%d,%.2f\n"), resultfilename, manualHits, autoscanHits, manualCTCs, autoscanCTCs,
					((manualCTCs > 0) ? (100.0*autoscanCTCs / manualCTCs) : 0.0), lastConfirmedCTC, confidence, timeElapsed);
				if (batchResFile.Open(_T("CTCFinderBatchRunResult.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
				{
					batchResFile.SeekToEnd();
					batchResFile.WriteString(message);
					batchResFile.Close();
				}
				if (attributeDataFile.Open(_T("CTCAttributes.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
				{
					attributeDataFile.SeekToEnd();
					for (int i = 0; i < (int)ctcAttributes.size(); i++)
					{
						HIT_ATTRIBUTES *ptr = &(ctcAttributes[i]);
						message.Format(_T("%s,%d,%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f\n"), resultfilename, ptr->ManualHitIndex, ptr->FinderHitIndex, ptr->RedBlobPixelCount, ptr->BlueBlobPixelCount,
							ptr->GreenBlobPixelCount, ptr->AverageCK20Intensity, ptr->CD45AverageIntensity, ptr->BlueBlobAverageIntensity, ptr->CellSize, ptr->NCRatio);
						attributeDataFile.WriteString(message);
					}
					attributeDataFile.Close();
				}
				if (attributeDataFile.Open(_T("FalseHitAttributes.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
				{
					attributeDataFile.SeekToEnd();
					for (int i = 0; i < (int)falseHitAttributes.size(); i++)
					{
						HIT_ATTRIBUTES *ptr = &(falseHitAttributes[i]);
						message.Format(_T("%s,%d,%d,%d,%d,%d,%d,%d,%.2f,%.2f\n"), resultfilename, ptr->FinderHitIndex, ptr->RedBlobPixelCount, ptr->BlueBlobPixelCount,
							ptr->GreenBlobPixelCount, ptr->AverageCK20Intensity, ptr->CD45AverageIntensity, ptr->BlueBlobAverageIntensity, ptr->CellSize, ptr->NCRatio);
						attributeDataFile.WriteString(message);
					}
					attributeDataFile.Close();
				}
				ctcAttributes.clear();
				falseHitAttributes.clear();
				message.Format(_T("(%d)%s,%d,%d,%d,%d,%.2lf%%,%d,%d,%.2f(sec)"), lineIdx, resultfilename, manualHits, autoscanHits, manualCTCs, autoscanCTCs,
					((manualCTCs > 0) ? (100.0*autoscanCTCs / manualCTCs) : 0.0), lastConfirmedCTC, confidence, timeElapsed);
				ptr->m_Log.Message(message);
				ptr->m_Status.Format(_T("%d)%s,%d,%d,%d,%d,%.2lf%%,%d,%d,%.2f(sec)"), lineIdx, resultfilename, manualHits, autoscanHits, manualCTCs, autoscanCTCs,
					((manualCTCs > 0) ? (100.0*autoscanCTCs / manualCTCs) : 0.0), lastConfirmedCTC, confidence, timeElapsed);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
			else
			{
				ptr->ReadFilesAndRun(pathname + _T("\\") + imagefilename, imagefilename, true, &confidence, &timeElapsed);
				autoscanHits = ptr->GetTotalHitCount();
				message.Format(_T("%s,%d,%d,%.2f\n"), imagefilename, autoscanHits, confidence, timeElapsed);
				if (batchResFile.Open(_T("CTCFinderBatchRunResult.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
				{
					batchResFile.SeekToEnd();
					batchResFile.WriteString(message);
					batchResFile.Close();
				}
				message.Format(_T("(%d)%s,%d,%d,%.2f(sec)"), lineIdx, imagefilename, autoscanHits, confidence, timeElapsed);
				ptr->m_Log.Message(message);
				ptr->m_Status.Format(_T("%d)%s,%d,%d,%.2f(sec)"), lineIdx, imagefilename, autoscanHits, confidence, timeElapsed);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
		}

		theFile.Close();
	}
	else
	{
		message.Format(_T("Failed to open CTCFinderBatch.txt file"));
		ptr->m_Log.Message(message);
		ptr->m_Status = _T("Failed to open CTCFinderBatch.txt file");
		::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	::PostMessage(ptr->GetSafeHwnd(), ENABLE_MESSAGE, NULL, NULL);
	return 0;
}

void CCellMapPlusMFCDlg::RunAutoScan(CString pathname, CString imagefilename, CString rgncsvfilename, CString resultfilename, 
	int *manualHits, int *autoscanHits, int *manualCTCs, int *autoscanCTCs, int *lastConfirmedCTC, int *confidence, 
	float *timeElpased, vector<HIT_ATTRIBUTES> *ctcAttributes, vector<HIT_ATTRIBUTES> *falseHitAttributes)
{
	*manualHits = 0;
	*autoscanHits = 0;
	*manualCTCs = 0;
	*autoscanCTCs = 0;
	*lastConfirmedCTC = 0;
	*confidence = 0;
	if (ReadFilesAndRun(pathname + _T("\\") + imagefilename, imagefilename, true, confidence, timeElpased))
	{
		CompareAndRun(pathname + _T("\\") + rgncsvfilename, rgncsvfilename, pathname + _T("\\") + resultfilename, resultfilename, 
			manualHits, autoscanHits, manualCTCs, autoscanCTCs, lastConfirmedCTC, ctcAttributes, falseHitAttributes);
	}
}

bool CCellMapPlusMFCDlg::ReadFilesAndRun(CString filename, CString m_RedFileName, bool batchMode, int *confidence, float *timeElapsed)
{
	bool ret = false;
	clock_t time1 = clock();
	const CString LEICA_RED_FILEPREFIX = _T("TRITC-Rhoadmine Selection");
	const CString LEICA_GREEN_FILEPREFIX = _T("FITC Selection");
	const CString LEICA_BLUE_FILEPREFIX = _T("DAPI Selection");
	const CString ZEISS_RED_POSTFIX = _T("_c3_ORG.tif");
	const CString ZEISS_GREEN_POSTFIX = _T("_c4_ORG.tif");
	const CString ZEISS_BLUE_POSTFIX = _T("_c1_ORG.tif");
	FreeRGNList(&m_RGNDataList);
	m_CellRegionList.DeleteAllItems();
	m_CellScoreList.DeleteAllItems();
	m_CellRegionIdx = 0;
	CString message;

	ret = m_RedTIFFData->LoadRawTIFFFile(filename);
	if (!ret)
	{
		m_Status.Format(_T("Failed to load %s"), m_RedFileName);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	else
	{
		message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), filename, m_RedTIFFData->GetCPI());
		m_Log.Message(message);
		int index = m_RedFileName.Find(LEICA_RED_FILEPREFIX, 0);
		if (index == -1)
		{
			int postfixIndex = filename.Find(ZEISS_RED_POSTFIX);
			if (postfixIndex == -1)
			{
				m_Status.Format(_T("Red TIFF Filename %s doesn't have PREFIX %s or POSTFIX %s"), m_RedFileName, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
			else
			{
				CString samplePathName = filename.Mid(0, postfixIndex);
				postfixIndex = m_RedFileName.Find(ZEISS_RED_POSTFIX);
				CString sampleName = m_RedFileName.Mid(0, postfixIndex);
				CString filename2 = samplePathName + ZEISS_GREEN_POSTFIX;
				BOOL ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
				if (ret)
				{
					filename2 = samplePathName + ZEISS_BLUE_POSTFIX;
					BOOL ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
						m_CTCParams.LoadCTCParams(message);
						m_Status.Format(_T("Loading and processing images, please wait"));
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
						CleanUpExclusionArea();
						GetCTCs(batchMode);
						*confidence = m_Confidence;
						CString regionFileName;
						WCHAR *char1 = _T("\\");
						int fileStringIndex = filename.ReverseFind(*char1);
						CString ExclusionDataFilename = filename.Mid(0, fileStringIndex + 1);
						regionFileName.Format(_T("%sCMP%sRun.rgn"), sampleName, CELLMAPPLUS_VERSION);
						regionFileName = filename.Mid(0, fileStringIndex + 1) + regionFileName;
						SaveRegionDataFile(regionFileName);
					}
					else
					{
						m_Status = _T("Failed to load Blue TIFF File");
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					}
				}
				else
				{
					m_Status = _T("Failed to load Green TIFF File");
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				}
			}
		}
		else
		{
			CString postfix = m_RedFileName.Mid(LEICA_RED_FILEPREFIX.GetLength());
			index = filename.Find(LEICA_RED_FILEPREFIX, 0);
			CString pathname = filename.Mid(0, index);
			CString filename2;
			filename2.Format(_T("%s%s%s"), pathname, LEICA_GREEN_FILEPREFIX, postfix);
			ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
			if (!ret)
			{
				m_Status.Format(_T("Failed to load %s%s"), LEICA_GREEN_FILEPREFIX, postfix);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
			else
			{
				message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
				m_Log.Message(message);
				pathname = filename.Mid(0, index);
				filename2.Format(_T("%s%s%s"), pathname, LEICA_BLUE_FILEPREFIX, postfix);
				ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					m_Status.Format(_T("Failed to load %s%s"), LEICA_BLUE_FILEPREFIX, postfix);
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				}
				else
				{
					message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
					m_Log.Message(message);
					message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
					m_CTCParams.LoadCTCParams(message);
					m_Status.Format(_T("Loading and processing images, please wait"));
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					CleanUpExclusionArea();
					GetCTCs(batchMode);
					*confidence = m_Confidence;
					CString regionFileName;
					WCHAR *char1 = _T("\\");
					int fileStringIndex = filename.ReverseFind(*char1);
					CString ExclusionDataFilename = filename.Mid(0, fileStringIndex + 1);
					index = postfix.Find(_T(".tif"));
					CString sampleName = postfix.Mid(0, index);
					regionFileName.Format(_T("%sCMP%sRun.rgn"), sampleName, CELLMAPPLUS_VERSION);
					regionFileName = filename.Mid(0, fileStringIndex + 1) + regionFileName;
					SaveRegionDataFile(regionFileName);
				}
			}
		}
	}
	clock_t time2 = clock();
	clock_t timediff = time2 - time1;
	*timeElapsed = ((float)timediff) / CLOCKS_PER_SEC;
	return ret;
}

void CCellMapPlusMFCDlg::CompareAndRun(CString rgncsvfullfile, CString rgncsvfile, CString resultfullfilename, CString resultfilename, 
	int *manualHits, int *autoscanHits, int *manualCTCs, int *autoscanCTCs, int *lastConfirmedCTC,
	vector<HIT_ATTRIBUTES> *ctcAttributes, vector<HIT_ATTRIBUTES> *falseHitAttributes)
{
	vector<CRGNData *> manualRGN;
	
	CString message;
	CStdioFile theFile;
	message.Format(_T("Start to Load %s"), rgncsvfullfile);
	m_Log.Message(message);
	if (theFile.Open(rgncsvfullfile, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		if (theFile.ReadString(textline))
		{
			int index = textline.Find(_T("CRCViewer"));
			if (index == -1)
			{
				m_Status = _T("Load RegionCSVFile saved by CRCViewer SaveHit Function");
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				theFile.Close();
				return;
			}
			else
			{
				theFile.ReadString(textline);
				theFile.ReadString(textline);
				bool failed = false;
				int lineIdx = 3;
				while (theFile.ReadString(textline))
				{
					int index = textline.Find(_T(","));
					if (index == -1)
					{
						failed = true;
						break;
					}
					CString numString = textline.Mid(0, index);
					textline = textline.Mid(index + 1);
					int hitIndex = _wtoi(numString);

					index = textline.Find(_T(","));
					if (index == -1)
					{
						failed = true;
						break;
					}
					numString = textline.Mid(0, index);
					textline = textline.Mid(index + 1);
					UINT32 colorCode = _wtoi(numString);

					index = textline.Find(_T(","));
					if (index == -1)
					{
						failed = true;
						break;
					}
					numString = textline.Mid(0, index);
					textline = textline.Mid(index + 1);
					int score = _wtoi(numString);

					index = textline.Find(_T(","));
					if (index == -1)
					{
						failed = true;
						break;
					}
					numString = textline.Mid(0, index);
					textline = textline.Mid(index + 1);
					int x0 = _wtoi(numString);

					index = textline.Find(_T(","));
					if (index == -1)
					{
						failed = true;
						break;
					}
					numString = textline.Mid(0, index);
					textline = textline.Mid(index + 1);
					int y0 = _wtoi(numString);
					int left = 0;
					int top = 0;
					int right = 0;
					int bottom = 0;
					if (score > 0)
					{
						index = textline.Find(_T(","));
						if (index == -1)
						{
							failed = true;
							break;
						}
						numString = textline.Mid(0, index);
						textline = textline.Mid(index + 1);
						left = _wtoi(numString);

						index = textline.Find(_T(","));
						if (index == -1)
						{
							failed = true;
							break;
						}
						numString = textline.Mid(0, index);
						textline = textline.Mid(index + 1);
						top = _wtoi(numString);

						index = textline.Find(_T(","));
						if (index == -1)
						{
							failed = true;
							break;
						}
						numString = textline.Mid(0, index);
						textline = textline.Mid(index + 1);
						right = _wtoi(numString);

						index = textline.Find(_T(","));
						if (index == -1)
						{
							failed = true;
							break;
						}
						numString = textline.Mid(0, index);
						textline = textline.Mid(index + 1);
						bottom = _wtoi(numString);
					}

					CRGNData *data = new CRGNData(x0, y0, m_ImageWidth, m_ImageHeight);
					data->SetColorCode(colorCode);
					data->SetScore(score);
					if (score > 0)
						data->SetBoundingBox(left, top, right, bottom);
					manualRGN.push_back(data);
					lineIdx++;
				}

				if (failed)
				{
					m_Status.Format(_T("Failed at Line %d of CSV File"), lineIdx);
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					theFile.Close();
					return;
				}
			}
		}
		theFile.Close();
	}

	if (manualRGN.size() > 0)
	{
		CString CTCRgnFilename = resultfullfilename;
		WCHAR *char1 = _T("\\");
		int index = CTCRgnFilename.ReverseFind(*char1);
		CTCRgnFilename = CTCRgnFilename.Mid(0, index + 1);
		index = resultfilename.Find(_T(".csv"));
		CTCRgnFilename += resultfilename.Mid(0, index + 1) + _T("rgn");
		CStdioFile rgnFile;
		int indexConfirmedCTCs = 0;
		if (rgnFile.Open(CTCRgnFilename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			rgnFile.Close();
		}
		if (theFile.Open(resultfullfilename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			theFile.Close();
		}
		else
		{
			m_Status.Format(_T("Failed to open Report File %s to save"), resultfilename);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			for (int i = 0; i < manualRGN.size(); i++)
			{
				CRGNData *region = manualRGN[i];
				delete region;
				manualRGN[i] = NULL;
			}
			manualRGN.clear();
			return;
		}
		CString textline;
		vector<int> missedCTCs;
		vector<int> matchedManualCTCs;
		vector<int> matchedAutoCTCs;
		if (theFile.Open(resultfullfilename, CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
		{
			theFile.SeekToEnd();
			textline.Format(_T("CellMapPlus%s Compare Result CSV File\n"), CELLMAPPLUS_VERSION);
			theFile.WriteString(textline);
			textline.Format(_T("Image:%s Autoscreen #Hits=%lu\n"), m_RedFileName, m_RGNDataList.size());
			theFile.WriteString(textline);
			textline.Format(_T("Manualscreen RegionDataFilename=%s #Hits=%lu\n"), rgncsvfile, manualRGN.size());
			theFile.WriteString(textline);
			textline.Format(_T("MIDX,MC,MS,MLEFT,MTOP,MRIGHT,MBOTTOM,AIDX,AS,AX0,AY0,AXC,AYC,MCTC\n"));
			theFile.WriteString(textline);
			theFile.Close();
		}
		int numMatchHits = 0;
		int numMatchCTCs = 0;
		int numManualCTCs = 0;
		int numManualWBCs = 0;
		int numManualNonCTCs = 0;
		int numManualUnsures = 0;
		int numMatchWBCs = 0;
		int numMatchNonCTCs = 0;
		int numMatchUnsures = 0;
		CRGNData *ptr = NULL;
		int lastRankCTCIndex = 0;
		int manualRgnIdxForLastRankCTC = 0;
		unsigned char *matchFlag = new unsigned char[(int)m_RGNDataList.size()];
		memset(matchFlag, 0, sizeof(unsigned char) * m_RGNDataList.size());
		for (int i = 0; i < (int)manualRGN.size(); i++)
		{
			ptr = manualRGN[i];
			int x0, y0;
			ptr->GetPosition(&x0, &y0);
			int score0 = ptr->GetScore();
			int left0, top0, right0, bottom0;
			ptr->GetBoundingBox(&left0, &top0, &right0, &bottom0);
			left0 += x0;
			right0 += x0;
			top0 += y0;
			bottom0 += y0;
			UINT32 color = ptr->GetColorCode();
			if ((color == CTC) || (color == CTC2))
				numManualCTCs++;
			else if (color == UNSURE)
				numManualUnsures++;
			else if (color == WBC)
				numManualWBCs++;
			else if (color == NONCTC)
				numManualNonCTCs++;

			int MHitIdx = i + 1;
			int AIndex = 0;
			bool matched = false;
			for (int j = 0; j < (int)m_RGNDataList.size(); j++)
			{
				ptr = m_RGNDataList[j];
				int x1, y1, left, top, right, bottom;
				ptr->GetPosition(&x1, &y1);
				ptr->GetBoundingBox(&left, &top, &right, &bottom);
				int centerX1 = x1 + (left + right) / 2;
				int centerY1 = y1 + (top + bottom) / 2;
				left += x1;
				top += y1;
				right += x1;
				bottom += y1;
				int score = ptr->GetScore();
				if ((centerX1 >= left0) && (centerX1 <= right0) && (centerY1 >= top0) && (centerY1 <= bottom0))
				{
					matched = true;
					AIndex = j;
					numMatchHits++;
				}	
				if (matched)
				{
					textline.Format(_T("%d,%u,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,"),
							i + 1, color, score0, left0, top0, right0, bottom0, AIndex + 1, score, x1, y1, centerX1, centerY1);
					if ((color == CTC) || (color == CTC2))
					{
						if (ctcAttributes != NULL)
						{
							HIT_ATTRIBUTES attribute;
							attribute.ManualHitIndex = i + 1;
							attribute.FinderHitIndex = j + 1;
							attribute.RedBlobPixelCount = ptr->GetPixels(RED_COLOR);
							attribute.BlueBlobPixelCount = ptr->GetPixels(RB_COLORS);
							attribute.GreenBlobPixelCount = ptr->GetPixels(GREEN_COLOR);
							attribute.AverageCK20Intensity = ptr->m_RedValue;
							attribute.CD45AverageIntensity = ptr->m_GreenValue;
							attribute.BlueBlobAverageIntensity = ptr->m_BlueValue;
							attribute.CellSize = ptr->m_CellSizeValue;
							attribute.NCRatio = ptr->m_NCRatio;
							ctcAttributes->push_back(attribute);
							matchFlag[j] = (unsigned char)1;
						}
						if (rgnFile.Open(CTCRgnFilename, CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
						{
							indexConfirmedCTCs++;
							CString rgnTextLine;
							rgnTextLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
								CTC2, x1, y1, indexConfirmedCTCs);
							rgnFile.SeekToEnd();
							rgnFile.WriteString(rgnTextLine);
							rgnFile.Close();
						}
						textline += _T("1\n");
						numMatchCTCs++;
						matchedManualCTCs.push_back(i + 1);
						matchedAutoCTCs.push_back(j + 1);
						if ((j + 1) > lastRankCTCIndex)
						{
							lastRankCTCIndex = (j + 1);
							manualRgnIdxForLastRankCTC = (i + 1);
						}
					}
					else if (color == WBC)
					{
						textline += _T("2\n");
						numMatchWBCs++;
					}
					else if (color == UNSURE)
					{
						textline += _T("3\n");
						numMatchUnsures++;
					}
					else
					{
						textline += _T("5\n");
						numMatchNonCTCs++;
					}
					if (theFile.Open(resultfullfilename, CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
					{
						theFile.SeekToEnd();
						theFile.WriteString(textline);
						theFile.Close();
					}
					break;
				}
			}
			if (!matched)
			{
				if ((color == CTC) || (color == CTC2))
				{
					missedCTCs.push_back(i + 1);
				}
			}
		}
		if (falseHitAttributes != NULL)
		{
			for (int j = 0; j < (int)m_RGNDataList.size(); j++)
			{
				if (matchFlag[j] == (unsigned char)0)
				{
					ptr = m_RGNDataList[j];
					HIT_ATTRIBUTES attribute;
					attribute.ManualHitIndex = 0;
					attribute.FinderHitIndex = j + 1;
					attribute.RedBlobPixelCount = ptr->GetPixels(RED_COLOR);
					attribute.BlueBlobPixelCount = ptr->GetPixels(RB_COLORS);
					attribute.GreenBlobPixelCount = ptr->GetPixels(GREEN_COLOR);
					attribute.AverageCK20Intensity = ptr->m_RedValue;
					attribute.CD45AverageIntensity = ptr->m_GreenValue;
					attribute.BlueBlobAverageIntensity = ptr->m_BlueValue;
					attribute.CellSize = ptr->m_CellSizeValue;
					attribute.NCRatio = ptr->m_NCRatio;
					falseHitAttributes->push_back(attribute);
				}
			}
		}
		*manualCTCs = numManualCTCs;
		*autoscanCTCs = numMatchCTCs;
		*manualHits = (int) manualRGN.size();
		*autoscanHits = (int) m_RGNDataList.size();
		if (theFile.Open(resultfullfilename, CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
		{
			theFile.SeekToEnd();
			textline.Format(_T("\n"));
			theFile.WriteString(textline);
			textline.Format(_T("Manualscreen RegionDataFilename=%s #Hits=%lu #CTCs=%d #WBCs=%d #UNSUREs=%d #NonCTCs=%d\n"), rgncsvfile,
				manualRGN.size(), numManualCTCs, numManualWBCs, numManualUnsures, numManualNonCTCs);
			theFile.WriteString(textline);
			textline.Format(_T("Autoscreen Image=%s #Hits=%lu\n"), m_RedFileName, m_RGNDataList.size());
			theFile.WriteString(textline);
			textline.Format(_T("Autoscreen Captured Hits=%d(%.2lf%%) CTCs=%d(%.2lf%%) WBCs=%d(%.2lf%%) UNSUREs=%d(%.2lf%%) NonCTCs=%d(%.2lf%%)\n"), numMatchHits,
				(100.0 * numMatchHits / manualRGN.size()), numMatchCTCs, ((numManualCTCs > 0) ? (100.0 * numMatchCTCs / numManualCTCs) : 0),
				numMatchWBCs, ((numManualWBCs > 0) ? (100.0 * numMatchWBCs / numManualWBCs) : 0),
				numMatchUnsures, ((numManualUnsures > 0) ? (100.0 * numMatchUnsures / numManualUnsures) : 0),
				numMatchNonCTCs, ((numManualNonCTCs > 0) ? (100.0 * numMatchNonCTCs / numManualNonCTCs) : 0));
			theFile.WriteString(textline);
			textline.Format(_T("Autoscreen missed %u CTCs (RegionIndex:"), missedCTCs.size());
			CString regionIndexStr;
			for (int i = 0; i < missedCTCs.size(); i++)
			{
				regionIndexStr.Format(_T("#%d "), missedCTCs[i]);
				textline += regionIndexStr;
			}
			textline += _T(")\n");
			theFile.WriteString(textline);
			textline.Format(_T("Matched CTC Pairs (Region Index):\n"));
			theFile.WriteString(textline);
			textline.Format(_T("Manual, Auto\n"));
			theFile.WriteString(textline);
			for (int i = 0; i < (int)matchedManualCTCs.size(); i++)
			{
				textline.Format(_T("%d,%d\n"), matchedManualCTCs[i], matchedAutoCTCs[i]);
				theFile.WriteString(textline);
			}
			*lastConfirmedCTC = lastRankCTCIndex;
			textline.Format(_T("Manual Region Index=%d  Rank # of Last CTC=%d\n"), manualRgnIdxForLastRankCTC, lastRankCTCIndex);
			theFile.WriteString(textline);
			theFile.Close();
		}
		missedCTCs.clear();
		matchedManualCTCs.clear();
		matchedAutoCTCs.clear();
		m_Status.Format(_T("TotalHits=%u,Captured:Hits=%d(%.2lf%%),CTCs=%d(%.2lf%%),WBCs=%d(%.2lf%%),UNSUREs=%d(%.2lf%%),NonCTCs=%d(%.2lf%%)"), m_RGNDataList.size(), numMatchHits,
			(100.0 * numMatchHits / manualRGN.size()), numMatchCTCs, ((numManualCTCs > 0) ? (100.0 * numMatchCTCs / numManualCTCs) : 0),
				numMatchWBCs, ((numManualWBCs > 0) ? (100.0 * numMatchWBCs / numManualWBCs) : 0),
				numMatchUnsures, ((numManualUnsures > 0) ? (100.0 * numMatchUnsures / numManualUnsures) : 0),
				numMatchNonCTCs, ((numManualNonCTCs > 0) ? (100.0 * numMatchNonCTCs / numManualNonCTCs) : 0));
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		for (int i = 0; i < manualRGN.size(); i++)
		{
			CRGNData *region = manualRGN[i];
			delete region;
			manualRGN[i] = NULL;
		}
		manualRGN.clear();
	}
}

void CCellMapPlusMFCDlg::OnBnClickedBatch()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_OPENOVERLAY);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_LOADPARAM);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_SAVEREGION);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_COMPARE);
	btn->EnableWindow(FALSE);

	CStdioFile theFile;
	if (theFile.Open(_T("CTCFinderBatch.txt"), CFile::modeRead | CFile::typeText))
	{
		theFile.Close();
		HANDLE thread = ::CreateThread(NULL, 0, BatchOperation, this, CREATE_SUSPENDED, NULL);
		CString message;
		if (!thread)
		{
			message.Format(_T("Failed to creat a thread for doing Batch Operation"));
			m_Log.Message(message);
			m_Status = _T("Fail to create thread for doing Batch Operation");
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			return;
		}

		::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
		::ResumeThread(thread);
		message.Format(_T("Launched a thread for doing Batch Operation"));
		m_Log.Message(message);
		m_Status = _T("Launched a thread for doing Batch Operation");
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	else
	{
		m_Status = _T("Failed to Open CTCFinderBatch.txt File");
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

void CCellMapPlusMFCDlg::SaveRegionDataFile(CString filenameForSave)
{
	CStdioFile theFile;

	if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		for (int i = 0; i < (int)m_RGNDataList.size(); i++)
		{
			CString singleLine;
			int x0, y0;
			m_RGNDataList[i]->GetPosition(&x0, &y0);
			m_RGNDataList[i]->SetColorCode(CTC);
			singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
				m_RGNDataList[i]->GetColorCode(), x0, y0, i + 1);
			theFile.WriteString(singleLine);
		}
		theFile.Close();
		WCHAR *char1 = _T("\\");
		int index = filenameForSave.ReverseFind(*char1);
		CString filename1 = filenameForSave.Mid(index + 1);
		m_Status.Format(_T("%s Saved with %u Hits"), filename1, m_RGNDataList.size());
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		index = filenameForSave.Find(_T(".rgn"));
		if (index != -1)
		{
			filenameForSave = filenameForSave.Mid(0, index) + _T(".csv");
			m_HitFinder.DumpRegionDataToCSVFile(filenameForSave, &m_RGNDataList, true);
		}
	}
	else
	{
		WCHAR *char1 = _T("\\");
		int index = filenameForSave.ReverseFind(*char1);
		CString filename1 = filenameForSave.Mid(index + 1);
		m_Status.Format(_T("Failed to Open %s to save."), filename1);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

void CCellMapPlusMFCDlg::CleanUpExclusionArea()
{
	const int ARRAYSIZE = 3000;

	int width = m_RedTIFFData->GetImageWidth();
	int height = m_RedTIFFData->GetImageHeight();

	RECT rect[12]; // 0: Left, 1: Right, 2:Top, 3:Bottom
	int width1;
	int height1;

	rect[0].left = 0;
	rect[0].right = ARRAYSIZE;
	rect[0].top = height / 2 - 25;
	rect[0].bottom = height / 2 + 25;
	
	rect[1].left = 0;
	rect[1].right = ARRAYSIZE;
	rect[1].top = height / 2 - 150;
	rect[1].bottom = height / 2 - 100;

	rect[2].left = 0;
	rect[2].right = ARRAYSIZE;
	rect[2].top = height / 2 + 100;
	rect[2].bottom = height / 2 + 150;

	rect[3].left = width - ARRAYSIZE;
	rect[3].right = width;
	rect[3].top = height / 2 - 25;
	rect[3].bottom = height / 2 + 25;

	rect[4].left = width - ARRAYSIZE;
	rect[4].right = width;
	rect[4].top = height / 2 - 150;
	rect[4].bottom = height / 2 - 100;

	rect[5].left = width - ARRAYSIZE;
	rect[5].right = width;
	rect[5].top = height / 2 + 100;
	rect[5].bottom = height / 2 + 150;

	rect[6].left = width / 2 - 25;
	rect[6].right = width / 2 + 25;
	rect[6].top = 0;
	rect[6].bottom = ARRAYSIZE;

	rect[7].left = width / 2 - 150;
	rect[7].right = width / 2 - 100;
	rect[7].top = 0;
	rect[7].bottom = ARRAYSIZE;

	rect[8].left = width / 2 + 100;
	rect[8].right = width / 2 + 150;
	rect[8].top = 0;
	rect[8].bottom = ARRAYSIZE;

	rect[9].left = width / 2 - 25;
	rect[9].right = width / 2 + 25;
	rect[9].top = height - ARRAYSIZE;
	rect[9].bottom = height;

	rect[10].left = width / 2 - 150;
	rect[10].right = width / 2 - 100;
	rect[10].top = height - ARRAYSIZE;
	rect[10].bottom = height;

	rect[11].left = width / 2 + 100;
	rect[11].right = width / 2 + 150;
	rect[11].top = height - ARRAYSIZE;
	rect[11].bottom = height;

	int left0 = 0;
	int right0 = 0;
	int top0 = 0;
	int bottom0 = 0;
	
	width1 = rect[0].right - rect[0].left;
	height1 = rect[0].bottom - rect[0].top;
	unsigned short *image = new unsigned short[width1 * height1];
	unsigned short *sumImage = new unsigned short[width1 * height1];
	int *profile[3];
	int size = sizeof(int) * ARRAYSIZE;
	for (int i = 0; i < 3; i++)
	{
		profile[i] = new int[ARRAYSIZE];
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		m_RedTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		m_GreenTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		m_BlueTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i], 0, size);
		ImageProjection(sumImage, profile[i], width1, height1, true);
		LowpassFiltering(profile[i], ARRAYSIZE);
	}
	left0 = GetStepEdgePosition(profile, ARRAYSIZE, false) + 300;
	
	for (int i = 3; i < 6; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		m_RedTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		m_GreenTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		m_BlueTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i-3], 0, size);
		ImageProjection(sumImage, profile[i-3], width1, height1, true);
		LowpassFiltering(profile[i-3], ARRAYSIZE);
	}

	right0 = rect[3].left + GetStepEdgePosition(profile, ARRAYSIZE, true) - 300;
	delete[] image;
	delete[] sumImage;

	width1 = rect[6].right - rect[6].left;
	height1 = rect[6].bottom - rect[6].top;
	image = new unsigned short[width1 * height1];
	sumImage = new unsigned short[width1 * height1];

	for (int i = 6; i < 9; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		m_RedTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		m_GreenTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		m_BlueTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 6], 0, size);
		ImageProjection(sumImage, profile[i - 6], width1, height1, false);
		LowpassFiltering(profile[i-6], ARRAYSIZE);
	}

	top0 = GetStepEdgePosition(profile, ARRAYSIZE, false) + 300;
	
	for (int i = 9; i < 12; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		m_RedTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		m_GreenTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		m_BlueTIFFData->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 9], 0, size);
		ImageProjection(sumImage, profile[i - 9], width1, height1, false);
		LowpassFiltering(profile[i-9], ARRAYSIZE);
	}

	bottom0 = rect[9].top + GetStepEdgePosition(profile, ARRAYSIZE, true) - 300;

	delete[] image;
	delete[] sumImage;
	for (int i = 0; i < 3; i++)
		delete[] profile[i];

	int centerX = (right0 + left0) / 2;
	int centerY = (top0 + bottom0) / 2;
	int radiusX = centerX - left0;
	int radiusY = centerY - top0;
	CString message;
	message.Format(_T("Exclusion: CenterX=%d, CenterY=%d, RadiusX=%d, RadiusY=%d"), centerX, centerY, radiusX, radiusY);
	m_Log.Message(message);
	message.Format(_T("Exclusion: Left0=%d, Top0=%d, Right0=%d, Bottom0=%d"), left0, top0, right0, bottom0);
	m_Log.Message(message);
	m_RedTIFFData->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
	m_GreenTIFFData->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
	m_BlueTIFFData->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
}

void CCellMapPlusMFCDlg::SumUpImageData(unsigned short *image, unsigned short *sumImage, int width, int height)
{
	unsigned short *ptrImage = image;
	unsigned short *ptrSum = sumImage;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, ptrImage++, ptrSum++)
		{
			*ptrSum += *ptrImage;
		}
	}
}

void CCellMapPlusMFCDlg::ImageProjection(unsigned short *sumImage, int *profile, int width, int height, bool verticalProjection)
{
	if (verticalProjection)
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				profile[i] += *(sumImage + width * j + i);
			}
		}
	}
	else
	{
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				profile[i] += *(sumImage + width * i + j);
			}
		}
	}
}

int CCellMapPlusMFCDlg::GetStepEdgePosition(int *profile[], int length, bool startFromBeginning)
{
	int stepsize = 10;
	int length1 = length / stepsize;
	int *temp = new int[length1];
	memset(temp, 0, sizeof(int)* length1);
	if (startFromBeginning)
	{
		for (int i = 3; i < (length - 6); i++)
		{
			for (int j = 0; j < 3; j++)
			{
				temp[i/stepsize] += (*(profile[j] + i + 1) - *(profile[j] + i));
			}
		}
	}
	else
	{
		for (int i = (length-6); i > 3; i--)
		{
			for (int j = 0; j < 3; j++)
			{
				temp[i / stepsize] += (*(profile[j] + i - 1) - *(profile[j] + i));
			}
		}
	}
	int max = 0;
	int maxi = 0;
	for (int i = 0; i < length1; i++)
	{
		if (temp[i] > max)
		{
			max = temp[i];
			maxi = i;
		}
	}
	delete[] temp;
	return (stepsize * maxi);
}

void CCellMapPlusMFCDlg::LowpassFiltering(int *profile, int length)
{
	int *temp = new int[length-8];
	temp[0] = 0;

	for (int i = 0; i < 7; i++)
		temp[0] += profile[i];

	for (int i = 1; i < (length - 8); i++)
		temp[i] = temp[i - 1] - profile[i] + profile[i + 7];

	for (int i = 0; i < 3; i++)
		profile[i] = 0;

	for (int i = 3; i < (length - 5); i++)
		profile[i] = temp[i-3] / 7;

	for (int i = (length - 5); i < length; i++)
		profile[i] = 0;

	delete temp;
}

int CCellMapPlusMFCDlg::GetTotalHitCount()
{
	return (int)m_RGNDataList.size();
}