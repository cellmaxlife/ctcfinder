
// CellMapPlusMFCDlg.h : 標頭檔
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "RGNData.h"
#include "Log.h"
#include <vector>
#include "HitFindingOperation.h"
#include "CTCParams.h"
#include "SingleChannelTIFFData.h"

using namespace std;

typedef struct hitattribute_tag
{
	int ManualHitIndex;
	int FinderHitIndex;
	int RedBlobPixelCount;
	int BlueBlobPixelCount;
	int GreenBlobPixelCount;
	int AverageCK20Intensity;
	int CD45AverageIntensity;
	int BlueBlobAverageIntensity;
	float CellSize;
	float NCRatio;
} HIT_ATTRIBUTES;

// CCellMapPlusMFCDlg 對話方塊
class CCellMapPlusMFCDlg : public CDialogEx
{
// 建構
public:
	CCellMapPlusMFCDlg(CWnd* pParent = NULL);	// 標準建構函式

// 對話方塊資料
	enum { IDD = IDD_CELLMAPPLUSMFC_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支援


// 程式碼實作
protected:
	HICON m_hIcon;
	CImage m_Image;
	CImage m_BlueImage;
	CImage m_RedImage;
	CImage m_GreenImage;
	CImage m_ImageGB;
	CImage m_ImageRB;
	CSingleChannelTIFFData *m_RedTIFFData;
	CSingleChannelTIFFData *m_GreenTIFFData;
	CSingleChannelTIFFData *m_BlueTIFFData;
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void UpdateImageDisplay(void);
	vector<CRGNData *> m_RGNDataList;
	void DisplayCellRegion(int index);
	void FillCellRegionList(void);
	void CopyToRGBImage(CRGNData *rgnPtr);
	BYTE GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff);
	CHitFindingOperation m_HitFinder;
	int m_RedIntensity;
	int m_GreenIntensity;
	int m_BlueIntensity;
	CCTCParams m_CTCParams;
	unsigned short *m_RedRegionImage;
	unsigned short *m_GreenRegionImage;
	unsigned short *m_BlueRegionImage;
	int m_ImageWidth;
	int m_ImageHeight;
	void ProcessOneRegionForDisplay(int index);
	void CleanUpExclusionArea();
	void SumUpImageData(unsigned short *image, unsigned short *sumImage, int width, int height);
	void ImageProjection(unsigned short *sumImage, int *profile, int width, int height, bool verticalProjection);
	int GetStepEdgePosition(int *profile[], int length, bool startFromBeginning);
	void LowpassFiltering(int *profile, int length);

	// 產生的訊息對應函式
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_BlueStain;
	CStatic m_GreenStain;
	CListCtrl m_CellRegionList;
	CStatic m_RedStain;
	CStatic m_RGBImage;
	CString m_Status;
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOpenred();
	int m_CellRegionIdx;
	BOOL PreTranslateMessage(MSG* pMsg);
	CString m_RedFileName;
	CString m_RedFullFileName;
	CStatic m_GBImage;
	CStatic m_RBImage;
	afx_msg void OnNMClickCellregionlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedLoadparam();
	afx_msg void OnBnClickedSaveregion();
	CButton m_UseKey;
	void GetCTCs(bool batchMode);
	CListCtrl m_CellScoreList;
	afx_msg void OnBnClickedCompare();
	afx_msg void OnColumnclickCellregionlist(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnMyMessage(WPARAM wparam, LPARAM lparam);
	afx_msg LRESULT OnEnableMessage(WPARAM wparam, LPARAM lparam);
	afx_msg void OnBnClickedBatch();
	CLog m_Log;
	void RunAutoScan(CString pathname, CString imagefilename, CString rgncsvfilename, CString resultfilename, 
		int *manualHits, int *autoscanHits, int *manualCTCs, int *autoscanCTCs, int *lastConfirmedCTC, int *confidence, 
		float *timeElapsed, vector<HIT_ATTRIBUTES> *ctcAttributes, vector<HIT_ATTRIBUTES> *falseHitAttributes);
	bool ReadFilesAndRun(CString imagefullfilename, CString imagefilename, bool batchMode, int *confidence, float *timeElapsed);
	void CompareAndRun(CString rgncsvfullfile, CString rgncsvfile, CString resultfullfilename, CString resultfilename, 
		int *manualHits, int *autoscanHits, int *manualCTCs, int *autoscanCTCs, int *lastConfirmedCTC, 
		vector<HIT_ATTRIBUTES> *ctcAttributes, vector<HIT_ATTRIBUTES> *falseHitAttributes);
	void SaveRegionDataFile(CString filename);
	int m_Confidence;
	int GetTotalHitCount();
};