﻿
// CTCImageGalleryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTCImageGallery.h"
#include "CTCImageGalleryDlg.h"
#include "afxdialogex.h"
#include "VersionNumber.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define IMAGE_WINDOW_X0 13
#define IMAGE_WINDOW_Y0 56
#define IMAGE_WINDOW_X1 1704
#define IMAGE_WINDOW_Y1 987

// CCTCImageGalleryDlg dialog

CCTCImageGalleryDlg::CCTCImageGalleryDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCTCImageGalleryDlg::IDD, pParent)
	, m_ImageFilename(_T(""))
	, m_RegionFilename(_T(""))
	, m_RGNDataList(NULL)
	, m_TotalRegions(0)
	, m_HitsDisplayedSoFar(0)
	, m_HitsLoaded(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_RedIntensity = 200;
	m_GreenIntensity = 200;
	m_BlueIntensity = 200;
	m_ImageWidth = 100;
	m_ImageHeight = 100;
	memset(m_ImageForRegionPair, 0, sizeof(int) * NUM_IMAGES);
	m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	m_RedRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
	m_GreenRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
	m_BlueRegionImage = new unsigned short[m_ImageWidth * m_ImageHeight];
}

void CCTCImageGalleryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_ImageFilename);
	DDX_Text(pDX, IDC_EDIT2, m_RegionFilename);
	DDX_Text(pDX, IDC_EDIT3, m_TotalRegions);
	DDX_Control(pDX, IDC_IMAGE, m_HitImage);
	DDX_Text(pDX, IDC_HITSDISPLAYED, m_HitsDisplayedSoFar);
	DDX_Text(pDX, IDC_HITSLOADED, m_HitsLoaded);
}

BEGIN_MESSAGE_MAP(CCTCImageGalleryDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_LOADIMG, &CCTCImageGalleryDlg::OnBnClickedLoadimg)
	ON_BN_CLICKED(IDC_LOADRGN, &CCTCImageGalleryDlg::OnBnClickedLoadrgn)
	ON_BN_CLICKED(IDC_NEXTVIEW, &CCTCImageGalleryDlg::OnBnClickedNextview)
	ON_BN_CLICKED(IDC_SAVERGN, &CCTCImageGalleryDlg::OnBnClickedSavergn)
	ON_BN_CLICKED(IDCANCEL, &CCTCImageGalleryDlg::OnBnClickedCancel)
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


// CCTCImageGalleryDlg message handlers

BOOL CCTCImageGalleryDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CString version;
	version.Format(_T("CTCImageGallery(%s)"), CTCIMAGEGALLERY_VERSION);
	m_Log.NewLog(version);
	SetWindowText(version);
	version.Format(_T("LeicaParams%s.txt"), CTCIMAGEGALLERY_VERSION);
	m_CTCParams.LoadCTCParams(version);
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	CButton *btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(FALSE);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCTCImageGalleryDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		PaintCImages();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCTCImageGalleryDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCTCImageGalleryDlg::UpdateImageDisplay()
{
	RECT rect;
	m_HitImage.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}


void CCTCImageGalleryDlg::OnBnClickedLoadimg()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(FALSE); 
	btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(FALSE);
	const CString LEICA_RED_FILEPREFIX = _T("TRITC-Rhoadmine Selection");
	const CString LEICA_GREEN_FILEPREFIX = _T("FITC Selection");
	const CString LEICA_BLUE_FILEPREFIX = _T("DAPI Selection");
	const CString ZEISS_RED_POSTFIX = _T("_c3_ORG.tif");
	const CString ZEISS_GREEN_POSTFIX = _T("_c4_ORG.tif");
	const CString ZEISS_BLUE_POSTFIX = _T("_c1_ORG.tif");
	FreeRGNList(&m_RGNDataList);
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	memset(m_ImageForRegionPair, 0, sizeof(int) * NUM_IMAGES);
	m_HitsDisplayedSoFar = 0;
	m_TotalRegions = 0;
	m_HitsLoaded = 0;
	UpdateData(FALSE);
	CString message;

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_ImageFilename = dlg.GetFileName();
		UpdateData(FALSE);
		int index = m_ImageFilename.Find(LEICA_RED_FILEPREFIX, 0);
		if (index == -1)
		{
			index = m_ImageFilename.Find(ZEISS_RED_POSTFIX, 0);
		}
		if (index == -1)
		{
			message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), m_ImageFilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
			AfxMessageBox(message);
			return;
		}
		bool ret = m_RedTIFFData->LoadRawTIFFFile(filename);
		if (!ret)
		{	
			message.Format(_T("Failed to load %s"), m_ImageFilename);
			AfxMessageBox(message);
			return;
		}
		else
		{
			message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), filename, m_RedTIFFData->GetCPI());
			m_Log.Message(message);
			index = m_ImageFilename.Find(LEICA_RED_FILEPREFIX, 0);
			if (index == -1)
			{
				int postfixIndex = filename.Find(ZEISS_RED_POSTFIX);
				if (postfixIndex == -1)
				{
					message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), m_ImageFilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
					AfxMessageBox(message);
					return;
				}
				else
				{
					CString samplePathName = filename.Mid(0, postfixIndex);
					postfixIndex = m_ImageFilename.Find(ZEISS_RED_POSTFIX);
					CString sampleName = m_ImageFilename.Mid(0, postfixIndex);
					CString filename2 = samplePathName + ZEISS_GREEN_POSTFIX;
					BOOL ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename, m_GreenTIFFData->GetCPI());
						m_Log.Message(message);
						filename2 = samplePathName + ZEISS_BLUE_POSTFIX;
						BOOL ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
						if (ret)
						{
							message.Format(_T("Blue TIFF File %s loaded successfully, Green CPI=%d"), filename, m_BlueTIFFData->GetCPI());
							m_Log.Message(message);
							message.Format(_T("ZeissParams%s.txt"), CTCIMAGEGALLERY_VERSION);
							m_CTCParams.LoadCTCParams(message);
							btn = (CButton *)GetDlgItem(IDC_LOADRGN);
							btn->EnableWindow(TRUE);
						}
						else
						{
							message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
							AfxMessageBox(message);
							return;
						}
					}
					else
					{
						message.Format(_T("Failed to load Green TIFF File %s"), filename2);
						AfxMessageBox(message);
						return;
					}
				}
			}
			else
			{
				CString postfix = m_ImageFilename.Mid(LEICA_RED_FILEPREFIX.GetLength());
				index = filename.Find(LEICA_RED_FILEPREFIX, 0);
				CString pathname = filename.Mid(0, index);
				CString filename2;
				filename2.Format(_T("%s%s%s"), pathname, LEICA_GREEN_FILEPREFIX, postfix);
				ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					message.Format(_T("Failed to load Green TIFF File %s"), filename2);
					AfxMessageBox(message);
					return;
				}
				else
				{
					message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
					m_Log.Message(message);
					pathname = filename.Mid(0, index);
					filename2.Format(_T("%s%s%s"), pathname, LEICA_BLUE_FILEPREFIX, postfix);
					ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
					if (!ret)
					{
						message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
						AfxMessageBox(message);
						return;
					}
					else
					{
						message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
						m_Log.Message(message);
						message.Format(_T("LeicaParams%s.txt"), CTCIMAGEGALLERY_VERSION);
						m_CTCParams.LoadCTCParams(message);
						btn = (CButton *)GetDlgItem(IDC_LOADRGN);
						btn->EnableWindow(TRUE);
					}
				}
			}
		}
	}
	btn = (CButton *)GetDlgItem(IDC_LOADIMG);
	btn->EnableWindow(TRUE);
}

void CCTCImageGalleryDlg::OnBnClickedLoadrgn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(FALSE);
	FreeRGNList(&m_RGNDataList);
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	memset(m_ImageForRegionPair, 0, sizeof(int) * NUM_IMAGES);
	m_HitsDisplayedSoFar = 0;
	m_TotalRegions = 0;
	m_HitsLoaded = 0;
	UpdateData(FALSE);
	m_RedIntensity = m_RedTIFFData->GetCPI();
	m_GreenIntensity = m_GreenTIFFData->GetCPI();
	m_BlueIntensity = m_BlueTIFFData->GetCPI();
	CString message;
	for (int i = 0; i <= (int)PARAM_LASTONE; i++)
	{
		message.Format(_T("%s=%d"), m_CTCParams.m_CTCParamNames[i], m_CTCParams.m_CTCParams[i]);
		m_Log.Message(message);
	}

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("RGN files (*.rgn, *.cz)|*.rgn; *.cz"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_RegionFilename = dlg.GetFileName();
		UpdateData(FALSE);
		if (LoadRegionData(filename))
		{
			message.Format(_T("Region File %s loaded successfully, #Hits=%d"), filename, m_RGNDataList.size());
			m_Log.Message(message);
			m_TotalRegions = (int)m_RGNDataList.size();
			m_HitsLoaded = m_TotalRegions;
			UpdateData(FALSE);
			if (m_TotalRegions > 0)
			{
				DisplayCellRegions();
				btn = (CButton *)GetDlgItem(IDC_NEXTVIEW);
				btn->EnableWindow(TRUE);
				btn = (CButton *)GetDlgItem(IDC_SAVERGN);
				btn->EnableWindow(TRUE);
			}
		}
		else
		{
			message.Format(_T("Failed to load Region File %s"), m_RegionFilename);
			AfxMessageBox(message);
		}
	}
	btn = (CButton *)GetDlgItem(IDC_LOADRGN);
	btn->EnableWindow(TRUE);
}


void CCTCImageGalleryDlg::OnBnClickedNextview()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_Image.Create((NUM_IMAGE_COLUMNS * m_ImageWidth), -(NUM_IMAGE_ROWS * m_ImageHeight), 24);
	}
	memset(m_ImageForRegionPair, 0, sizeof(int) * NUM_IMAGES);
	DisplayCellRegions();
}

void CCTCImageGalleryDlg::OnBnClickedSavergn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if (m_RGNDataList.size() > 0)
	{
		CString filenameForSave;

		CFileDialog dlg(FALSE,    // save
			CString(".rgn"),    // no default extension
			NULL,    // no initial file name
			OFN_OVERWRITEPROMPT,
			_T("RGN files (*.rgn)|*.rgn||"), NULL, 0, TRUE);
		if (dlg.DoModal() == IDOK)
		{
			filenameForSave = dlg.GetPathName();
			m_RegionFilename = dlg.GetFileName();
			UpdateData(FALSE);

			CStdioFile theFile;

			if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
			{
				for (int i = 0; i < (int)m_RGNDataList.size(); i++)
				{
					if (m_RGNDataList[i] != NULL)
					{
						CString singleLine;
						int x0, y0;
						m_RGNDataList[i]->GetPosition(&x0, &y0);
						singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
							m_RGNDataList[i]->GetColorCode(), x0, y0, i + 1);
						theFile.WriteString(singleLine);
					}
				}
				theFile.Close();
			}
		}
	}
	else
	{
		AfxMessageBox(_T("ERROR: The Number of Regions is 0"));
	}
}

void  CCTCImageGalleryDlg::PaintCImages()
{
	CPaintDC dc(&m_HitImage);
	CRect rect;
	m_HitImage.GetClientRect(&rect);
	dc.SetStretchBltMode(HALFTONE);
	m_Image.StretchBlt(dc.m_hDC, rect);
	CDC* pDC = m_HitImage.GetDC();
	CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255));
	CPen *pOldPen = pDC->SelectObject(&CursorPen);
	int rectWidth = rect.right - rect.left + 1;
	int rectHeight = rect.bottom - rect.top + 1;
	int blockWidth = rectWidth / NUM_IMAGE_COLUMNS;
	int blockHeight = rectHeight / NUM_IMAGE_ROWS;
	for (int i = 1; i < NUM_IMAGE_ROWS; i++)
	{
		pDC->MoveTo(rect.left, rect.top + i * blockHeight);
		pDC->LineTo(rect.right, rect.top + i * blockHeight);
	}
	for (int i = 1; i < NUM_IMAGE_COLUMNS; i++)
	{
		pDC->MoveTo(rect.left + i * blockWidth, rect.top);
		pDC->LineTo(rect.left + i * blockWidth, rect.bottom);
	}
	for (int i = 0; i < NUM_IMAGES; i++)
	{
		if (m_ImageForRegionPair[i] > 0)
		{
			int ii = i / NUM_IMAGE_COLUMNS;
			int jj = i % NUM_IMAGE_COLUMNS;
			pDC->SetTextColor(RGB(255,255,255));
			pDC->SetBkMode(TRANSPARENT);
			CRect rect1;
			pDC->SetTextAlign(TA_LEFT);
			rect1.left = rect.left + jj * blockWidth + 10;
			rect1.top = rect.top + ii * blockHeight + 10;
			rect1.right = rect1.left + 200;
			rect1.bottom = rect1.top + 20;
			CString label;
			label.Format(_T("%d"), m_ImageForRegionPair[i]);
			pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
		}
	}
	pDC->SelectObject(pOldPen);
}

void CCTCImageGalleryDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_RGNDataList);

	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	delete m_RedRegionImage;
	delete m_GreenRegionImage;
	delete m_BlueRegionImage;
	CDialogEx::OnCancel();
}

BOOL CCTCImageGalleryDlg::LoadRegionData(CString filename)
{
	BOOL ret = FALSE;
	int failureLocation = 0;
	CFile theFile;
	CString message;
	message.Format(_T("Start to Load %s"), filename);
	m_Log.Message(message);
	UINT32 color = 0;
	int index = 0;

	try
	{
		if (theFile.Open(filename, CFile::modeRead))
		{
			failureLocation = 1;
			BYTE* buf = new BYTE[512];
			int count = 0;
			int offset = 0;
			char *ptr = NULL;
			char *ptr1 = NULL;
			int x0 = 0;
			int y0 = 0;

			while (TRUE)
			{
				failureLocation = 2;
				count = theFile.Read(&buf[offset], 1);
				if (count == 1)
				{
					failureLocation = 3;
					if ((buf[offset] == '\0') || (buf[offset] == '\n') || (buf[offset] == '\r'))
					{
						failureLocation = 4;
						if (offset > 0)
						{
							failureLocation = 5;
							if (filename.Find(_T(".rgn"), 0) > 0)
							{
								ptr = strchr((char *)buf, ',');
								if (ptr != NULL)
								{
									failureLocation = 6;
									ptr = strstr(ptr, " 1 ");
									if (ptr != NULL)
									{
										ptr += 3;
										ptr1 = strchr(ptr, ',');
										if (ptr1 != NULL)
										{
											failureLocation = 7;
											*ptr1 = '\0';
											color = (UINT32)atoi(ptr);
											ptr1++;
											ptr = strstr(ptr1, " 2 ");
											if (ptr != NULL)
											{
												failureLocation = 8;
												ptr += 3;
												ptr1 = strchr(ptr, ' ');
												if (ptr1 != NULL)
												{
													failureLocation = 9;
													*ptr1 = '\0';
													x0 = atoi(ptr);
													ptr1++;
													ptr = strchr(ptr1, ',');
													if (ptr != NULL)
													{
														failureLocation = 10;
														*ptr = '\0';
														y0 = atoi(ptr1);
														CRGNData* data = new CRGNData(x0, y0, m_ImageWidth, m_ImageHeight);
														data->SetColorCode(color);
														data->SetCPI(RED_COLOR, m_RedIntensity);
														data->SetCPI(GREEN_COLOR, m_GreenIntensity);
														data->SetCPI(BLUE_COLOR, m_BlueIntensity);
														data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
														data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
														data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
														data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
														data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
														data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
														data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
														data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
														data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
														m_RGNDataList.push_back(data);
														ret = TRUE;
														offset = 0;
														failureLocation = 11;
													}
												}
											}
										}
									}
								}
							}
							else
							{
								failureLocation = 8;
								int type = 0;
								buf[offset + 1] = '\0';
								ptr1 = NULL;
								ptr = strstr((char *)buf, "<Left>");
								if (ptr != NULL)
									type = 1;
								else
								{
									ptr = strstr((char *)buf, "<Top>");
									if (ptr != NULL)
										type = 2;
									else
									{
										failureLocation = 9;
										type = 0;
										if (buf[offset] == '\0')
											break;
										else
										{
											offset = 0;
											continue;
										}
									}
								}
								if (type == 1)
								{
									failureLocation = 10;
									ptr += 6;
									ptr1 = strchr(ptr, '<');
									*ptr1 = '\0';
									x0 = (int)atof(ptr);
									offset = 0;
									continue;
								}
								else if (type == 2)
								{
									failureLocation = 11;
									ptr += 5;
									ptr1 = strchr(ptr, '<');
									*ptr1 = '\0';
									y0 = (int)atof(ptr);
									CRGNData* data = new CRGNData(x0, y0, m_ImageWidth, m_ImageHeight);
									data->SetColorCode(color);
									data->SetCPI(RED_COLOR, m_RedIntensity);
									data->SetCPI(GREEN_COLOR, m_GreenIntensity);
									data->SetCPI(BLUE_COLOR, m_BlueIntensity);
									data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
									data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
									data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
									data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
									data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
									data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
									data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
									data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
									data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
									m_RGNDataList.push_back(data);
									ret = TRUE;
									offset = 0;
									continue;
								}
							}
						}
						else
						{
							if (buf[offset] == '\0')
							{
								failureLocation = 12;
								break;
							}
							else
							{
								failureLocation = 13;
								offset = 0;
								continue;
							}
						}
					}
					else
					{
						failureLocation = 14;
						offset++;
					}
				}
				else
				{
					failureLocation = 15;
					break;
				}
			}

			theFile.Close();
			delete[] buf;
			buf = NULL;
		}
	}
	catch (CException *e)
	{
		TCHAR errCause[255];
		e->GetErrorMessage(errCause, 255);
		CString msg;
		msg.Format(_T("Cought Exception %s, Failre Location = %d"), errCause, failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	catch (...)
	{
		CString msg;
		msg.Format(_T("Cought Exception, Failre Location = %d"), failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	return ret;
}

void CCTCImageGalleryDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CCTCImageGalleryDlg::DisplayCellRegions()
{
	for (int i = 0; i < NUM_IMAGES; i++)
	{
		if (m_ImageForRegionPair[i] == 0)
		{
			if (m_HitsDisplayedSoFar < m_HitsLoaded)
			{
				CRGNData *ptr = m_RGNDataList[m_HitsDisplayedSoFar];
				int x0, y0;
				ptr->GetPosition(&x0, &y0);
				bool redRet = m_RedTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_RedRegionImage);
				bool greenRet = m_GreenTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_GreenRegionImage);
				bool blueRet = m_BlueTIFFData->GetImageRegion(x0, y0, m_ImageWidth, m_ImageHeight, m_BlueRegionImage);
				if (redRet && greenRet && blueRet)
				{
					ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
					ProcessOneRegionForDisplay(ptr);
					CopyToRGBImage(ptr, i);
					ptr->NullImages();
				}
				else
				{
					CString message;
					message.Format(_T("Failed to load region image: X0=%d,Y0=%d,Width=%d,Height=%d"),
						x0, y0, m_ImageWidth, m_ImageHeight);
					m_Log.Message(message);
				}
				m_ImageForRegionPair[i] = m_HitsDisplayedSoFar + 1;
				m_HitsDisplayedSoFar++;
				UpdateData(FALSE);
			}
			else
			{
				memset(m_RedRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
				memset(m_GreenRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
				memset(m_BlueRegionImage, 0, sizeof(unsigned short) * m_ImageWidth * m_ImageHeight);
				CRGNData *ptr = new CRGNData(0, 0, m_ImageWidth, m_ImageHeight);
				ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
				CopyToRGBImage(ptr, i);
				ptr->NullImages();
				delete ptr;
				break;
			}
		}
	}
	UpdateImageDisplay();
}

void CCTCImageGalleryDlg::CopyToRGBImage(CRGNData *rgnPtr, int ImageLocationIndex)
{
	BYTE *pCursor = (BYTE*)m_Image.GetBits();
	int nPitch = m_Image.GetPitch();

	unsigned short *pBlueBuffer = rgnPtr->GetImage(BLUE_COLOR);
	unsigned short *pGreenBuffer = rgnPtr->GetImage(GREEN_COLOR);
	unsigned short *pRedBuffer = rgnPtr->GetImage(RED_COLOR);
	int regionWidth = rgnPtr->GetWidth();
	int regionHeight = rgnPtr->GetHeight();

	int blueMax = rgnPtr->GetContrast(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR);
	if (rgnPtr->m_BlueFrameMax > 0)
		blueMax = rgnPtr->m_BlueFrameMax;
	int redMax = rgnPtr->GetContrast(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR);
	if (rgnPtr->m_RedFrameMax > 0)
		redMax = rgnPtr->m_RedFrameMax;

	BYTE bluePixelValue = 0;
	BYTE greenPixelValue = 0;
	BYTE redPixelValue = 0;

	for (int y = 0; y < regionHeight; y++)
	{
		for (int x = 0; x < regionWidth; x++)
		{
			bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, rgnPtr->GetCutoff(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR));
			greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, rgnPtr->GetContrast(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR), rgnPtr->GetCutoff(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR));
			redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, rgnPtr->GetCutoff(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR));
			pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = bluePixelValue;
			pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = greenPixelValue;
			pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = redPixelValue;
		}
	}

	vector<CBlobData *> *redBlobs = rgnPtr->GetBlobData(RED_COLOR);

	if (redBlobs->size() > 0)
	{
		for (int i = 0; i < (int)redBlobs->size(); i++)
		{
			for (int j = 0; j < (int)(*redBlobs)[i]->m_Boundary->size(); j++)
			{
				unsigned int pos = (*(*redBlobs)[i]->m_Boundary)[j];
				int x0 = m_HitFinder.GetX(pos);
				int y0 = m_HitFinder.GetY(pos);
				pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
				pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
				pCursor[nPitch * (m_ImageHeight * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (m_ImageWidth * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
			}
		}
	}

}

BYTE CCTCImageGalleryDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int)contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

void CCTCImageGalleryDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO:  在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值
	CString message;
	message.Format(_T("Point: X=%d,Y=%d"), point.x, point.y);
	m_Log.Message(message);
	int xLoc = -1;
	int xInterval = (IMAGE_WINDOW_X1 - IMAGE_WINDOW_X0) / NUM_IMAGE_COLUMNS;
	for (int i = 0; i < NUM_IMAGE_COLUMNS; i++)
	{
		if ((point.x >= (IMAGE_WINDOW_X0 + i * xInterval)) &&
			(point.x < (IMAGE_WINDOW_X0 + (i + 1) * xInterval)))
		{
			xLoc = i;
			break;
		}
	}
	int yLoc = -1;
	int yInterval = (IMAGE_WINDOW_Y1 - IMAGE_WINDOW_Y0) / NUM_IMAGE_ROWS;
	for (int i = 0; i < NUM_IMAGE_ROWS; i++)
	{
		if ((point.y >= (IMAGE_WINDOW_Y0 + i * yInterval)) &&
			(point.y < (IMAGE_WINDOW_Y0 + (i + 1) * yInterval)))
		{
			yLoc = i;
			break;
		}
	}
	message.Format(_T("XInterval=%d,YInterval=%d,X0=%d,Y0=%d,XLoc=%d,YLoc=%d"),
		xInterval, yInterval, IMAGE_WINDOW_X0, IMAGE_WINDOW_Y0, xLoc, yLoc);
	m_Log.Message(message);
	if ((xLoc > -1) && (yLoc > -1))
	{
		int ImageLocationIndex = NUM_IMAGE_COLUMNS * yLoc + xLoc;
		int index = m_ImageForRegionPair[ImageLocationIndex];
		message.Format(_T("ImageLoc=%d,RegionIndex=%d"), ImageLocationIndex, index);
		m_Log.Message(message);
		if (index > 0)
		{
			index--;
			if (m_RGNDataList[index] != NULL)
			{
				delete m_RGNDataList[index];
				m_RGNDataList[index] = NULL;
				m_TotalRegions--;
				UpdateData(FALSE);
			}
			m_ImageForRegionPair[ImageLocationIndex] = 0;
			DisplayCellRegions();
		}
	}
	CDialogEx::OnLButtonDblClk(nFlags, point);
}

void CCTCImageGalleryDlg::ProcessOneRegionForDisplay(CRGNData *ptr)
{
	vector<CRGNData *> cellList;
	m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList);
	if (cellList.size() > 0)
	{
		m_HitFinder.GetCenterCell(ptr, &cellList, -1, -1, NULL, 1);
		FreeRGNList(&cellList);
	}
}
