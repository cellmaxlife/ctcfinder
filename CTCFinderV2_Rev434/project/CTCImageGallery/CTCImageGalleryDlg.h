
// CTCImageGalleryDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "RGNData.h"
#include "Log.h"
#include <vector>
#include "HitFindingOperation.h"
#include "CTCParams.h"
#include "SingleChannelTIFFData.h"
using namespace std;

#define NUM_IMAGE_COLUMNS 8
#define NUM_IMAGE_ROWS	4
#define NUM_IMAGES	(NUM_IMAGE_COLUMNS * NUM_IMAGE_ROWS) 

// CCTCImageGalleryDlg dialog
class CCTCImageGalleryDlg : public CDialogEx
{
// Construction
public:
	CCTCImageGalleryDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CTCIMAGEGALLERY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	CImage m_Image;
	CSingleChannelTIFFData *m_RedTIFFData;
	CSingleChannelTIFFData *m_GreenTIFFData;
	CSingleChannelTIFFData *m_BlueTIFFData;
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void UpdateImageDisplay();
	vector<CRGNData *> m_RGNDataList;
	void DisplayCellRegions();
	void CopyToRGBImage(CRGNData *rgnPtr, int ImageLocationIndex);
	BYTE GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff);
	CHitFindingOperation m_HitFinder;
	int m_RedIntensity;
	int m_GreenIntensity;
	int m_BlueIntensity;
	CCTCParams m_CTCParams;
	int m_ImageWidth;
	int m_ImageHeight;
	void PaintCImages();
	BOOL LoadRegionData(CString filename);
	int m_ImageForRegionPair[NUM_IMAGES];
	unsigned short *m_RedRegionImage;
	unsigned short *m_GreenRegionImage;
	unsigned short *m_BlueRegionImage;
	void ProcessOneRegionForDisplay(CRGNData *ptr);

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedLoadimg();
	afx_msg void OnBnClickedLoadrgn();
	afx_msg void OnBnClickedNextview();
	afx_msg void OnBnClickedSavergn();
	CString m_ImageFilename;
	CString m_RegionFilename;
	CLog m_Log;
	void SaveRegionDataFile(CString filename);
	int m_TotalRegions;
	afx_msg void OnBnClickedCancel();
	CStatic m_HitImage;
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	int m_HitsDisplayedSoFar;
	int m_HitsLoaded;
};
